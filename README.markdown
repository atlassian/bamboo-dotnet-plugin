# This repository has been discontinued
=================
!!!The plugin has been monorepoed. This repository exists only so that we can track its history!!!
=================

# Atlassian Bamboo .NET Plugin

The Atlassian Bamboo .NET Plugin adds Tasks for building and testing .NET projects.

## Integration
* Visual Studio 2008 and 2010 projects
* MSBuild projects
* NAnt Projects
* MSTest
* MBUnit
* NUnit

Plugin will autodetect all Visual Studio, MSBuild and MSTest executables.

Legal information
=================

To contribute if you are an Atlassian customer no further action is required because our
[End User Agreement](http://www.atlassian.com/end-user-agreement/) (Section 7) gives Atlassian the right to use
contributions from customers.
If your are not an Atlassian customer then you will need to sign and submit our [Contribution Agreement](ACLA.pdf).


How to contribute
=================
To contribute you code to the plugin please:
* create a feature branch,
* change the code,
* add unit and/or integration tests,
* test it and
* eventually create a pull request to review them.
After our review, your feature branch will be merged into the master branch!

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

See _Contributors License Agreement_ section of [Open Source at Atlassian](https://developer.atlassian.com/opensource/) page.

## Changelog
### 6.0.0
- Bamboo Java Specs for NUnit tasks
### 5.6.0
- MSBuild options quoting finally solved, all options can be passed to MSBuild using response file (*-Dbamboo.plugin.dotnet.msbuild.useResponseFile=true*)

## Special Thanks

The Atlassian Bamboo team would like to thank Ross Rowe for building and maintaining the predecessor to the .NET plugin, the Bamboo NAnt Plugin, for many years.
Without hard working people in the ecosystem like Ross we wouldn't be here today.

## Authors
* Ross Rowe
* James Dumay

## Contributors
* Marko Lahma
* Krystian Brazulewicz

## License
Copyright (c) 2007 Ross Rowe

Copyright (c) 2011 Atlassian

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of the Atlassian nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.