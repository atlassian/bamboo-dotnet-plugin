[@ui.messageBox type="info"]
    [@s.text name="agent.capability.type.system.vstestdiscoverer.create.info" /]
[/@ui.messageBox]

[@s.textfield labelKey="agent.capability.type.system.vstestdiscoverer.key" name="name" cssClass="long-field" /]
[@s.textfield labelKey="agent.capability.type.system.vstestdiscoverer.value" name="supportedFileTypes" cssClass="long-field" /]