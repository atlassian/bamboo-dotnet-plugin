[#-- @ftlvariable name="" type="com.atlassian.bamboo.plugin.dotnet.ncover.ViewNCoverBuildResults" --]
[#-- @ftlvariable name="action" type="com.atlassian.bamboo.plugin.dotnet.ncover.ViewNCoverBuildResults" --]

<html>
<head>
	<title> [@ui.header page='Coverage' object='${immutablePlan.name} ${buildResultsSummary.buildNumber}' title=true /]</title>
	<meta name="tab" content="ncover"/>
</head>

<body>
    <div class="section">
        <h2>Code Coverage</h2>
    [#assign customDataMap=buildResultsSummary.customBuildData /]


[@ui.bambooInfoDisplay title='Test Coverage' float=true]

    [@ww.label label='Line Rate' description='A measurement of the coverage of a particular line of code. The second value in brackets
        is the change in line coverage percentage since the previous successful build.']
        [@ww.param name='value' ]${customDataMap.NCOVER_LINE_RATE?if_exists}%
        [#if customDataMap.NCOVER_COVERAGE_DELTA?exists ]
            (${numberUtils.stringToDouble(customDataMap.NCOVER_COVERAGE_DELTA)}%)
        [/#if]
        [/@ww.param]
    [/@ww.label]

    [@ww.label label='Methods' value='${customDataMap.NCOVER_METHODS?if_exists}' /]
    [@ww.label label='Classes' value='${customDataMap.NCOVER_CLASSES?if_exists}' /]
    [@ww.label label='Assemblies Checked' value='${customDataMap.NCOVER_ASSEMBLIES?if_exists}' /]
    [@ww.label label='Lines of Code' value='${customDataMap.NCOVER_LOC?if_exists}' /]
    [@ww.label label='Lines Executed'value='${customDataMap.NCOVER_LE?if_exists}' /]
    [@ww.label label='Lines Not Executed'value='${customDataMap.NCOVER_LNE?if_exists}' /]
[/@ui.bambooInfoDisplay]

[#if customDataMap.NCOVER_LINE_RATE?exists ]
        [#assign linePercentage=customDataMap.NCOVER_LINE_RATE /]
        <div id="successRate">
            <div id="successRatePercentage">
                <h2>
                    ${numberUtils.stringToLong(linePercentage)}%
                </h2>
                <p>
                    Line Rate
                </p>
            </div>
        </div>
        [/#if]

        </div>

[#if !coverageChanges.isEmpty()]
<div class="section">
        [@ui.bambooInfoDisplay title='Significant Coverage Changes']
	<ul class="classes">
            [#list coverageChanges as classInformation]
            <li>${classInformation.className}: ${classInformation.lineRate * 100}% coverage (
            [#if classInformation.delta < 0]
            <span style="color:#cc3333;">
            [#else]
            <span style="color:#339933;">
			[/#if]
            ${classInformation.delta * 100}%
            </span>
            )
            </li>
            [/#list]
    </ul>
     [/@ui.bambooInfoDisplay]
    </div>
[/#if]


</body>
</html>