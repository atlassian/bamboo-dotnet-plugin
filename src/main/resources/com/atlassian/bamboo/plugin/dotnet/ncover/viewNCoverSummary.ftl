[#-- @ftlvariable name="" type="com.atlassian.bamboo.plugin.dotnet.ncover.ViewNCoverCoverageSummary" --]
[#-- @ftlvariable name="action" type="com.atlassian.bamboo.plugin.dotnet.ncover.ViewNCoverCoverageSummary" --]

<html>
<head>
	<title> [@ui.header page='Coverage Summary' object='${immutablePlan.name}' title=true /]</title>
	<meta name="tab" content="ncover"/>
</head>

<body>
    [@ui.header page='Coverage Summary' object='${immutablePlan.name}' /]

    <div id="graphs" class="topPadded">
        <div id="graph1">
            [@ww.action name="viewNCoverCoverageSummary" namespace="/build" executeResult=true /]
        </div>
    </div>
</body>
</html>