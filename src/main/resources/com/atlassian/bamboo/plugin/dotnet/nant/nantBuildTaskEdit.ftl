[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='nant' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
            list=uiConfigSupport.getExecutableLabels('nant')
            extraUtility=addExecutableLink required='true' /]

[@ww.textfield labelKey='nant.buildFile' name='buildFile' cssClass="long-field" /]
[@ww.textfield labelKey='nant.target' name='target' required='true' cssClass="long-field"/]
[@ww.textfield labelKey='nant.options' name='options' cssClass="long-field"/]

[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
[@ww.textfield labelKey='builder.common.sub' name='workingSubDirectory' helpKey='nant.working.sub.directory' cssClass="long-field" /]