[#if build.buildDefinition.customConfiguration.get('custom.ncover.exists')?has_content]
    [@ui.bambooInfoDisplay titleKey='NCover' float=false height='80px']
    [@ww.label label='NCover XML Directory' ]
            [@ww.param name='value']${build.buildDefinition.customConfiguration.get('custom.ncover.path')?if_exists}[/@ww.param]
    [/@ww.label]
    [/@ui.bambooInfoDisplay]
[/#if]