[@ui.bambooSection title='Where should Bamboo look for the NCover code-coverage output?' ]
    [@ww.checkbox label='NCover output will be produced' name='custom.ncover.exists' toggle='true' description="NCover is a code coverage tool that helps ensure that your code base is well tested.  For more information visit: <a href='http://ncover.sourceforge.net/' target='_blank'>NCover</a>" /]

    [@ui.bambooSection dependsOn='custom.ncover.exists' showOn='true']
        [@ww.textfield name='custom.ncover.path' label='NCover XML Directory'
         description='(Optional) This is where Bamboo will look for the XML report output file from NCover.
                        The NCover report generation must run as part of your build.' cssClass="long-field" /]
    [/@ui.bambooSection]
[/@ui.bambooSection ]