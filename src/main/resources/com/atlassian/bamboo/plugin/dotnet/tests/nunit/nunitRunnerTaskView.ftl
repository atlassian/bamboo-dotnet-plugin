[@ww.label labelKey='nunit.label' name='label' hideOnNull='true' /]
[@ww.label labelKey='nunit.testFiles' name='nunitTestFiles' hideOnNull='true' /]
[@ww.label labelKey='nunit.resultsFile' name='nunitResultsFile' hideOnNull='true' /]
[@ww.label labelKey='nunit.testsToRun' name='run' /]
[@ww.label labelKey='nunit.includedCategories' name='include' hideOnNull='true' /]
[@ww.label labelKey='nunit.excludedCategories' name='exclude' hideOnNull='true' /]
[@ww.label labelKey='nunit.options' name='options' hideOnNull='true' /]
[@ww.label labelKey='builder.common.env' name='environmentVariables' hideOnNull='true' /]
