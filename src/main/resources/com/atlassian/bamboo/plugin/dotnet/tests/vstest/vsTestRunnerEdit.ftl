[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='vstestconsole' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
            list=uiConfigSupport.getExecutableLabels('vstestconsole')
            extraUtility=addExecutableLink required='true' /]

[@ww.textfield labelKey='vstest.testfile' name='vstestTestFile' cssClass="full-width-field" /]

[@ww.textfield labelKey='vstest.resultsFile' name='resultsfile' required='true' cssClass="full-width-field" /]
[@ww.textfield labelKey='vstest.resultDir' name='vstestResultDir' required='true' cssClass="full-width-field" /]

[@ww.checkboxlist labelKey='vstest.required.discoverers' name='vstestTestDiscoverers'
            list='discoverers' /]

[@ww.select cssClass="builderSelectWidget"
            labelKey='vstest.platform' name='vstestPlatform' list='platforms' /]
[@ww.select cssClass="builderSelectWidget"
            labelKey='vstest.framework' name='vstestFramework' list='frameworks' /]

[@ww.select cssClass='builderSelectWidget' labelKey='vstest.useVsixExtensions' name='useVsixExtensions' list=[true,false] /]

[@ww.checkbox labelKey='vstest.enablecodecoverage'  name='vstestEnableCodeCoverage' /]
[@ww.checkbox labelKey='vstest.inisolation' name='vstestInIsolation' /]

[@ww.textfield labelKey='vstest.additional.args' name='additionalArgs' cssClass="long-field" /]

[@ww.textfield labelKey='vstest.settingsfile' name='vstestSettings' cssClass="full-width-field" /]

[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]

