[@ww.label labelKey='executable.type' name='label' hideOnNull='true' /]

[@ww.label labelKey='vstest.testfile' name='vstestTestFile' hideOnNull='true' /]

[@ww.label labelKey='vstest.resultsFile' name='resultsfile' required='true' hideOnNull='true' /]
[@ww.label labelKey='vstest.resultDir' name='vstestResultDir' required='true' hideOnNull='true' /]

[@ww.checkboxlist labelKey='vstest.required.discoverers' name='vstestTestDiscoverers'
            list=vstestSupport.getTestDiscoverers() /]

[@ww.label labelKey='vstest.platform' name='vstestPlatform'  hideOnNull='true' /]
[@ww.label labelKey='vstest.framework' name='vstestFramework' hideOnNull='true' /]
[@ww.label labelKey='vstest.useVsixExtensions' name='useVsixExtensions' /]

[@ww.checkbox labelKey='vstest.enablecodecoverage'  name='vstestEnableCodeCoverage' disabled='true' hideOnNull='true' /]
[@ww.checkbox labelKey='vstest.inisolation' name='vstestInIsolation' disabled='true' hideOnNull='true' /]

[@ww.label labelKey='vstest.settingsfile' name='vstestSettings' hideOnNull='true' /]

[@ww.label labelKey='builder.common.env' name='environmentVariables' hideOnNull='true' /]
[@ww.label labelKey='vstest.additional.args' name='additionalArgs' hideOnNull='true' /]
