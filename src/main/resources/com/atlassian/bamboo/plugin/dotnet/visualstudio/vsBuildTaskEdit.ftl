[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='devenv' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
            list=uiConfigSupport.getExecutableLabels('devenv')
            extraUtility=addExecutableLink required='true' /]

[@ww.textfield labelKey='visualstudio.solution' name='solution' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='visualstudio.options' name='options' required='true' cssClass="long-field" /]
[@ww.select labelKey='visualstudio.environment' name='vsEnvironment' required='true' list='environments'/]

[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field"  /]
[@ww.textfield labelKey='builder.common.sub' name='workingSubDirectory' helpKey='devenv.com.working.sub.directory' cssClass="long-field" /]
