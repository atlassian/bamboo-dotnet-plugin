[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='msbuild' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
            list=uiConfigSupport.getExecutableLabels('msbuild')
            extraUtility=addExecutableLink required='true' /]
[@ww.textfield labelKey='msbuild.solution' name='solution' required='true'  cssClass="long-field"/]
[@ww.textfield labelKey='msbuild.options' name='options' cssClass="long-field"/]
[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
[@ww.textfield labelKey='builder.common.sub' name='workingSubDirectory' helpKey='msbuild.exe.working.sub.directory' cssClass="long-field" /]