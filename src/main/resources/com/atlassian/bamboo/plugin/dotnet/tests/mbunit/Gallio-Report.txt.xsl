<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:g="http://www.gallio.org/">

  <xsl:output method="text" encoding="utf-8"/>

  <xsl:template match="/">
    <xsl:apply-templates select="*" />
  </xsl:template>
  
  <xsl:template match="g:testStepRun">
    <xsl:variable name="kind" select="g:testStep/g:metadata/g:entry[@key='TestKind']/g:value" />

    <xsl:if test="$kind">
      <xsl:text>[</xsl:text>
      <xsl:value-of select="$kind" />
      <xsl:text>] </xsl:text>
    </xsl:if>
    
    <xsl:value-of select="g:testStep/@fullName" />
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="g:testLog" />
    <xsl:text>&#xA;</xsl:text>

    <xsl:apply-templates select="g:children/g:testStepRun" />
  </xsl:template>

  <xsl:template match="g:testLog">
    <xsl:apply-templates select="g:streams" />
  </xsl:template>

  <xsl:template match="g:streams">
    <xsl:apply-templates select="g:stream" />
  </xsl:template>
  
  <xsl:template match="g:stream">
    <xsl:param name="prefix" select="'  '" />

    <xsl:value-of select="$prefix"/>
    <xsl:text>&lt;Stream: </xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text>&gt;&#xA;</xsl:text>
    <xsl:apply-templates select="g:body">
      <xsl:with-param name="prefix" select="concat($prefix, '  ')" />
    </xsl:apply-templates>
    <xsl:value-of select="$prefix"/>
    <xsl:text>&lt;End Stream&gt;&#xA;</xsl:text>
  </xsl:template>
  
  <xsl:template match="g:body">
    <xsl:param name="prefix" select="''" />

    <xsl:apply-templates select="g:contents">
      <xsl:with-param name="prefix" select="$prefix" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="g:contents">
    <xsl:param name="prefix" select="''"  />

    <xsl:apply-templates select="child::node()[self::g:text or self::g:section or self::g:embed or self::g:marker]">
      <xsl:with-param name="prefix" select="$prefix" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="g:text">
    <xsl:param name="prefix" select="''"  />

    <xsl:variable name="preceding-inline-tag" select="preceding::node()[self::g:body or self::g:text or self::g:section or self::g:embed][1][self::g:text]"/>
    <xsl:variable name="following-inline-tag" select="following::node()[self::g:body or self::g:text or self::g:section or self::g:embed][1][self::g:text]"/>

    <xsl:call-template name="indent">
      <xsl:with-param name="text" select="text()" />
      <xsl:with-param name="firstLinePrefix">
        <!-- omit prefix when we have a preceding text node with no intervening block tags -->
        <xsl:if test="not($preceding-inline-tag)">
          <xsl:value-of select="$prefix"/>
        </xsl:if>
      </xsl:with-param>
      <xsl:with-param name="otherLinePrefix" select="$prefix" />
      <!-- omit suffix when we have a following text node with no intervening block tags -->
      <xsl:with-param name="trailingNewline" select="not($following-inline-tag)" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="g:section">
    <xsl:param name="prefix" select="''"  />
    
    <xsl:value-of select="$prefix"/>
    <xsl:text>&lt;Section: </xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text>&gt;&#xA;</xsl:text>
    <xsl:apply-templates select="g:contents">
      <xsl:with-param name="prefix" select="concat($prefix, '  ')" />
    </xsl:apply-templates>
    <xsl:value-of select="$prefix"/>
    <xsl:text>&lt;End Section&gt;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="g:marker">
    <xsl:param name="prefix" select="''"  />

    <xsl:apply-templates select="g:contents">
      <xsl:with-param name="prefix" select="$prefix" />
    </xsl:apply-templates>
  </xsl:template>

 
  <xsl:template match="*">
  </xsl:template>
  
   <!-- Common utility functions -->
  
  <!-- Formats a CodeLocation -->
  <xsl:template name="format-code-location">
    <xsl:param name="codeLocation" />
    
    <xsl:choose>
      <xsl:when test="$codeLocation/@path">
        <xsl:value-of select="$codeLocation/@path"/>
        <xsl:if test="$codeLocation/@line">
          <xsl:text>(</xsl:text>
          <xsl:value-of select="$codeLocation/@line"/>
          <xsl:if test="$codeLocation/@column">
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$codeLocation/@column"/>
          </xsl:if>
          <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>(unknown)</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Formats a CodeReference -->
  <xsl:template name="format-code-reference">
    <xsl:param name="codeReference" />
    
    <xsl:if test="$codeReference/@parameter">
      <xsl:text>Parameter </xsl:text>
      <xsl:value-of select="$codeReference/@parameter"/>
      <xsl:text> of </xsl:text>
    </xsl:if>
    
    <xsl:choose>
      <xsl:when test="$codeReference/@type">
        <xsl:value-of select="$codeReference/@type"/>
        <xsl:if test="$codeReference/@member">
          <xsl:text>.</xsl:text>
          <xsl:value-of select="$codeReference/@member"/>
        </xsl:if>
      </xsl:when>
      <xsl:when test="$codeReference/@namespace">
        <xsl:value-of select="$codeReference/@namespace"/>
      </xsl:when>
    </xsl:choose>
    
    <xsl:if test="$codeReference/@assembly">
      <xsl:if test="$codeReference/@namespace">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:value-of select="$codeReference/@assembly"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:key name="outcome-category" match="g:outcome" use="@category" />
  <xsl:template name="aggregate-statistics-outcome-summaries">
    <xsl:param name="status" />
    <xsl:param name="outcomes" />
    
    <xsl:for-each select="$outcomes[generate-id() = generate-id(key('outcome-category', @category)[1])]">
      <xsl:variable name="category" select="@category" />
      <g:outcomeSummary count="{count($outcomes[@category = $category])}">
        <g:outcome status="{$status}" category="{$category}" />
      </g:outcomeSummary>
    </xsl:for-each>
  </xsl:template>
  
  <!-- Indents text using the specified prefix -->
  <xsl:template name="indent">
    <xsl:param name="text" />
    <xsl:param name="firstLinePrefix" select="'  '" />
    <xsl:param name="otherLinePrefix" select="'  '" />
    <xsl:param name="trailingNewline" select="1" />

    <xsl:if test="$text!=''">
      <xsl:call-template name="indent-recursive">
        <xsl:with-param name="text" select="translate($text, '&#9;&#xA;&#xD;', ' &#xA;')" />
        <xsl:with-param name="firstLinePrefix" select="$firstLinePrefix" />
        <xsl:with-param name="otherLinePrefix" select="$otherLinePrefix" />
        <xsl:with-param name="trailingNewline" select="$trailingNewline" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="indent-recursive">
    <xsl:param name="text" />
    <xsl:param name="firstLinePrefix" />
    <xsl:param name="otherLinePrefix" />
    <xsl:param name="trailingNewline" />

    <xsl:choose>
      <xsl:when test="contains($text, '&#xA;')">
        <xsl:value-of select="$firstLinePrefix"/>
        <xsl:value-of select="substring-before($text, '&#xA;')"/>
        <xsl:text>&#xA;</xsl:text>
        <xsl:call-template name="indent-recursive">
          <xsl:with-param name="text" select="substring-after($text, '&#xA;')" />
          <xsl:with-param name="firstLinePrefix" select="$otherLinePrefix" />
          <xsl:with-param name="otherLinePrefix" select="$otherLinePrefix" />
          <xsl:with-param name="trailingNewline" select="$trailingNewline" />
        </xsl:call-template>
      </xsl:when>
      <!-- Note: when we are not adding a trailing new line, we must be careful
           to include the leading prefix space even when the text line is empty
           because subsequently appended text will assume that the current
           line is already indented appropriately -->
      <xsl:when test="$text!='' or not($trailingNewline)">
        <xsl:value-of select="$firstLinePrefix"/>
        <xsl:value-of select="$text"/>
        <xsl:if test="$trailingNewline">
          <xsl:text>&#xA;</xsl:text>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Prints text with <br/> at line breaks.
       Also introduces <wbr/> word break characters every so often if no natural breakpoints appear.
       Some of the soft breaks are useful, but the forced break at 20 chars is intended to work around
       limitations in browsers that do not support the word-wrap:break-all CSS3 property.
  -->
  <xsl:template name="print-text-with-breaks">
    <xsl:param name="text" />
    <xsl:param name="count" select="0" />
    
    <xsl:if test="$text">
      <xsl:variable name="char" select="substring($text, 1, 1)"/>
      
      <xsl:choose>
        <!-- natural word breaks -->
        <xsl:when test="$char = ' '">
          <xsl:choose>
            <!-- when we have leading spaces, use a non-breaking space instead so that it does not get collapsed into any previous spaces
                 this code generates effectively sequences of alternating non-breaking spaces and spaces so that the text behaves
                 almost as if it were preformatted -->
            <xsl:when test="$count = 0">
              <xsl:text>&#160;</xsl:text><wbr/>
              <xsl:call-template name="print-text-with-breaks">
                <xsl:with-param name="text" select="substring($text, 2)" />
                <xsl:with-param name="count" select="1" />
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> </xsl:text>
              <xsl:call-template name="print-text-with-breaks">
                <xsl:with-param name="text" select="substring($text, 2)" />
                <xsl:with-param name="count" select="0" />
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>

        <!-- line breaks -->
        <xsl:when test="$char = '&#10;'">
          <br/>
          <xsl:call-template name="print-text-with-breaks">
            <xsl:with-param name="text" select="substring($text, 2)" />
            <xsl:with-param name="count" select="0" />
          </xsl:call-template>
        </xsl:when>
        
        <!-- characters to break before -->
        <xsl:when test="$char = '.' or $char = '/' or $char = '\' or $char = ':' or $char = '(' or $char = '&lt;' or $char = '[' or $char = '{' or $char = '_'">
          <wbr/>
          <xsl:value-of select="$char"/>
          <xsl:call-template name="print-text-with-breaks">
            <xsl:with-param name="text" select="substring($text, 2)" />
            <xsl:with-param name="count" select="1" />
          </xsl:call-template>
        </xsl:when>
        
        <!-- characters to break after -->
        <xsl:when test="$char = ')' or $char = '>' or $char = ']' or $char = '}'">
          <xsl:value-of select="$char"/>
          <wbr/>
          <xsl:call-template name="print-text-with-breaks">
            <xsl:with-param name="text" select="substring($text, 2)" />
            <xsl:with-param name="count" select="0" />
          </xsl:call-template>
        </xsl:when>
        
        <!-- other characters -->
        <xsl:when test="$count = 19">
          <xsl:value-of select="$char"/>
          <wbr/>
          <xsl:call-template name="print-text-with-breaks">
            <xsl:with-param name="text" select="substring($text, 2)" />
            <xsl:with-param name="count" select="0" />
          </xsl:call-template>
        </xsl:when>
        
        <xsl:otherwise>
          <xsl:value-of select="$char"/>
          <xsl:call-template name="print-text-with-breaks">
            <xsl:with-param name="text" select="substring($text, 2)" />
            <xsl:with-param name="count" select="$count + 1" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  
    
</xsl:stylesheet>