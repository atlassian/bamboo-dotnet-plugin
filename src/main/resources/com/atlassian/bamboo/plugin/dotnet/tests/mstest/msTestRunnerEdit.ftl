[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='devenv' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
            list=uiConfigSupport.getExecutableLabels('devenv')
            extraUtility=addExecutableLink required='true' /]
[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
[@ww.textfield labelKey='mstest.container' name='mstestContainer' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='mstest.testMetadata' name='testmetadata' cssClass="long-field" /]
[@ww.textfield labelKey='mstest.resultsFile' name='resultsfile' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='mstest.runConfig' name='mstestRunConfig' cssClass="long-field"  /]