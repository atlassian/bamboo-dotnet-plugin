package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.util.NumberUtils;
import com.atlassian.bamboo.utils.FileVisitor;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.bamboo.v2.build.BaseConfigurableBuildPlugin;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildContextHelper;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Handles parsing NCover report files, and stores the coverage results within
 * the specific {@link BuildResultsSummary} custom data.
 *
 * @author Ross Rowe
 */
public class NCoverBuildProcessor extends BaseConfigurableBuildPlugin implements
																	  CustomBuildProcessor
{

	private static final Logger log = Logger
			.getLogger(NCoverBuildProcessor.class);

	private static final String NCOVER_PATH = "custom.ncover.path";

	public static final String NCOVER_EXISTS = "custom.ncover.exists";

	public static final String NCOVER_XML_PATH_KEY = NCOVER_PATH;

	public static final String NCOVER_LINE_COVERAGE = "NCOVER_LINE_COVERAGE";
	public static final String NCOVER_ASSEMBLIES = "NCOVER_ASSEMBLIES";
	public static final String NCOVER_LOC = "NCOVER_LOC";
	public static final String NCOVER_LE = "NCOVER_LE";
	public static final String NCOVER_LNE = "NCOVER_LNE";

	public static final String NCOVER_COVERAGE_DELTA = "NCOVER_COVERAGE_DELTA";

	public static final String NCOVER_RESULT_CONTENTS = "NCOVER_RESULT_CONTENTS";

	public static final String NCOVER_METHODS = "NCOVER_METHODS";

	public static final String NCOVER_CLASSES = "NCOVER_CLASSES";

	public static final String NCOVER_COVERAGE_CHANGES = "NCOVER_COVERAGE_CHANGES";

	public static final String NCOVER_LINE_RATE = "NCOVER_LINE_RATE";

	public static final String LINE_SEPARATOR = System
			.getProperty("line.separator");

	public static final String BUILD_NUMBER = "buildNumber";

	public static final String BUILD_KEY = "buildKey";

	public static final String NCOVER_POSTBUILD_XMPP = "custom.bamboo.ncover.postbuild.xmpp";

	public static final String NCOVER_POSTBUILD_THRESHOLD = "threshold";

	public static final String NCOVER_POSTBUILD_EMAIL = "custom.bamboo.ncover.postbuild.email";

	public static final String NCOVER_POSTBUILD = "custom.bamboo.ncover.postbuild";

	/**
	 * {@link FileVisitor} subclass that handles identifying and parsing NCover
	 * report files.
	 *
	 * @author Ross Rowe
	 */
	private final class NCoverCoverageFileVisitor extends FileVisitor {

		private final Map<String, String> results;

		/**
		 * Constructs a NCoverCoverageFileVisitor.
		 *
		 * @param file
		 * @param results
		 */
		private NCoverCoverageFileVisitor(File file, Map<String, String> results) {
			super(file);
			this.results = results;
		}

		/**
		 * Run the parse over the <code>file</code> if the <code>file</code>'s
		 * name ends with 'xml'.
		 *
		 * @param file
		 *            file being visted
		 */
		@Override
		public void visitFile(File file) {
			if (file.getName().endsWith("xml")) {
				runParse(file);
			}
		}

		/**
		 * Parses the NCover XML report file.
		 *
		 * @param file
		 */
		private void runParse(final File file) {
			try {
				log.info("Parsing file: " + file.getAbsolutePath());
				NCoverReportParser parser = new NCoverReportParser();
				parser.parse(file);
				storeResultsFromParser(parser);
			} catch (Exception e) {
				log.error("Failed to parse artifact result file \""
						+ file.getName() + "\"", e);
			}
		}

		/**
		 * Stores the variables from the {@link NCoverReportParser} instance
		 * within the <code>results</code> map.
		 *
		 * @param parser
		 *            Report File parser
		 */
		private void storeResultsFromParser(NCoverReportParser parser)
		{
			double coverage = NumberUtils.round(parser.getLineRate() * 100, 4);

			// store CSV instead of XML to cut down on the file size (the
			// CUSTOM_INFO_DATA column of the BUILDRESULTSUMMARY_CUSTOMDATA
			// table has a 4000 character limit)
			NumberFormat defaultFormat = NumberFormat.getInstance();
			appendResults(NCOVER_RESULT_CONTENTS, parser.getCsv());
			appendDoubleResults(NCOVER_LINE_RATE, defaultFormat
					.format(coverage));
			appendLongResults(NCOVER_METHODS, Long
					.toString(parser.getMethods()));
			appendLongResults(NCOVER_CLASSES, Long
					.toString(parser.getClasses()));
			appendLongResults(NCOVER_ASSEMBLIES, Long.toString(parser
					.getAssemblies()));
			appendLongResults(NCOVER_LOC, Long
					.toString(parser.getLinesOfCode()));
			appendLongResults(NCOVER_LE, Long.toString(parser
					.getLinesExecuted()));
			appendLongResults(NCOVER_LNE, Long.toString(parser
					.getLinesNotExecuted()));
		}

		private void appendResults(String key, String results) {
			log.debug("Appending " + results + " to " + key);
			String existingResults = this.results.get(key);
			if (existingResults == null)
				existingResults = "";
			if (results == null)
				results = "";
			String newValue = existingResults + results;
			this.results.put(key, newValue);
		}

		private void appendLongResults(String key, String results) {
			log.debug("Appending " + results + " to " + key);
			String existingResults = this.results.get(key);
			if (existingResults == null || existingResults.equals(""))
				existingResults = Long.toString(0);
			if (results == null || results.equals(""))
				results = Long.toString(0);
			Long newValue = NumberUtils.stringToLong(existingResults)
					+ NumberUtils.stringToLong(results);
			NumberFormat defaultFormat = NumberFormat.getInstance();
			this.results.put(key, defaultFormat.format(newValue));
		}

		private void appendDoubleResults(String key, String results) {
			log.debug("Appending " + results + " to " + key);
			boolean storeAverage = true;
			String existingResults = this.results.get(key);
			if (existingResults == null || existingResults.equals("")) {
				existingResults = Double.toString(0);
				storeAverage = false;
			}
			if (results == null || results.equals(""))
				results = Double.toString(0);
			Double newValue = NumberUtils.unlocalizedStringToDouble(existingResults)
					+ NumberUtils.unlocalizedStringToDouble(results);
			if (storeAverage)
				newValue = newValue / 2;
			NumberFormat defaultFormat = NumberFormat.getInstance();
			this.results.put(key, defaultFormat.format(newValue));
		}

	}

	/**
	 * Validates whether the <code>configuration</code> is valid.
	 *
	 */
	@Override
	public ErrorCollection validate(BuildConfiguration configuration) {
		ErrorCollection ec = new SimpleErrorCollection();
		if (configuration.getBoolean(NCOVER_EXISTS)
				&& StringUtils.isBlank(configuration.getString(NCOVER_PATH))) {
			ec
					.addError(NCOVER_PATH,
							"Please specify the directory containing the XML NCover output files.");
		}
		return ec;
	}

	/**
	 * Processes the build results to run the {@link NCoverReportParser} over
	 * the result files, and stores the NCover results within the
	 * <code>buildResult</code>'s customBuildData.
	 */
	@Override
	@NotNull
	public BuildContext call() throws Exception {
		log.info("inside NCoverBuildProcessor.call()");
		final Map<String, String> ncoverResults = new HashMap<>();

		CurrentBuildResult buildResult = buildContext.getBuildResult();
		if (buildResult != null) {

			final Map<String, String> customConfiguration = buildContext.getBuildDefinition()
					.getCustomConfiguration();
			if (Boolean.parseBoolean(customConfiguration.get(NCOVER_EXISTS)) && customConfiguration.containsKey(NCOVER_XML_PATH_KEY)) {
				String pathPattern = customConfiguration.get(NCOVER_XML_PATH_KEY);

				if (!StringUtils.isEmpty(pathPattern)) {
					File planSourceDirectory = BuildContextHelper.getBuildWorkingDirectory((CommonContext)buildContext);
					FileVisitor fileVisitor = new NCoverCoverageFileVisitor(
							planSourceDirectory, ncoverResults);
					log.info("Running NCover Build Processor over source dir:"
							+ planSourceDirectory.getAbsolutePath()
							+ " path pattern: " + pathPattern);
					fileVisitor.visitFilesThatMatch(pathPattern);
					if (ncoverResults.isEmpty()) {
						log
								.error("Could not find any NCover results in source dir:"
										+ planSourceDirectory.getAbsolutePath()
										+ " path pattern: " + pathPattern);
					} else {
						buildResult.getCustomBuildData().putAll(ncoverResults);
					}
				}
			}

		}
		return buildContext;
	}

	@NotNull
	public Set<String> getConfigurationKeys()
	{
		return ImmutableSet.of(NCOVER_PATH, NCOVER_EXISTS);
	}

	@Override
	public boolean isConfigurationMissing(@NotNull final HierarchicalConfiguration configuration)
	{
		return !configuration.containsKey(NCOVER_EXISTS);
	}
}
