package com.atlassian.bamboo.plugin.dotnet.tests.mstest;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.plugin.dotnet.visualstudio.VisualStudioTaskType;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MSTestRunnerConfigurator extends AbstractDotNetTaskConfigurator implements BuildTaskRequirementSupport, TaskTestResultsSupport
{
    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            MSTestRunnerTaskType.LABEL,
            MSTestRunnerTaskType.CONTAINER,
            MSTestRunnerTaskType.RUN_CONFIG,
            MSTestRunnerTaskType.RESULTS_FILE,
            MSTestRunnerTaskType.TEST_METADATA,
            MSTestRunnerTaskType.ENVIRONMENT
    );
    private static final String DEFAULT_TEST_FILE = "testresults.trx";

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        final String label = params.getString(MSTestRunnerTaskType.LABEL);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(MSTestRunnerTaskType.LABEL, textProvider.getText("mstest.label.error"));
        }

        final String container = params.getString(MSTestRunnerTaskType.CONTAINER);
        final String testMetadata = params.getString(MSTestRunnerTaskType.TEST_METADATA);

        if (StringUtils.isNotEmpty(container) && StringUtils.isNotEmpty(testMetadata))
        {
            errorCollection.addError(MSTestRunnerTaskType.TEST_METADATA, textProvider.getText("mstest.has.both.testMetadata.container"));
            errorCollection.addError(MSTestRunnerTaskType.CONTAINER, textProvider.getText("mstest.has.both.testMetadata.container"));
        }
        else if (StringUtils.isEmpty(container) && StringUtils.isEmpty(testMetadata))
        {
            errorCollection.addError(MSTestRunnerTaskType.TEST_METADATA, textProvider.getText("mstest.has.neither.testMetadata.container"));
            errorCollection.addError(MSTestRunnerTaskType.CONTAINER, textProvider.getText("mstest.has.neither.testMetadata.container"));
        }

        final String resultsFile = params.getString(MSTestRunnerTaskType.RESULTS_FILE);
        if (StringUtils.isEmpty(resultsFile))
        {
            errorCollection.addError(MSTestRunnerTaskType.RESULTS_FILE, textProvider.getText("mstest.resultsFile.error"));
        }

        if (StringUtils.isNotBlank(resultsFile) && !resultsFile.endsWith(".trx"))
        {
            errorCollection.addError(MSTestRunnerTaskType.RESULTS_FILE, textProvider.getText("mstest.resultsFile.missingExtension"));
        }
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(MSTestRunnerTaskType.RESULTS_FILE, DEFAULT_TEST_FILE);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }


    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull final TaskDefinition taskDefinition, @NotNull final Job job)
    {
        final Set<Requirement> requirements = new HashSet<>();
        taskConfiguratorHelper.addSystemRequirementFromConfiguration(requirements, taskDefinition,
                                                                     MSTestRunnerTaskType.LABEL, VisualStudioTaskType.VS_CAPABILITY_PREFIX);
        return requirements;
    }

    @Override
    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition)
    {
        return true;
    }
}
