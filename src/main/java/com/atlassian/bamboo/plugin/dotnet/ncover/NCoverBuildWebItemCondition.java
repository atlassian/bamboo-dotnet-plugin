package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableBuildable;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.web.Condition;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Contains logic to control whether a 'NCover' tab is displayed on the Build
 * results page.
 *
 * @author Ross Rowe
 *
 */
public class NCoverBuildWebItemCondition implements Condition {

	@BambooImport
	@Autowired
	private CachedPlanManager cachedPlanManager;

	@Override
    public void init(Map map) throws PluginParseException {

	}

	/**
	 * Only display the NCover tab item if the appropriate configuration
	 * settings have been set.
	 *
	 * @return boolean indicating whether NCover tab item should be displayed
	 */
	@Override
    public boolean shouldDisplay(Map context) {

		String buildKey = (String) context.get(NCoverBuildProcessor.BUILD_KEY);

		if (buildKey == null)
        {
			return false;
        }

        ImmutableBuildable build = cachedPlanManager.getPlanByKeyIfOfType(PlanKeys.getPlanKey(buildKey), ImmutableBuildable.class);
		if (build != null)
        {
            Map<String, String> customConfiguration = build.getBuildDefinition().getCustomConfiguration();

            if (customConfiguration != null)
            {
				return StringUtils.isNotEmpty(customConfiguration.get(NCoverBuildProcessor.NCOVER_EXISTS));
            }
        }

		return false;
	}

}
