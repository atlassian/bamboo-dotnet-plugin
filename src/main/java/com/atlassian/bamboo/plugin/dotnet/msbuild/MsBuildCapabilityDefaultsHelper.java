package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class MsBuildCapabilityDefaultsHelper implements CapabilityDefaultsHelper
{
    private static final Logger log = Logger.getLogger(MsBuildCapabilityDefaultsHelper.class);
    private static final Pattern IS_VERSION_NUMBER = Pattern.compile("[0-9.]+");

    private enum DotNetFrameworkType
    {
        ARCH_32_BIT,
        ARCH_64_BIT
    }

    @Override
    @NotNull
    public CapabilitySet addDefaultCapabilities(@NotNull final CapabilitySet capabilitySet)
    {
        if (!SystemUtils.IS_OS_WINDOWS)
        {
            return capabilitySet;
        }

        addMsBuildPre12Capability(capabilitySet);

        addMsBuild12PlusCapabilities(capabilitySet);

        return capabilitySet;
    }

    //MsBuild versions 12+ are installed in a different location
    private void addMsBuild12PlusCapabilities(@NotNull final CapabilitySet capabilitySet)
    {
        final Optional<File> programFiles = WindowsSystemPaths.PROGRAM_FILES.get();

        programFiles.ifPresent(
                programFilesFolder ->
                {
                    final File msBuildDirectory = new File(programFilesFolder, "MSBuild");
                    processFrameworkDirForMsBuild12Plus(capabilitySet, msBuildDirectory);
                }
        );

        final Optional<File> programFilesX86 = WindowsSystemPaths.PROGRAM_FILES_X86.get();

        programFilesX86.ifPresent(
                programFilesX86Folder -> {
                    if (!programFiles.isPresent() || !programFiles.get().getAbsoluteFile().equals(programFilesX86Folder.getAbsoluteFile()))
                    {
                        final File msBuildDirectory = new File(programFilesX86Folder, "MSBuild");
                        processFrameworkDirForMsBuild12Plus(capabilitySet, msBuildDirectory);
                    }
                }
        );
    }

    private static void addMsBuildPre12Capability(@NotNull final CapabilitySet capabilitySet)
    {
        WindowsSystemPaths.WINDOWS.get().ifPresent(
                windowsFolder -> {
                    final File netFramework32 = new File(windowsFolder, "\\Microsoft.NET\\Framework");
                    processFrameworkDirForMsBuildPre12(capabilitySet, netFramework32, DotNetFrameworkType.ARCH_32_BIT);

                    final File netFramework64 = new File(windowsFolder, "\\Microsoft.NET\\Framework64");
                    processFrameworkDirForMsBuildPre12(capabilitySet, netFramework64, DotNetFrameworkType.ARCH_64_BIT);
                }
        );
    }

    private static void processFrameworkDirForMsBuildPre12(@NotNull final CapabilitySet capabilitySet, @NotNull final File frameworkDirectory, @NotNull final DotNetFrameworkType dotNetFrameworkType)
    {
        subdirectoriesOf(frameworkDirectory)
                .filter(subDir -> subDir.getName().startsWith("v"))
                .forEach(subDir -> addMSBuildCapability(capabilitySet, subDir, dotNetFrameworkType));
    }

    private static void processFrameworkDirForMsBuild12Plus(@NotNull final CapabilitySet capabilitySet, final File frameworkDirectory)
    {
        subdirectoriesOf(frameworkDirectory)
                .filter(subDir -> IS_VERSION_NUMBER.matcher(subDir.getName()).matches())
                .forEach(subDir -> processFrameworkSubdirFileSince12(capabilitySet, subDir));
    }

    private static void processFrameworkSubdirFileSince12(@NotNull final CapabilitySet capabilitySet, final File subDirectory)
    {
        final String frameworkVersion = subDirectory.getName();
        final String friendlyVersion = frameworkVersion.length() >= 5 ? frameworkVersion.substring(0, 5) : frameworkVersion;
        final String labelPrefix = "MSBuild v" + friendlyVersion;

        final File msBuildPath32 = new File(subDirectory + "/bin", "MSBuild.exe");
        if (msBuildPath32.exists())
        {
            addMSBuilCapability(capabilitySet, msBuildPath32, labelPrefix + " (32bit)");
        }

        final File msBuildPath64 = new File(subDirectory + "/bin/amd64", "MSBuild.exe");
        if (msBuildPath64.exists())
        {
            addMSBuilCapability(capabilitySet, msBuildPath64, labelPrefix + " (64bit)");
        }
    }

    private static void addMSBuildCapability(@NotNull final CapabilitySet capabilitySet, @NotNull final File frameworkDirectory, @NotNull final DotNetFrameworkType dotNetFrameworkType)
    {
        final File msBuildPath = new File(frameworkDirectory, "MSBuild.exe");
        if (msBuildPath.exists())
        {
            final String frameworkVersion = frameworkDirectory.getName().substring(1);
            final String friendlyVersion = frameworkVersion.length() >= 3 ? frameworkVersion.substring(0, 3) : frameworkVersion;
            final String architecture = dotNetFrameworkType == DotNetFrameworkType.ARCH_64_BIT ? "64bit" : "32bit";
            final String label = "MSBuild v" + friendlyVersion + " (" + architecture + ')';
            addMSBuilCapability(capabilitySet, msBuildPath, label);
        }
    }

    private static void addMSBuilCapability(@NotNull final CapabilitySet capabilitySet, @NotNull final File msBuildPath, @NotNull final String label)
    {
        final Capability capability = new CapabilityImpl(MsBuildTaskType.MSBUILD_CAPABILITY_PREFIX + '.' + label, msBuildPath.getAbsolutePath());
        capabilitySet.addCapability(capability);
    }

    private static Stream<File> subdirectoriesOf(final File frameworkDirectory)
    {
        if (!frameworkDirectory.isDirectory())
        {
            log.warn("Framework directory " + frameworkDirectory + " is not a directory");
            return Stream.empty();
        }
        final File[] files = frameworkDirectory.listFiles();
        if (files==null)
        {
            log.warn("Framework directory " + frameworkDirectory + " could not be listed");
            return Stream.empty();
        }
        return Arrays.stream(files).filter(File::isDirectory);
    }
}
