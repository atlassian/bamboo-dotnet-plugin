package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.build.ViewBuildResults;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

/**
 * NCover-specifc {@link ViewBuildResults} subclass that handles populating
 * the <code>coverageChanges</code> collection when servicing requests to
 * display the NCover Build results tab.
 * 
 * @author Ross Rowe
 * 
 */
public class ViewNCoverBuildResults extends ViewBuildResults {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ViewNCoverBuildResults.class);

	private Collection<NCoverCoverageInformation> coverageChanges = new ArrayList<>();


	/**
	 * Performs the super.execute(), and populates the
	 * <code>coverageChanges</code> variable.
	 * 
	 * @throws Exception
	 *             if an error occurs processing the action
	 */
	@Override
	public String execute() throws Exception {
		String superResult = super.execute();

		if (ERROR.equals(superResult)) {
			return ERROR;
		}

		populateCoverageChanges(getBuildResultsSummary());

		return superResult;
	}

	/**
	 * Reads the NCover Coverage Changes from the {@link BuildResultsSummary}'s
	 * customBuildData, and adds corresponding
	 * {@link NCoverCoverageInformation} instances into the
	 * <code>coverageChanges</code> variable.
	 */
	private void populateCoverageChanges(BuildResultsSummary buildResultsSummary)
    {
		String csv = buildResultsSummary.getCustomBuildData().get(NCoverBuildProcessor.NCOVER_COVERAGE_CHANGES);
		if (StringUtils.isNotEmpty(csv)) {
			BufferedReader reader = new BufferedReader(new StringReader(csv));
			String line;
			try {
				while ((line = reader.readLine()) != null) {
					StringTokenizer tokenizer = new StringTokenizer(line, ",");
					String className = tokenizer.nextToken();
					Double lineRate = NumberUtils.unlocalizedStringToDouble(tokenizer.nextToken());
					Double delta = NumberUtils.unlocalizedStringToDouble(tokenizer.nextToken());
					//lineRate = NumberUtils.round(lineRate * 100, 2);
					//delta = NumberUtils.round(delta * 100, 2);
					coverageChanges.add(new NCoverCoverageInformation(
							className, lineRate, delta));
				}
			} catch (NumberFormatException | IOException e) {
				log.error(e);
			}
		}
	}

	public Collection getCoverageChanges() {
		return coverageChanges;
	}

}
