package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.ChainExecution;
import com.atlassian.bamboo.chains.ChainResultsSummary;
import com.atlassian.bamboo.chains.ChainStageResult;
import com.atlassian.bamboo.chains.plugins.PostChainAction;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.util.NumberUtils;
import org.jetbrains.annotations.NotNull;

public class NCoverChainResultAggregator implements PostChainAction
{

    @Override
    public void execute(@NotNull final Chain chain, @NotNull final ChainResultsSummary chainResultsSummary, @NotNull final ChainExecution chainExecution)
    {
        execute((ImmutableChain)chain, chainResultsSummary, chainExecution);
    }

    //deliberately skipping override annotation
    public void execute(@NotNull ImmutableChain immutableChain, @NotNull ChainResultsSummary chainResultsSummary, @NotNull ChainExecution chainExecution) {
        if (!chainResultsSummary.isSuccessful())
        {
            return;
        }
        double aggregatedLineRate = 0;
        int aggregatedLineRateCnt = 0;

        for (ChainStageResult stageResult : chainResultsSummary.getStageResults())
        {
            for (BuildResultsSummary buildResult : stageResult.getBuildResults())
            {
                final Double lineRate = NumberUtils.createDoubleQuietly(buildResult.getCustomBuildData().get(NCoverBuildProcessor.NCOVER_LINE_RATE));

                if (lineRate != null)
                {
                    aggregatedLineRate += lineRate;
                    aggregatedLineRateCnt++;
                }
            }
        }
        if (aggregatedLineRateCnt > 0)
        {
            chainResultsSummary.getCustomBuildData().put(NCoverBuildProcessor.NCOVER_LINE_RATE, Double.toString(aggregatedLineRate / aggregatedLineRateCnt));
        }
    }
}
