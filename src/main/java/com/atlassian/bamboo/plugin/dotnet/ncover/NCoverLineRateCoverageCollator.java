package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.chains.ChainResultsSummary;
import com.atlassian.bamboo.charts.collater.TimePeriodCollater;
import com.atlassian.bamboo.charts.timeperiod.AbstractTimePeriodCollater;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultStatisticsProvider;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.util.NumberUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * <code>AbstractTimePeriodCollater</code> subclass that collects line rate
 * data for use in reports.
 * @author Ross Rowe
 *
 */
public class NCoverLineRateCoverageCollator extends
		AbstractTimePeriodCollater implements TimePeriodCollater {

    private static final Logger log = Logger.getLogger(NCoverLineRateCoverageCollator.class);

    private int count;

	private double coverage;

	/**
	 * Retrieves the branch rate from the <code>result</code>'s custom build
	 * data, and adds it to the <code>coverage</code> instance variable.
	 * @param result
	 */
	@Override
    public void addResult(ResultStatisticsProvider result)
    {
        log.debug("Inside addResult");
		final BuildResultsSummary results = Narrow.downTo(result, BuildResultsSummary.class);
		if (results != null) {
			addJobResult(results);
		}
		final ChainResultsSummary chainResultsSummary = Narrow.downTo(result, ChainResultsSummary.class);
		if (chainResultsSummary != null) {
			chainResultsSummary.getOrderedJobResultSummaries().forEach(this::addJobResult);
		}
	}

	private void addJobResult(@NotNull ResultsSummary summary) {
		Double nCoverLineRate = NumberUtils.createDoubleQuietly(summary.getCustomBuildData().get(NCoverBuildProcessor.NCOVER_LINE_RATE));
		if (nCoverLineRate != null)
		{
			log.debug("Adding line rate: " + nCoverLineRate);
			coverage = coverage + nCoverLineRate;
			count++;
		}
	}

	/**
	 * Retrieves the daily average coverage.
	 * 
	 * @return
	 */
	@Override
    public double getValue() {
		if (count == 0)
			return 0.0D;
		else
			return coverage / (double) count;
	}

}
