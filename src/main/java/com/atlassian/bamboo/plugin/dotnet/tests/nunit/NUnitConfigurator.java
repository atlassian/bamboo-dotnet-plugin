package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

public class NUnitConfigurator extends AbstractDotNetTaskConfigurator implements TaskTestResultsSupport
{
    private static final String DEFAULT_TEST_DIRECTORY = "**/test-reports/*.xml";

    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            NUnitCollectorTaskType.TEST_DIRECTORY, TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE
    );

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        final String testDirectory = params.getString(NUnitCollectorTaskType.TEST_DIRECTORY);
        if (StringUtils.isEmpty(testDirectory))
        {
            errorCollection.addError(NUnitCollectorTaskType.TEST_DIRECTORY, textProvider.getText("nunit.testDirectory.error"));
        }
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(NUnitCollectorTaskType.TEST_DIRECTORY, DEFAULT_TEST_DIRECTORY);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }

    @Override
    public boolean taskProducesTestResults(@NotNull final TaskDefinition taskDefinition)
    {
        return true;
    }
}
