package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.plugin.dotnet.tests.AbstractDotNetTestReportCollector;
import com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParser;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class NUnitTestReportCollector extends AbstractDotNetTestReportCollector
{
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(NUnitTestReportCollector.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods

    @NotNull
    @Override
    protected TestResultsParser getParser()
    {
        return new NUnitXmlTestResultsParser();
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
