package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public class NUnitCollectorTaskExporter implements TaskDefinitionExporter {
    private static final ValidationContext NUNIT_PARSER_CONTEXT = ValidationContext.of("NUnit parser task");

    @NotNull
    @Override
    public TestParserTask toSpecsEntity(final TaskDefinition taskDefinition) {
        TestParserTask testParserTask = TestParserTask.createNUnitParserTask();
        final Map<String, String> configuration = taskDefinition.getConfiguration();
        final String testDirectory = configuration.get(NUnitCollectorTaskType.TEST_DIRECTORY);
        if (!StringUtils.isEmpty(testDirectory)) {
            Splitter.on(',').trimResults().split(testDirectory).forEach(testParserTask::resultDirectories);
        }
        final String pickupOutdatedFiles = configuration.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE);
        if (!StringUtils.isEmpty(pickupOutdatedFiles)) {
            testParserTask.pickUpTestResultsCreatedOutsideOfThisBuild(Boolean.valueOf(pickupOutdatedFiles));
        }
        return testParserTask;
    }

    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, final TaskProperties taskProperties) {
        final TestParserTaskProperties testParserTaskProperties = Narrow.downTo(taskProperties, TestParserTaskProperties.class);
        if (testParserTaskProperties != null) {
            final Map<String, String> cfg = new HashMap<>();
            if (testParserTaskProperties.getResultDirectories() != null) {
                cfg.put(NUnitCollectorTaskType.TEST_DIRECTORY, getResultPath(testParserTaskProperties));
            }
            cfg.put(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, Boolean.toString(testParserTaskProperties.getPickUpTestResultsCreatedOutsideOfThisBuild()));
            return cfg;
        }
        throw new IllegalStateException("Don't know how to import task properties of type: " + taskProperties.getClass().getName());
    }

    private String getResultPath(TestParserTaskProperties testParserTaskProperties) {
        return testParserTaskProperties.getResultDirectories().stream().collect(joining(","));
    }

    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        List<ValidationProblem> result = new ArrayList<>();
        final TestParserTaskProperties testParserTaskProperties = Narrow.downTo(taskProperties, TestParserTaskProperties.class);

        if (testParserTaskProperties != null) {
            if (StringUtils.isEmpty(getResultPath(testParserTaskProperties))) {
                result.add(new ValidationProblem(NUNIT_PARSER_CONTEXT, "Result path is mandatory"));
            }
        }

        return result;
    }
}
