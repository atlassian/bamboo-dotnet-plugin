package com.atlassian.bamboo.plugin.dotnet.tests.mstest;

import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugin.dotnet.visualstudio.VisualStudioTaskType;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.utils.process.ExternalProcess;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MSTestRunnerTaskType implements TaskType
{
    public static final String LABEL = "label";
    public static final String RUN_CONFIG = "mstestRunConfig";
    public static final String CONTAINER = "mstestContainer";
    public static final String RESULTS_FILE = "resultsfile";
    public static final String TEST_METADATA = "testmetadata";
    public static final String ENVIRONMENT = "environmentVariables";

    private final ProcessService processService;
    private final TestCollationService testCollationService;
    private final CapabilityContext capabilityContext;
    private final EnvironmentVariableAccessor environmentVariableAccessor;

    public MSTestRunnerTaskType(final ProcessService processService, final TestCollationService testCollationService, final CapabilityContext capabilityContext, final EnvironmentVariableAccessor environmentVariableAccessor)
    {
        this.processService = processService;
        this.testCollationService = testCollationService;
        this.capabilityContext = capabilityContext;
        this.environmentVariableAccessor = environmentVariableAccessor;
    }


    @Override
    @NotNull
    public TaskResult execute(@NotNull final TaskContext taskContext)
    {
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);
        try
        {
            final ConfigurationMap configuration = taskContext.getConfigurationMap();

            final String label = configuration.get(LABEL);
            Preconditions.checkNotNull(label, "You must select a Visual Studio executable before this task can execute.");

            final String runConfig = configuration.get(RUN_CONFIG);
            Preconditions.checkNotNull(runConfig, "Run Configuration");

            final String container = configuration.get(CONTAINER);

            final String testMetadata = configuration.get(TEST_METADATA);

            final String resultsfile = configuration.get(RESULTS_FILE);
            Preconditions.checkNotNull(resultsfile, "Results File Name");

            final Capability capability = capabilityContext.getCapabilitySet().getCapability(VisualStudioTaskType.VS_CAPABILITY_PREFIX + "." + label);
            Preconditions.checkNotNull(capability, "Capability");

            final File msTestExe = new File(capability.getValue(), "MSTest.exe");
            Preconditions.checkArgument(msTestExe.exists(), msTestExe.getAbsolutePath() + " cannot be found");

            final List<String> command = new ArrayList<>();

            command.add(msTestExe.getAbsolutePath());
            if (StringUtils.isNotEmpty(container))
            {
                command.add("/testcontainer:" + container);
            }
            else if (StringUtils.isNotEmpty(testMetadata))
            {
                command.add("/testmetadata:" + testMetadata);
            }
            else
            {
                throw new IllegalStateException("Configuration is not valid. Neither a Test Container or a path to a Test Metadata file were specified");
            }

            command.add("/resultsfile:" + resultsfile);
            
            if (StringUtils.isNotEmpty(runConfig))
            {
                command.add("/runconfig:" + runConfig);
            }

            final String environment = configuration.get(ENVIRONMENT);

            final Map<String, String> env = environmentVariableAccessor.splitEnvironmentAssignments(environment);

            final ExternalProcess externalProcess = processService.executeExternalProcess(taskContext,
                                                                                  new ExternalProcessBuilder()
                                                                                          .command(command)
                                                                                          .env(env)
                                                                                          .workingDirectory(taskContext.getWorkingDirectory()));

            testCollationService.collateTestResults(taskContext, resultsfile, new MSTestTestReportCollector());

            return TaskResultBuilder.newBuilder(taskContext)
                    .checkTestFailures()
                    .checkReturnCode(externalProcess)
                    .build();
        }
        finally
        {
            taskContext.getBuildContext().getBuildResult().addBuildErrors(errorLines.getErrorStringList());
        }
    }
}
