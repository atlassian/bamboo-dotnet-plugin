package com.atlassian.bamboo.plugin.dotnet.tests.vstest;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetManager;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Bill Tutt
 */
public class VSTestRunnerConfigurator extends AbstractDotNetTaskConfigurator implements BuildTaskRequirementSupport, TaskTestResultsSupport
{
    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            VSTestRunnerTaskType.LABEL,
            VSTestRunnerTaskType.SETTINGS_FILE,
            VSTestRunnerTaskType.ENABLECODECOVERAGE,
            VSTestRunnerTaskType.INISOLATION,
            VSTestRunnerTaskType.PLATFORM,
            VSTestRunnerTaskType.FRAMEWORK,
            VSTestRunnerTaskType.TESTCASEFILTER,
            VSTestRunnerTaskType.TESTNAMES,
            VSTestRunnerTaskType.TESTFILE,
            VSTestRunnerTaskType.ENVIRONMENT,
            //NOTE: Array configs can't use the helper methods
            //VSTestRunnerTaskType.DISCOVERERS,
            VSTestRunnerTaskType.RESULTS_DIR,
            VSTestRunnerTaskType.RESULTS_FILE,
            VSTestRunnerTaskType.USE_VSIX_EXTENSIONS,
            VSTestRunnerTaskType.ADDITIONAL_ARGS
    );

    private static final String DEFAULT_TEST_FILE = "testresults.trx";
    private static final String[] platforms = new String[] {"", "x86", "x64", "ARM" };
    private static final String[] frameworks = new String[] {"", "Framework35", "Framework40", "Framework45", "FrameworkUap10"};

    private static final String PLATFORMS = "platforms";
    private static final String FRAMEWORKS = "frameworks";
    private static final String DISCOVERERS = "discoverers";

    @Autowired
    @BambooImport
    private CapabilitySetManager capabilitySetManager;

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        final String label = params.getString(VSTestRunnerTaskType.LABEL);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(VSTestRunnerTaskType.LABEL, textProvider.getText("vstest.label.error"));
        }

        final String testFile = params.getString(VSTestRunnerTaskType.TESTFILE);

        if (StringUtils.isEmpty(testFile))
        {
            errorCollection.addError(VSTestRunnerTaskType.TESTFILE, textProvider.getText("vstest.requires.testfile"));
        }

        final String resultsFile = params.getString(VSTestRunnerTaskType.RESULTS_FILE);

        if (StringUtils.isEmpty(resultsFile))
        {
            errorCollection.addError(VSTestRunnerTaskType.RESULTS_FILE, textProvider.getText("vstest.resultsFile.error"));
        }

        if (StringUtils.isNotBlank(resultsFile) && !resultsFile.endsWith(".trx"))
        {
            errorCollection.addError(VSTestRunnerTaskType.RESULTS_FILE, textProvider.getText("vstest.resultsFile.missingExtension"));
        }

        final String platform = params.getString(VSTestRunnerTaskType.PLATFORM);

        // Platform is optional, but verify that the value is valid.
        if (StringUtils.isNotEmpty(platform) && !ArrayUtils.contains(platforms, platform))
        {
            errorCollection.addError(VSTestRunnerTaskType.PLATFORM, textProvider.getText("vstest.invalid.platform"));
        }

        final String framework = params.getString(VSTestRunnerTaskType.FRAMEWORK);

        if (StringUtils.isNotEmpty(framework) && !ArrayUtils.contains(frameworks, framework))
        {
            errorCollection.addError(VSTestRunnerTaskType.FRAMEWORK, textProvider.getText("vstest.invalid.framework"));
        }
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull final TaskDefinition taskDefinition, @NotNull final Job job)
    {
        Set<Requirement> requirements = new HashSet<>();
        taskConfiguratorHelper.addSystemRequirementFromConfiguration(requirements, taskDefinition,
                VSTestRunnerTaskType.LABEL, VSTestRunnerTaskType.VSTESTEXE_CAPABILITY_PREFIX);
        List<String> discoverers = getDiscoverKeysFromTaskDefinition(taskDefinition);
        Map<String, String> config = taskDefinition.getConfiguration();
        for (String discoverer: discoverers)
        {
            requirements.add(new RequirementImpl(config.get(discoverer), true, ".*", true));
            // The system requirement helper method below doesn't tend to work for custom Capability Types.
            // taskConfiguratorHelper.addSystemRequirementFromConfiguration(requirements, taskDefinition,
            // config.get(discoverer), VSTestDiscoverCapabilityTypeModule.VSTEST_CAPABILITY_PREFIX);
        }
        return requirements;
    }

    @Override
    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition)
    {
        return true;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(PLATFORMS, platforms);
        context.put(FRAMEWORKS, frameworks);
        context.put(DISCOVERERS, getTestDiscoverers());

        // Defaults:
        context.put(VSTestRunnerTaskType.RESULTS_FILE, DEFAULT_TEST_FILE);
        context.put(VSTestRunnerTaskType.INISOLATION, true);
        context.put(VSTestRunnerTaskType.FRAMEWORK, "");
        context.put(VSTestRunnerTaskType.PLATFORM, "");
        context.put(VSTestRunnerTaskType.DISCOVERERS, new String[0]);
        context.put(VSTestRunnerTaskType.USE_VSIX_EXTENSIONS, "true");
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        populateContextWithConfiguration(context, taskDefinition);
        context.put(PLATFORMS, platforms);
        context.put(FRAMEWORKS, frameworks);
        context.put(DISCOVERERS, getTestDiscoverers());
    }

    private void populateContextWithConfiguration(Map<String, Object> context, TaskDefinition taskDefinition)
    {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        // You have to move array data on your own.
        List<String> discoverKeys = getDiscoverKeysFromTaskDefinition(taskDefinition);
        Map<String, String> config = taskDefinition.getConfiguration();
        List<String> discoverers = new ArrayList<>();
        for (String key: discoverKeys)
        {
            discoverers.add(config.get(key));
        }
        context.put(VSTestRunnerTaskType.DISCOVERERS, discoverers);
    }

    private List<String> getDiscoverKeysFromTaskDefinition(TaskDefinition taskDefinition)
    {
        return taskDefinition.getConfiguration().keySet().stream()
                .filter(key -> key.startsWith(VSTestRunnerTaskType.DISCOVERERS))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        // String arrays have to be populated yourself.
        int index = 0;
        String[] discoverers = params.getStringArray(VSTestRunnerTaskType.DISCOVERERS);
        if (discoverers != null)
        {
            for (String s: discoverers)
            {
                map.put(VSTestRunnerTaskType.DISCOVERERS + Integer.toString(index), s);
                index++;
            }
        }
        return map;
    }

    /**
     * Returns all discovered Test Discoverers.
     *
     * @return A map from the capability key to the display label: "VS20XX " + Test Discover Class to the capability value.
     */
    private Map<String, String> getTestDiscoverers()
    {
        return capabilitySetManager.getSystemCapabilityKeys(VSTestDiscoverCapabilityTypeModule.VSTEST_CAPABILITY_NAME, false).stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        VSTestDiscoverCapabilityTypeModule::constructLabel
                ));
    }

}
