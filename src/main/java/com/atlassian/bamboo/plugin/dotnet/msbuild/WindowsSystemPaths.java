package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.utils.BambooOptionals;
import com.sun.jna.platform.win32.Shell32Util;
import com.sun.jna.platform.win32.ShlObj;
import com.sun.jna.platform.win32.Win32Exception;
import io.atlassian.util.concurrent.Lazy;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class WindowsSystemPaths
{
    private static final Logger log = Logger.getLogger(WindowsSystemPaths.class);

    static final Supplier<Optional<File>> PROGRAM_FILES = Lazy.supplier(() -> getFolderPath(ShlObj.CSIDL_PROGRAM_FILES, "%PROGRAMFILES%"));
    static final Supplier<Optional<File>> PROGRAM_FILES_X86 = Lazy.supplier(() -> getFolderPath(ShlObj.CSIDL_PROGRAM_FILESX86, "%PROGRAMFILES(X86)%"));
    static final Supplier<Optional<File>> WINDOWS = Lazy.supplier(() -> getFolderPath(ShlObj.CSIDL_WINDOWS, "%WINDIR%"));

    private WindowsSystemPaths()
    {
    }
    
    public static Collection<File> getProgramFilesDirectories()
    {
        return Stream.of(PROGRAM_FILES, PROGRAM_FILES_X86)
                .map(Supplier::get)
                .flatMap(BambooOptionals::stream)
                .collect(Collectors.toList());
    }

    private static Optional<File> getFolderPath(final int csidl, final String dirName)
    {
        try
        {
            final String folderPath = Shell32Util.getFolderPath(csidl);
            if (StringUtils.isEmpty(folderPath))
            {
                return Optional.empty();
            }
            final File file = new File(folderPath);
            if (file.isDirectory()) {
                return Optional.of(file);
            }
            log.warn(dirName + " is not a directory");
            return Optional.empty();
        }
        catch (final Win32Exception e)
        {
            log.warn("Could not find the " + dirName +  " directory", e);
            return Optional.empty();
        }
    }
}
