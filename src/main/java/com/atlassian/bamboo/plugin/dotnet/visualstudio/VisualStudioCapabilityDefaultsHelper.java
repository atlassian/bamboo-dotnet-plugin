package com.atlassian.bamboo.plugin.dotnet.visualstudio;

import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

public class VisualStudioCapabilityDefaultsHelper implements CapabilityDefaultsHelper
{
    @Override
    @NotNull
    public CapabilitySet addDefaultCapabilities(@NotNull final CapabilitySet capabilitySet)
    {
        if (SystemUtils.IS_OS_WINDOWS)
        {
            final String vs2008Location = System.getenv("VS90COMNTOOLS");
            final String vs2010Location = System.getenv("VS100COMNTOOLS");
            final String vs2012Location = System.getenv("VS110COMNTOOLS");
            final String vs2013Location = System.getenv("VS120COMNTOOLS");

            createCapabilityForVisualStudio("Visual Studio 2008", capabilitySet, vs2008Location);
            createCapabilityForVisualStudio("Visual Studio 2010", capabilitySet, vs2010Location);
            createCapabilityForVisualStudio("Visual Studio 2012", capabilitySet, vs2012Location);
            createCapabilityForVisualStudio("Visual Studio 2013", capabilitySet, vs2013Location);
        }
        return capabilitySet;
    }

    private void createCapabilityForVisualStudio(@NotNull final String label, @NotNull final CapabilitySet capabilitySet, @Nullable final String path)
    {
        if (StringUtils.isNotEmpty(path))
        {
            File file = new File(new File(path).getParentFile(), "IDE");
            if (file.exists() && file.isDirectory())
            {
                Capability capability = new CapabilityImpl(VisualStudioTaskType.VS_CAPABILITY_PREFIX + "." + label, file.getAbsolutePath());
                capabilitySet.addCapability(capability);
            }
        }
    }
}
