package com.atlassian.bamboo.plugin.dotnet.tests.vstest;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugin.dotnet.tests.mstest.MSTestTestReportCollector;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.utils.BambooPathUtils;
import com.atlassian.bamboo.utils.FileVisitor;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.utils.process.ExternalProcess;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bill Tutt
 */
public class VSTestRunnerTaskType implements TaskType
{
    private static final Logger log = Logger.getLogger(VSTestRunnerTaskType.class);

    public static final String LABEL = "label";
    public static final String SETTINGS_FILE = "vstestSettings";
    public static final String ENABLECODECOVERAGE = "vstestEnableCodeCoverage";
    public static final String INISOLATION = "vstestInIsolation";
    public static final String PLATFORM = "vstestPlatform";
    public static final String FRAMEWORK = "vstestFramework";
    public static final String TESTCASEFILTER = "vstestTestCaseFilter";
    public static final String TESTNAMES = "vstestTestNames";
    public static final String TESTFILE = "vstestTestFile";
    public static final String DISCOVERERS = "vstestTestDiscoverers";
    public static final String RESULTS_FILE = "resultsfile";
    public static final String RESULTS_DIR = "vstestResultDir";
    public static final String USE_VSIX_EXTENSIONS = "useVsixExtensions";
    public static final String ADDITIONAL_ARGS = "additionalArgs";
    public static final String ENVIRONMENT = "environmentVariables";
    public static final String VSTESTEXE_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".vstestconsole";
    public static final String VSTESTEXE = "vstest.console.exe";

    private final ProcessService processService;
    private final TestCollationService testCollationService;
    private final CapabilityContext capabilityContext;
    private final EnvironmentVariableAccessor environmentVariableAccessor;

    public VSTestRunnerTaskType(@ComponentImport final ProcessService processService,
                                @ComponentImport final TestCollationService testCollationService,
                                @ComponentImport final CapabilityContext capabilityContext,
                                @ComponentImport final EnvironmentVariableAccessor environmentVariableAccessor)
    {
        this.processService = processService;
        this.testCollationService = testCollationService;
        this.capabilityContext = capabilityContext;
        this.environmentVariableAccessor = environmentVariableAccessor;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException
    {
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);
        try
        {
            final ConfigurationMap configuration = taskContext.getConfigurationMap();

            final String resultsFile = configuration.get(RESULTS_FILE);
            Preconditions.checkNotNull(resultsFile, "Results File Name");

            final String resultsDir = configuration.get(RESULTS_DIR);
            Preconditions.checkNotNull(resultsDir, "Results Directory Name");

            final List<String> command = getCommand(taskContext, configuration);

            final String environment = configuration.get(ENVIRONMENT);
            final Map<String, String> env = environmentVariableAccessor.splitEnvironmentAssignments(environment);

            // vstest.console.exe doesn't allow you to specify the result file, so execute vstest.console.exe
            // in a temporary directory so we can fixup the result file as the configuration requests.
            File tempDir = Files.createTempDir();
            try
            {
                final ExternalProcess externalProcess = processService.executeExternalProcess(taskContext,
                        new ExternalProcessBuilder()
                                .command(command)
                                .env(env) // taskContext.getWorkingDirectory()
                                .workingDirectory(tempDir));

                publishToResultsDir(taskContext, resultsDir, resultsFile, tempDir);

		        // send resultsFile only since the parser searches inside the working directory
                testCollationService.collateTestResults(taskContext, resultsDir, resultsFile, new MSTestTestReportCollector(), false);

                return TaskResultBuilder.newBuilder(taskContext)
                        .checkTestFailures()
                        .checkReturnCode(externalProcess)
                        .build();
            }
            finally
            {
                try
                {
                    BambooPathUtils.deleteDirectory(tempDir.toPath());
                }
                catch (final IOException e)
                {
                    log.error("Unable to delete " + tempDir, e);
                }
            }
        }
        finally
        {
            taskContext.getBuildContext().getBuildResult().addBuildErrors(errorLines.getErrorStringList());
        }
    }

    @VisibleForTesting
    @NotNull List<String> getCommand(@NotNull TaskContext taskContext, ConfigurationMap configuration) {
        final String label = configuration.get(LABEL);
        Preconditions.checkNotNull(label, "You must select a vstest.console executable before this task can execute.");

        final String settingsFile = configuration.get(SETTINGS_FILE);
        Preconditions.checkNotNull(settingsFile, "Settings File");

        final String platform = configuration.get(PLATFORM);
        final String framework = configuration.get(FRAMEWORK);

        final String inIsolation = configuration.get(INISOLATION);
        final String codeCoverage = configuration.get(ENABLECODECOVERAGE);

        final String testFile = configuration.get(TESTFILE);
        Preconditions.checkNotNull(testFile, "Test File");


        final Capability capability = capabilityContext.getCapabilitySet().getCapability(VSTESTEXE_CAPABILITY_PREFIX + "." + label);
        Preconditions.checkNotNull(capability, "Capability");

        final File vsTestExe = new File(capability.getValue());
        Preconditions.checkArgument(vsTestExe.exists(), vsTestExe.getAbsolutePath() + " cannot be found");

        final String useVsixExtensions = configuration.getOrDefault(USE_VSIX_EXTENSIONS, "true");

        final List<String> command = new ArrayList<>();

        command.add(vsTestExe.getAbsolutePath());
        if (StringUtils.isNotEmpty(inIsolation)
            && inIsolation.compareTo("true") == 0)
        {
            command.add("/InIsolation");
        }

        if (StringUtils.isNotEmpty(codeCoverage)
            && codeCoverage.compareTo("true") == 0)
        {
            command.add("/EnableCodeCoverage");
        }

        if (StringUtils.isNotEmpty(platform))
        {
            command.add("/platform:" + platform);
        }
        if (StringUtils.isNotEmpty(framework))
        {
            command.add("/framework:" + framework);
        }
        if (StringUtils.isNotEmpty(settingsFile))
        {
            command.add("/Settings:" + settingsFile);
        }
        if (StringUtils.isNotEmpty(testFile))
        {
            // Due to the working directory being a temp directory
            // we need to prefix the testFile with the working directory
            // if the file path isn't absolute.
            File test = new File(testFile);
            if (!test.isAbsolute())
            {
                test = new File(taskContext.getWorkingDirectory(), testFile);
            }
            command.add(test.getPath());
        }
        else
        {
            throw new IllegalStateException("Configuration is not valid. A Test File was not specified");
        }

        command.add("/logger:Trx");
        if ("true".equalsIgnoreCase(useVsixExtensions))
        {
            command.add("/UseVsixExtensions:true");
        }

        final String additionalArgs = configuration.get(ADDITIONAL_ARGS);
        if (StringUtils.isNotBlank(additionalArgs))
        {
            command.add(additionalArgs);
        }
        return command;
    }

    private void publishToResultsDir(@NotNull final TaskContext taskContext, @NotNull final String resultDirPath, @NotNull final String resultFile, @NotNull final File tempDir) throws TaskException
    {
        // Rename .trx to resultFile and copy all files from tempDir to resultDir creating resultDir if necessary.
        File resultsDir = new File(resultDirPath);
        if (!resultsDir.isAbsolute())
        {
            resultsDir = new File(taskContext.getWorkingDirectory(), resultDirPath);
        }
        if (!resultsDir.exists())
        {
            if (!resultsDir.mkdirs())
            {
                throw new TaskException("Unable to create results directory: " + resultDirPath);
            }
        }
        // Inner class closures need to be final in Java.
        final File resultDir = resultsDir;
        FileVisitor fileVisitor = new FileVisitor(tempDir)
        {
            public void visitFile(File file) throws InterruptedException
            {
                try
                {
                    // Ensure that the test log (trx) file gets the name
                    // the task configuration specified.
                    if (FilenameUtils.isExtension(file.getName(), "trx"))
                    {
                        FileUtils.copyFile(file, new File(resultDir, resultFile), true);
                    }
                    else
                    {
                        FileUtils.copyFile(file, new File(resultDir, file.getName()), true);
                    }
                }
                catch (IOException exception)
                {
                    throw new InterruptedException(exception.toString());
                }
            }
        };
        try
        {
            fileVisitor.visitFilesThatMatch("**/*");
        }
        catch (InterruptedException | IOException e)
        {
            log.error(e.toString());
            log.error(taskContext.getBuildLogger().addErrorLogEntry("Failed to publish test result files to results directory. Build was interrupted."));
        }
    }
}
