package com.atlassian.bamboo.plugin.dotnet.nant;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deployments.environments.DeploymentTaskRequirementSupport;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

public class NantConfigurator extends AbstractDotNetTaskConfigurator implements BuildTaskRequirementSupport, DeploymentTaskRequirementSupport
{
    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            NantTaskType.LABEL,
            NantTaskType.BUILD_FILE,
            NantTaskType.TARGET,
            NantTaskType.OPTIONS,
            NantTaskType.ENVIRONMENT,
            TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY
    );
    private static final String DEFAULT_TARGET = "run";
    private static final String DEFAULT_BUILD_FILE = "default.build";

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Environment environment)
    {
        final String label = taskDefinition.getConfiguration().get(NantTaskType.LABEL);
        return Sets.newHashSet(new RequirementImpl(NantTaskType.NANT_CAPABILITY_PREFIX + "." + label, true, ".*"));
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Job buildable)
    {
        final String label = taskDefinition.getConfiguration().get(NantTaskType.LABEL);
        return Sets.newHashSet(new RequirementImpl(NantTaskType.NANT_CAPABILITY_PREFIX + "." + label, true, ".*"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap actionParametersMap, @NotNull final ErrorCollection errorCollection)
    {
        final String target = actionParametersMap.getString(NantTaskType.TARGET);
        if (StringUtils.isEmpty(target))
        {
            errorCollection.addError(NantTaskType.TARGET, textProvider.getText("nant.target.error"));
        }

        final String label = actionParametersMap.getString(NantTaskType.LABEL);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(NantTaskType.LABEL, textProvider.getText("nant.label.error"));
        }
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(NantTaskType.TARGET, DEFAULT_TARGET);
        context.put(NantTaskType.BUILD_FILE, DEFAULT_BUILD_FILE);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }
}
