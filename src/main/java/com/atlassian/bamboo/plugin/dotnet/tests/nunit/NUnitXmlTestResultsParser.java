package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.configuration.ConfigurationException;
import com.atlassian.bamboo.configuration.DefaultElementParser;
import com.atlassian.bamboo.configuration.ElementContentElementParser;
import com.atlassian.bamboo.plugin.dotnet.tests.TestResultContentHandler;
import com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParser;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.atlassian.security.xml.SecureXmlParserFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import net.jcip.annotations.NotThreadSafe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles parsing NUnit report files.
 *
 * @author Ross Rowe
 */
@NotThreadSafe
public class NUnitXmlTestResultsParser extends TestResultContentHandler implements TestResultsParser
{

    /**
     * Handles parsing 'message' elements.
     *
     * @author Ross Rowe
     */
    public class MessageElementParser extends ElementContentElementParser
    {
        /**
         * Overriden to record error message details.
         */
        @Override
        public void endElement() throws ConfigurationException
        {
            super.endElement();
            if (getCurrentTestError() != null)
            {
                final StringBuilder builder = new StringBuilder();
                if (getCurrentTestError().getContent() != null)
                {
                    builder.append(getCurrentTestError().getContent());
                    builder.append(System.getProperty("line.separator"));
                }
                builder.append(getElementContent());
                setCurrentTestError(new TestCaseResultErrorImpl(builder.toString()));
            }
        }

    }

    /**
     * Handles parsing 'test-suite' elements.
     *
     * @author Ross Rowe
     */
    public class TestSuiteElementParser extends ElementContentElementParser
    {
        /**
         * Overriden to store the suite class name.
         */
        @Override
        public void startElement(Attributes attributes)
        {
            super.startElement(attributes);

            setSuiteClassName(attributes.getValue("name"));
        }
    }

    /**
     * Handles parsing 'test-case' elements.
     * https://github.com/nunit/docs/wiki/Test-Result-XML-Format#test-case
     *
     * @author Ross Rowe
     */
    public class TestCaseElementParser extends ElementContentElementParser
    {
        private final static String RESULT_PASSED_NUNIT2 = "SUCCESS";
        private final static String RESULT_PASSED = "PASSED";
        private final static String RESULT_INCONCLUSIVE = "INCONCLUSIVE";
        private final static String RESULT_SKIPPED = "SKIPPED";
        private final static String RESULT_FAILED = "FAILED";

        /**
         * Overriden to store test case information.
         */
        @Override
        public void startElement(Attributes attributes)
        {
            super.startElement(attributes);

            final double testDurationInMillis = NumberUtils.toDouble(attributes.getValue("time")) * 1000;
            final String fqTestName = attributes.getValue("name");
            final String className = getClassName(attributes, fqTestName);
            final String testName = getTestName(fqTestName);

            final TestResults testResults = new TestResults(className != null ? className : getSuiteClassName(),
                                                            testName, (long) testDurationInMillis);
            testResults.setState(getTestState(attributes));
            setCurrentTestResult(testResults);
        }

        /**
         * Overriden to add currentTestResult to either <code>failedTests</code>
         * or <code>successfulTests</code>.
         */
        @Override
        public void endElement() throws ConfigurationException
        {
            super.endElement();

            final TestResults testResults = getCurrentTestResult();

            if (testResults != null)
            {
                switch (testResults.getState())
                {
                    case FAILED:
                        failedTests.add(testResults);
                        break;

                    case SKIPPED:
                        inconclusiveTests.add(testResults);
                        break;

                    case SUCCESS:
                        passedTests.add(testResults);
                        break;
                }

                setCurrentTestResult(null);
            }
        }

        /**
         * See nunit's TestContext documentation:
         * http://nunit.org/nunitv2/docs/2.6/testContext.html
         * https://github.com/nunit/docs/wiki/Test-Result-XML-Format#test-case
         */
        private TestState getTestState(Attributes attributes)
        {
            final String result = attributes.getValue("result");
            if (result != null) {
                // result files produced by nunit 3 and modern nunit 2 use 'result' attribute to pass information on test case state
                switch (result.toUpperCase()) {
                    case RESULT_PASSED:
                    case RESULT_PASSED_NUNIT2:
                        return TestState.SUCCESS;

                    case RESULT_FAILED:
                        return TestState.FAILED;

                    case RESULT_INCONCLUSIVE:
                    case RESULT_SKIPPED:
                    default:
                        return TestState.SKIPPED;
                }
            } else {
                // result files produced by older tools use 'success' attribute
                final boolean executed = Boolean.parseBoolean(attributes.getValue("executed"));
                final boolean success = Boolean.parseBoolean(attributes.getValue("success"));

                if (executed) {
                    return success ? TestState.SUCCESS : TestState.FAILED;
                } else {
                    return TestState.SKIPPED;
                }
            }
        }

        private String getClassName(Attributes attributes, String fqTestName) {
            final String className = attributes.getValue("classname");
            final String fqNameWithoutParams = StringUtils.substringBefore(fqTestName, "(");
            if (className == null && fqNameWithoutParams.contains("."))
            {
                if (fqNameWithoutParams.endsWith("."))
                {
                    return null;
                }
                else
                {
                    return StringUtils.substringBeforeLast(fqNameWithoutParams, ".");
                }
            }

            return className;
        }

    }

    @VisibleForTesting
    @NotNull
    static String getTestName(final String fqTestName)
    {
        if (!fqTestName.contains("."))
        {
            return fqTestName;
        }

        final String fqNameWithoutParams = StringUtils.substringBefore(fqTestName, "(");

        if (fqNameWithoutParams.endsWith("."))
        {
            return fqNameWithoutParams;
        }
        else
        {
            return fqTestName.substring(fqNameWithoutParams.lastIndexOf(".") + 1);
        }
    }

    public class ErrorElementParser extends ElementContentElementParser
    {
        /**
         * Overriden to store error details.
         */
        @Override
        public void endElement() throws ConfigurationException
        {
            super.endElement();

            TestCaseResultErrorImpl error = new TestCaseResultErrorImpl(getElementContent());
            if (getCurrentTestResult() != null)
            {
                getCurrentTestResult().addError(error);
            }
            else
            {
                TestResults errorTestResult = new TestResults(getSuiteClassName(), "unknownTestCase", 0L);
                errorTestResult.addError(error);
                failedTests.add(errorTestResult);
            }
        }

    }

    /**
     * Handles parsing 'failure' elements'
     *
     * @author Ross Rowe
     */
    public class FailureElementParser extends ElementContentElementParser
    {
        private String message;

        @Override
        public void startElement(Attributes attributes)
        {
            super.startElement(attributes);
            message = attributes.getValue("message");
            setCurrentTestError(new TestCaseResultErrorImpl(message));
        }

        @Override
        public void endElement() throws ConfigurationException
        {
            super.endElement();

            if (getCurrentTestResult() != null)
            {
                getCurrentTestResult().addError(getCurrentTestError());
                getCurrentTestResult().setState(TestState.FAILED);
                setCurrentTestError(null);
            }
        }
    }

    /**
     * Constructs a NantXmlTestResultsParser instance.
     */
    public NUnitXmlTestResultsParser()
    {
        registerElementParser("categories", new DefaultElementParser());
        registerElementParser("category", new DefaultElementParser());
        registerElementParser("test-results", new DefaultElementParser());
        registerElementParser("environment", new DefaultElementParser());
        registerElementParser("culture-info", new DefaultElementParser());
        registerElementParser("results", new DefaultElementParser());
        registerElementParser("reason", new DefaultElementParser());
        registerElementParser("message", new MessageElementParser());
        registerElementParser("test-suite", new TestSuiteElementParser());
        registerElementParser("stack-trace", new DefaultElementParser());
        registerElementParser("test-case", new TestCaseElementParser());
        registerElementParser("error", new ErrorElementParser());
        registerElementParser("failure", new FailureElementParser());
    }

    public int getNumberOfErrors() {
        return failedTests.size();
    }

    @Override
    public ImmutableList<TestResults> getSuccessfulTests()
    {
        return ImmutableList.copyOf(passedTests);
    }

    @Override
    public ImmutableList<TestResults> getFailedTests()
    {
        return ImmutableList.copyOf(failedTests);
    }

    @Override
    public ImmutableList<TestResults> getInconclusiveTests()
    {
        return ImmutableList.copyOf(inconclusiveTests);
    }

    public String getSystemOut() {
        return systemOut;
    }

    public String getSystemErr() {
        return errorOut;
    }

    /**
     * @param inputStream
     */
    @Override
    public void parse(InputStream inputStream)
    {
        failedTests = new ArrayList<>();
        passedTests = new ArrayList<>();
        inconclusiveTests = new ArrayList<>();
        try
        {
            XMLReader reader = SecureXmlParserFactory.newNamespaceAwareXmlReader();
            reader.setContentHandler(this);
            reader.parse(new InputSource(inputStream));
        }
        catch (Exception e)
        {
            log.error("Failed to parse xml test results.", e);
        }
    }

    private static final Logger log = Logger
            .getLogger(NUnitXmlTestResultsParser.class);

    private String suiteClassName;

    private TestResults currentTestResult;

    private TestCaseResultErrorImpl currentTestError;

    private List<TestResults> failedTests;
    private List<TestResults> passedTests;
    private List<TestResults> inconclusiveTests;

    private String systemOut;

    private String errorOut;

    public TestResults getCurrentTestResult() {
        return currentTestResult;
    }

    public String getSuiteClassName()
    {
        return suiteClassName;
    }

    public void setCurrentTestResult(TestResults currentTestResult) {
        this.currentTestResult = currentTestResult;
    }

    public void setSuiteClassName(String suiteClassName) {
        this.suiteClassName = suiteClassName;
    }

    public String getErrorOut() {
        return errorOut;
    }

    public void setErrorOut(String errorOut) {
        this.errorOut = errorOut;
    }

    public void setSystemOut(String systemOut) {
        this.systemOut = systemOut;
    }

    public TestCaseResultErrorImpl getCurrentTestError() {
        return currentTestError;
    }

    public void setCurrentTestError(TestCaseResultErrorImpl currentTestError) {
		this.currentTestError = currentTestError;
	}

}
