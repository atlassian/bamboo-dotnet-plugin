package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.charts.collater.TimePeriodCollater;
import com.atlassian.bamboo.reports.collector.AbstractTimePeriodCollector;

/**
 * {@link AbstractTimePeriodCollector} subclass that handles charting of line rate information.
 * @author Ross Rowe
 *
 */
public class NCoverCoverageCollector extends AbstractTimePeriodCollector
{
	/**
	 * @return a new {@link NCoverLineRateCoverageCollator} instance
	 */
    @Override
    protected TimePeriodCollater getCollater()
    {
        return new NCoverLineRateCoverageCollator();
    }
    
}
