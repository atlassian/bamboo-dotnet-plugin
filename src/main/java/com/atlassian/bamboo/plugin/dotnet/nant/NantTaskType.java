package com.atlassian.bamboo.plugin.dotnet.nant;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.logger.interceptors.LogMemorisingInterceptor;
import com.atlassian.bamboo.plugin.dotnet.support.DotNetLogHelper;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.CurrentResult;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NantTaskType implements CommonTaskType
{
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(NantTaskType.class);
    // ------------------------------------------------------------------------------------------------------- Constants

    public static final String NANT_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".nant";
    public static final String ENVIRONMENT = "environmentVariables";
    public static final String LABEL = "label";
    public static final String BUILD_FILE = "buildFile";
    public static final String TARGET = "target";
    public static final String OPTIONS = "options";
    public static final String EXECUTABLE_NAME = SystemUtils.IS_OS_WINDOWS ? "nant.exe" : "nant";
    private static final int LINES_TO_PARSE_FOR_ERRORS = 200;

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies

    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;

    // ---------------------------------------------------------------------------------------------------- Constructors

    public NantTaskType(final ProcessService processService, final EnvironmentVariableAccessor environmentVariableAccessor, final CapabilityContext capabilityContext)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods


    @Override
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException
    {
        final BuildLogger buildLogger = commonTaskContext.getBuildLogger();
        final CurrentResult currentBuildResult = commonTaskContext.getCommonContext().getCurrentResult();

        LogMemorisingInterceptor recentLogLines = new LogMemorisingInterceptor(LINES_TO_PARSE_FOR_ERRORS);
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();

        buildLogger.getInterceptorStack().add(recentLogLines);
        buildLogger.getInterceptorStack().add(errorLines);

        try
        {
            final Map<String, String> environment = getCustomEnvironment(commonTaskContext);
            final String executable = getExecutablePath(commonTaskContext);
            final String buildFile = getBuildFile(commonTaskContext);
            final String options = getOptions(commonTaskContext);
            final String target = getTarget(commonTaskContext);

            final List<String> command = new ArrayList<>();
            command.add(executable);
            if (StringUtils.isNotEmpty(buildFile))
            {
                command.add("-buildfile:" + buildFile);
            }
            command.addAll(CommandlineStringUtils.tokeniseCommandline(options));
            command.addAll(CommandlineStringUtils.tokeniseCommandline(target));

            return TaskResultBuilder.newBuilder(commonTaskContext)
                    .checkReturnCode(
                            processService.executeExternalProcess(
                                    commonTaskContext,
                                    new ExternalProcessBuilder()
                                            .env(environment)
                                            .command(command)
                                            .workingDirectory(commonTaskContext.getWorkingDirectory()))
                    ).build();
        } finally
        {
            currentBuildResult.addBuildErrors(errorLines.getErrorStringList());
            currentBuildResult.addBuildErrors(DotNetLogHelper.parseErrorOutput(recentLogLines.getLogEntries()));
        }
    }

    private Map<String, String> getCustomEnvironment(final CommonTaskContext taskContext)
    {
        return environmentVariableAccessor.splitEnvironmentAssignments(taskContext.getConfigurationMap().get(ENVIRONMENT));
    }

    private String getOptions(@NotNull final CommonTaskContext commonTaskContext)
    {
        return commonTaskContext.getConfigurationMap().get(OPTIONS);
    }

    public String getTarget(@NotNull final CommonTaskContext commonTaskContext)
    {
        return commonTaskContext.getConfigurationMap().get(TARGET);
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    private String getBuildFile(@NotNull final CommonTaskContext commonTaskContext)
    {
        return commonTaskContext.getConfigurationMap().get(BUILD_FILE);
    }

    String getExecutablePath(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final File executableFile = getExecutableFile(taskContext);

        if (!executableFile.exists())
        {
            throw new TaskException("Executable '" + EXECUTABLE_NAME + "'  does not exist at path '" + executableFile.getPath() + "'");
        }

        return executableFile.getPath();
    }

    File getExecutableFile(@NotNull final CommonTaskContext commonTaskContext)
    {
        final String label = commonTaskContext.getConfigurationMap().get(LABEL);
        Preconditions.checkNotNull(label, "Label");

        final String capabilityKey = NANT_CAPABILITY_PREFIX + "." + label;
        Capability capability = capabilityContext.getCapabilitySet().getCapability(capabilityKey);
        Preconditions.checkNotNull(capability, "Capability");

        final String path = Joiner.on(File.separator).join(capability.getValue(), "bin", EXECUTABLE_NAME);
        final File executableFile = new File(path);

        return executableFile.isAbsolute() ? executableFile : new File(commonTaskContext.getRootDirectory(), path);
    }


    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
