package com.atlassian.bamboo.plugin.dotnet.support;

import com.atlassian.bamboo.build.LogEntry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Helper class to extract error messages from MSBuild style build output.
 *
 * @author Marko Lahma
 */
public class DotNetLogHelper implements Serializable
{
    public static List<String> parseErrorOutput(final Iterable<LogEntry> logsToParse)
    {
        return parseErrorOutput(logsToParse, new ErrorMatcher());
    }

    private static List<String> parseErrorOutput(final Iterable<LogEntry> logsToParse, final ErrorMatcher fatalMatcher)
    {
        parseLogs(fatalMatcher, logsToParse);

        return fatalMatcher.getErrorLogs();
    }

    private static void parseLogs(final ErrorMatcher matcher, final Iterable<LogEntry> logsToParse)
    {
        String previousLog = null;
        for (LogEntry logEntry : logsToParse)
        {
            String currentLog = logEntry.getUnstyledLog();
            matcher.match(previousLog, currentLog);
            previousLog = currentLog;
        }
    }

    /**
     * A helper class to match error messages from standard build output.
     */
    private static class ErrorMatcher
    {

        private boolean matched;
        private List<String> errorLogs = new ArrayList<>();
        private static final String ERROR_MESSAGE_INDICATOR = ": error ";

        public void match(String previousLog, String currentLog)
        {
            if (matched)
            {
                if (!isEnd(previousLog, currentLog))
                {
                    errorLogs.add(previousLog);
                } else
                {
                    matched = false;
                }
            } else if (isStart(previousLog, currentLog))
            {
                if (previousLog != null)
                {
                    errorLogs.add(previousLog);
                }
                matched = true;
            }
        }

        public List<String> getErrorLogs()
        {
            return errorLogs;
        }

        private boolean isEnd(String previousLog, String currentLog)
        {
            return previousLog != null && !previousLog.contains(ERROR_MESSAGE_INDICATOR);
        }

        private boolean isStart(String previousLog, String currentLog)
        {
            return currentLog != null && currentLog.contains(ERROR_MESSAGE_INDICATOR);
        }
    }
}
