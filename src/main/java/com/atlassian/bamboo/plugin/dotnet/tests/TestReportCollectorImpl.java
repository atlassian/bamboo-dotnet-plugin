package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.google.common.collect.Sets;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Set;

/**
 * Collects the results from parsing NUnit report files.
 * 
 * @author Ross Rowe
 *
 */
public class TestReportCollectorImpl implements TestReportCollector
{
	private TestResultsParser parser;

	/**
	 * Constructs a NantNunitTestReportCollector instance.
	 * 
	 * @param parser
	 */
	public TestReportCollectorImpl(TestResultsParser parser)
    {
		this.parser = parser;
	}

    @Override
    @NotNull
    public TestCollectionResult collect(@NotNull final File file) throws Exception
    {
        try (InputStream inputStream = new FileInputStream(file)) {
            parser.parse(inputStream);
            return new TestCollectionResultBuilder()
                    .addSuccessfulTestResults(parser.getSuccessfulTests())
                    .addFailedTestResults(parser.getFailedTests())
                    .addSkippedTestResults(parser.getInconclusiveTests())
                    .build();
        }
        catch(FileNotFoundException e)
        {
            throw new RuntimeException(e);
        }
	}

    @Override
    @NotNull
    public Set<String> getSupportedFileExtensions()
    {
        return Sets.newHashSet("trx", "xml");
    }
}
