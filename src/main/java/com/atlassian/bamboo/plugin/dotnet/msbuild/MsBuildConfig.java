package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.google.common.base.Preconditions;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Map;
import java.util.Optional;

/**
 * MSBuild runtime configuration
 */
public class MsBuildConfig {
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(MsBuildConfig.class);

    public static final String MSBUILD_EXECUTABLE_LABEL = "msbuild";
    public static final String MSBUILD_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + "." + MSBUILD_EXECUTABLE_LABEL;

    public static final String LABEL = "label";
    public static final String SOLUTION = "solution";
    public static final String OPTIONS = "options";
    public static final String ENVIRONMENT = "environmentVariables";

    private final String msBuildExecutable;
    private final String options;
    private final String solution;
    private final Map<String, String> environmentMap;

    private MsBuildConfig(@NotNull String msBuildExecutable, @Nullable String options, @NotNull String solution, @NotNull Map<String, String> environmentMap) {
        this.msBuildExecutable = msBuildExecutable;
        this.options = options;
        this.solution = solution;
        this.environmentMap = environmentMap;
    }

    public static MsBuildConfig build(@NotNull CommonTaskContext taskContext,
                                      @NotNull CapabilityContext capabilityContext,
                                      @NotNull EnvironmentVariableAccessor environmentVariableAccessor) {
        final ConfigurationMap configuration = taskContext.getConfigurationMap();

        final String label = configuration.get(LABEL);
        Preconditions.checkNotNull(label);

        final String solution = configuration.get(SOLUTION);
        Preconditions.checkNotNull(solution, "Solution");

        final String options = configuration.get(OPTIONS);

        final Map<String, String> environmentMap = environmentVariableAccessor.splitEnvironmentAssignments(Optional.ofNullable(configuration.get(ENVIRONMENT)).orElse(""));
        Preconditions.checkNotNull(environmentMap, "Process Environment");

        final String capaName = MSBUILD_CAPABILITY_PREFIX + "." + label;
        final Capability msBuildCapability = capabilityContext.getCapabilitySet().getCapability(capaName);
        Preconditions.checkNotNull(msBuildCapability, "Capability " + capaName + " is missing");
        final String msBuildExecutable = msBuildCapability.getValue();
        Preconditions.checkNotNull(msBuildExecutable, "Could not find MSBuild.exe, " + capaName + " point to nowhere");
        final String msBuildExecutableAbsolutePath = new File(msBuildExecutable).getAbsolutePath();

        return new MsBuildConfig(msBuildExecutableAbsolutePath, options, solution, environmentMap);
    }

    @NotNull
    public String getMsBuildExecutable() {
        return msBuildExecutable;
    }

    @Nullable
    public String getOptions() {
        return options;
    }

    @NotNull
    public String getSolution() {
        return solution;
    }

    @NotNull
    public Map<String, String> getEnvironmentMap() {
        return environmentMap;
    }
}
