package com.atlassian.bamboo.plugin.dotnet.tests.vstest;

import com.atlassian.bamboo.utils.BambooFieldValidate;
import com.atlassian.bamboo.v2.build.agent.capability.AbstractCapabilityTypeModule;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.struts.TextProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bill Tutt
 */
public class VSTestDiscoverCapabilityTypeModule extends AbstractCapabilityTypeModule
{
    private static final Logger log = Logger.getLogger(VSTestDiscoverCapabilityTypeModule.class);

    static final String VSTEST_CAPABILITY_NAME = "vstestdiscoverer";
    static final String VSTEST_CAPABILITY_PREFIX = "system.vstestdiscoverer";

    private interface Configuration
    {
        String NAME = "name";
        String SUPPORTED_FILE_TYPES = "supportedFileTypes";
    }

    private TextProvider textProvider;

    @Autowired
    public VSTestDiscoverCapabilityTypeModule(final TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }

    @NotNull
    @Override
    public Map<String, String> validate(@NotNull Map<String, String[]> params)
    {
        final Map<String, String> fieldErrors = new HashMap<>();

        final String name = params.get(Configuration.NAME)[0];

        if (StringUtils.isBlank(name))
        {
            fieldErrors.put(Configuration.NAME, textProvider.getText("agent.capability.type.system.vstestdiscoverer.error.emptyName"));
        }
        else
        {
            BambooFieldValidate.checkFieldXssSafety(fieldErrors, textProvider, Configuration.NAME, name);
        }

        return fieldErrors;
    }

    @NotNull
    @Override
    public Capability getCapability(@NotNull Map<String, String[]> params)
    {
        final String name = params.get(Configuration.NAME)[0];
        final String supportedFileTypes = params.get(Configuration.SUPPORTED_FILE_TYPES)[0];

        return new CapabilityImpl(VSTEST_CAPABILITY_PREFIX + "." + name, supportedFileTypes);
    }

    /**
     * Return the pretty name of this capability key for use in the UI.
     * This routine gets called from several different bits of UI.
     * The admin area of capabilities, plan requirement tab, etc...
     *
     * @param key The capability key.
     * @return The display label for the capability.
     */
    @NotNull
    @Override
    public String getLabel(@NotNull String key)
    {
        return constructLabel(key);
    }

    //--------------------------------------------------------------------------------------------------- Public Methods
    public static String constructLabel(@NotNull String key)
    {
        String[] strings = key.split("\\.", 0);
        if (strings.length > 4)
        {
            // "VS 20XX " + test discoverer class.
            key = strings[2] + " " + StringUtils.join(strings, ".", 3, strings.length);
        }
        else if (strings.length == 4)
        {
            key = strings[2] + " " + strings[3];
        }
        else
        {
            log.warn("Key: '" + key + "' cannot be parsed. Returning full key");
        }
        return key;
    }
}
