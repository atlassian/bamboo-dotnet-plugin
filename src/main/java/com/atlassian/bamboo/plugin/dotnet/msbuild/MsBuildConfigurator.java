package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deployments.environments.DeploymentTaskRequirementSupport;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.specs.builders.task.MsBuildTask;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

public class MsBuildConfigurator extends AbstractDotNetTaskConfigurator implements BuildTaskRequirementSupport, DeploymentTaskRequirementSupport
{
    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            MsBuildConfig.LABEL,
            MsBuildConfig.SOLUTION,
            MsBuildConfig.OPTIONS,
            MsBuildConfig.ENVIRONMENT,
            TaskConfigConstants.CFG_WORKING_SUBDIRECTORY
    );
    private static final String DEFAULT_SOLUTION = MsBuildTask.DEFAULT_PROJECT;

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Environment environment)
    {
        final String label = taskDefinition.getConfiguration().get(MsBuildConfig.LABEL);
        return Sets.newHashSet(new RequirementImpl(MsBuildTaskType.MSBUILD_CAPABILITY_PREFIX + "." + label, true, ".*"));
    }

    @Override
    @NotNull
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Job buildable)
    {
        final String label = taskDefinition.getConfiguration().get(MsBuildConfig.LABEL);
        return Sets.newHashSet(new RequirementImpl(MsBuildTaskType.MSBUILD_CAPABILITY_PREFIX + "." + label, true, ".*"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap actionParametersMap, @NotNull final ErrorCollection errorCollection)
    {
        final String solution = actionParametersMap.getString(MsBuildConfig.SOLUTION);
        if (StringUtils.isEmpty(solution))
        {
            errorCollection.addError(MsBuildConfig.SOLUTION, textProvider.getText("msbuild.solution.error"));
        }

        final String label = actionParametersMap.getString(MsBuildConfig.LABEL);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(MsBuildConfig.LABEL, textProvider.getText("msbuild.label.error"));
        }
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(MsBuildConfig.SOLUTION, DEFAULT_SOLUTION);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }
}
