package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NUnitRunnerTask;
import com.atlassian.bamboo.specs.model.task.NUnitRunnerTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public class NUnitRunnerTaskExporter implements TaskDefinitionExporter {
    private static final ValidationContext NUNIT_PARSER_CONTEXT = ValidationContext.of("NUnit runner task");

    @Autowired
    private UIConfigSupport uiConfigSupport;

    @NotNull
    @Override
    public NUnitRunnerTask toSpecsEntity(final @NotNull TaskDefinition taskDefinition) {
        final Map<String, String> configuration = taskDefinition.getConfiguration();
        return getTaskForCapabilityKey(configuration.get(NUnitRunnerTaskType.CAPABILITY_KEY))
                .nUnitTestFiles(configuration.get(NUnitRunnerTaskType.TEST_FILES))
                .resultFilename(configuration.get(NUnitRunnerTaskType.RESULTS_FILE))
                .testsToRun(configuration.getOrDefault(NUnitRunnerTaskType.TESTS_TO_RUN, "").split(","))
                .testCategoriesToInclude(configuration.getOrDefault(NUnitRunnerTaskType.INCLUDED_TEST_CATEGORIES, "").split(","))
                .testCategoriesToExclude(configuration.getOrDefault(NUnitRunnerTaskType.EXCLUDED_TEST_CATEGORIES, "").split(","))
                .commandLineOptions(configuration.get(NUnitRunnerTaskType.COMMAND_OPTIONS))
                .environmentVariables(configuration.get(NUnitRunnerTaskType.ENVIRONMENT));
    }

    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, final TaskProperties taskProperties) {
        final NUnitRunnerTaskProperties properties = Narrow.downTo(taskProperties, NUnitRunnerTaskProperties.class);
        if (properties != null) {
            final Map<String, String> cfg = new HashMap<>();
            final NUnitRunnerTask.NUnitVersion nUnitVersion = properties.getNUnitVersion();
            cfg.put(NUnitRunnerTaskType.CAPABILITY_KEY, getCapabilityKey(properties.getExecutable(), nUnitVersion));
            cfg.put(NUnitRunnerTaskType.TEST_FILES, properties.getNUnitTestFiles());
            cfg.put(NUnitRunnerTaskType.RESULTS_FILE, properties.getResultFilename());
            if (!properties.getTestsToRun().isEmpty()) {
                cfg.put(NUnitRunnerTaskType.TESTS_TO_RUN, properties.getTestsToRun().stream().collect(joining(",")));
            }
            if (!properties.getTestCategoriesToInclude().isEmpty()) {
                cfg.put(NUnitRunnerTaskType.INCLUDED_TEST_CATEGORIES, properties.getTestCategoriesToInclude().stream().collect(joining(",")));
            }
            if (!properties.getTestCategoriesToExclude().isEmpty()) {
                cfg.put(NUnitRunnerTaskType.EXCLUDED_TEST_CATEGORIES, properties.getTestCategoriesToExclude().stream().collect(joining(",")));
            }
            if (StringUtils.isNotBlank(properties.getCommandLineOptions())) {
                cfg.put(NUnitRunnerTaskType.COMMAND_OPTIONS, properties.getCommandLineOptions());
            }
            if (StringUtils.isNotBlank(properties.getEnvironmentVariables())) {
                cfg.put(NUnitRunnerTaskType.ENVIRONMENT, properties.getEnvironmentVariables());
            }
            return cfg;
        }
        throw new IllegalStateException("Don't know how to import task properties of type: " + taskProperties.getClass().getName());
    }

    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        List<ValidationProblem> result = new ArrayList<>();
        final NUnitRunnerTaskProperties properties = Narrow.downTo(taskProperties, NUnitRunnerTaskProperties.class);

        if (properties != null) {
            final String capabilityPrefix = getCapabilityShortKey(properties.getNUnitVersion());
            final List<String> labels = uiConfigSupport.getExecutableLabels(capabilityPrefix);
            final String label = properties.getExecutable();
            if (labels == null || !labels.contains(label)) {
                result.add(new ValidationProblem(NUNIT_PARSER_CONTEXT,
                        String.format("Can't find executable by label: '%s'. Available values: %s", label, labels)));
            }
        }

        return result;
    }

    @NotNull
    private static NUnitRunnerTask getTaskForCapabilityKey(@NotNull String key) {
        final NUnitRunnerTask task = new NUnitRunnerTask();

        if (key.startsWith(NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX)) {
            return task.executable(key.substring(NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX.length() + 1))
                    .nUnitVersion3();
        } else if (key.startsWith(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX)) {
            return task.executable(key.substring(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX.length() + 1))
                    .nUnitVersion2();
        } else {
            return task.executable(key)
                    .nUnitVersion2();
        }
    }

    @NotNull
    private static String getCapabilityKey(String capabilityLabel, NUnitRunnerTask.NUnitVersion nUnitVersion) {
        return Joiner.on(".").join(CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX,
                getCapabilityShortKey(nUnitVersion),
                capabilityLabel);
    }

    @NotNull
    private static String getCapabilityShortKey(NUnitRunnerTask.NUnitVersion nUnitVersion) {
        switch (nUnitVersion) {
            case NUNIT_2:
                return NUnitRunnerTaskType.NUNIT_SHORT_KEY;
            case NUNIT_3:
                return NUnitRunnerTaskType.NUNIT3_SHORT_KEY;
            default:
                throw new IllegalStateException("Unsupported enum value: " + nUnitVersion);
        }
    }


}
