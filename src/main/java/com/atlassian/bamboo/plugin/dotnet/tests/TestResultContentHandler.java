/*_____________________________________________________________________                                      
 *
 * Copyright (c) 2008 Department of Immigration and Citizenship
 * _____________________________________________________________________
 */
package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.configuration.ConfigurationException;
import com.atlassian.bamboo.configuration.DefaultElementParser;
import com.atlassian.bamboo.configuration.ElementParser;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Reimplemented logic contained in {@link com.atlassian.bamboo.configuration.DefaultContentHandler} so that
 * we can cope with changes in the test report XML structure without causing fatal exceptions.
 *
 * @author Ross Rowe
 */
public class TestResultContentHandler implements ContentHandler
{
    private Map<String, ElementParser> elementParsers;
    private Deque<ElementParser> currentElementParsers;

    public TestResultContentHandler()
    {
        this.elementParsers = new HashMap<>();
        this.currentElementParsers = new LinkedList<>();
    }

    protected void registerElementParser(String elementName, ElementParser elementParser)
    {
        this.elementParsers.put(elementName, elementParser);
    }

    @Override
    public void setDocumentLocator(Locator locator)
    {
    }

    @Override
    public void startDocument()
    {
    }

    @Override
    public void endDocument()
    {
    }

    @Override
    public void startPrefixMapping(String buildName, String buildName1)
    {
    }

    @Override
    public void endPrefixMapping(String buildName)
    {
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
    {
        if (!this.elementParsers.containsKey(localName))
        {
            registerElementParser(localName, new DefaultElementParser());
        }

        ElementParser elementParser = this.elementParsers.get(localName);
        elementParser.startElement(attributes);
        this.currentElementParsers.push(elementParser);

    }

    @Override
    public void endElement(String uri, String localName, String qName)
    {
        ElementParser elementParser = this.currentElementParsers.pop();
        try
        {
            elementParser.endElement();
        }
        catch (ConfigurationException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void characters(char[] chars, int start, int end)
    {
        ElementParser elementParser = this.currentElementParsers.peek();
        elementParser.characters(chars, start, end);
    }

    @Override
    public void ignorableWhitespace(char[] chars, int i, int i1)
    {
    }

    @Override
    public void processingInstruction(String buildName, String buildName1)
    {
    }

    @Override
    public void skippedEntity(String buildName)
    {
    }
}