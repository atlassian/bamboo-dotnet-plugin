package com.atlassian.bamboo.plugin.dotnet.visualstudio;

import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.spring.container.LazyComponentReference;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.base.Preconditions;
import net.jcip.annotations.ThreadSafe;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Extracts various wrapper scripts from the plugin classpath in a thread safe way
 */
@ThreadSafe
public class RunnerExtractor
{
    private static final LazyComponentReference<BuildDirectoryManager> BUILD_DIRECTORY_MANAGER_REFERENCE = new LazyComponentReference<>("buildDirectoryManager");

    private static final LazyReference<String> DOTNET_RUNNERS_HOME = new LazyReference<String>()
    {
        @Override
        protected String create()
        {
            File file = new File(BUILD_DIRECTORY_MANAGER_REFERENCE.get().getApplicationHome(), "DotNetSupport");
            file.mkdirs();
            return file.getAbsolutePath();
        }
    };

    private static final LazyReference<String> DEVENV_RUNNER_REFERENCE = new LazyReference<String>()
    {
        @Override
        protected String create() throws Exception
        {
            return extract("/com/atlassian/bamboo/plugin/dotnet/visualstudio/devenvrunner.bat", "devenvrunner.bat");
        }
    };

    private RunnerExtractor()
    {
    }

    /**
     * Get the path to Bamboo's external devenv wrapper batch file
     * @return path
     */
    public static String getDevenvRunnerPath()
    {
        return DEVENV_RUNNER_REFERENCE.get();
    }

    private static String extract(@NotNull String resourceName, String filename) throws IOException
    {
        try (InputStream inputStream = RunnerExtractor.class.getResourceAsStream(resourceName))
        {
            Preconditions.checkState(inputStream != null, "Could not find '%s' on classpath", resourceName);

            final File file = new File(DOTNET_RUNNERS_HOME.get(), filename);
            try (OutputStream outputStream = new FileOutputStream(file)) {
                IOUtils.copy(inputStream, outputStream);
                return file.getAbsolutePath();
            }
        }
    }
}
