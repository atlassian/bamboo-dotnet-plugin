package com.atlassian.bamboo.plugin.dotnet.visualstudio;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.logger.interceptors.LogMemorisingInterceptor;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugin.dotnet.support.DotNetLogHelper;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class VisualStudioTaskType implements TaskType
{
    public static final String VS_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".devenv";
    public static final String SOLUTION = "solution";
    public static final String OPTIONS = "options";
    public static final String LABEL = "label";
    public static final String VS_ENVIRONMENT = "vsEnvironment";
    public static final String ENVIRONMENT = "environmentVariables";
    private static final int LINES_TO_PARSE_FOR_ERRORS = 200;

    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;

    public VisualStudioTaskType(final ProcessService processService, final EnvironmentVariableAccessor environmentVariableAccessor, final CapabilityContext capabilityContext)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
    }

    @Override
    @NotNull
    public TaskResult execute(@NotNull final TaskContext taskContext)
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();
        final CurrentBuildResult currentBuildResult = taskContext.getBuildContext().getBuildResult();

        LogMemorisingInterceptor recentLogLines = new LogMemorisingInterceptor(LINES_TO_PARSE_FOR_ERRORS);
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();

        buildLogger.getInterceptorStack().add(recentLogLines);
        buildLogger.getInterceptorStack().add(errorLines);

        try
        {
            final String runnerPath = RunnerExtractor.getDevenvRunnerPath();
            Preconditions.checkNotNull(runnerPath, "Visual Studio Runner");

            final ConfigurationMap configuration = taskContext.getConfigurationMap();
            
            String visualStudioEnvironment = configuration.get(VS_ENVIRONMENT);
            if (StringUtils.isEmpty(visualStudioEnvironment))
            {
                visualStudioEnvironment = VisualStudioConfigurator.DEFAULT_VS_ENVIRONMENT;
            }
            Preconditions.checkNotNull(visualStudioEnvironment, "Visual Studio Environment");

            final String label = configuration.get(LABEL);
            Preconditions.checkNotNull(label);

            final String solution = configuration.get(SOLUTION);
            Preconditions.checkNotNull(solution, "Solution");

            final Map<String, String> environment = getEnvironment(taskContext);
            Preconditions.checkNotNull(environment, "Process Environment");

            final String installationDirectory = getVisualStudioInstallationDirectory(label);

            Preconditions.checkNotNull(installationDirectory, "Could not find Visual Studio installation directory");

            final List<String> command = new ArrayList<>(Arrays.asList(runnerPath,
                                                                       taskContext.getWorkingDirectory().getAbsolutePath(),
                                                                       installationDirectory,
                                                                       visualStudioEnvironment,
                                                                       solution));

            command.addAll(getOptions(taskContext));

            return TaskResultBuilder.newBuilder(taskContext)
                    .checkReturnCode(processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
                            .command(command)
                            .env(environment)
                            .workingDirectory(taskContext.getWorkingDirectory())))
                    .build();
        }
        finally
        {
            currentBuildResult.addBuildErrors(errorLines.getErrorStringList());
            currentBuildResult.addBuildErrors(DotNetLogHelper.parseErrorOutput(recentLogLines.getLogEntries()));
        }
    }

    @NotNull
    private Map<String, String> getEnvironment(final TaskContext taskContext)
    {
        final String environment = taskContext.getConfigurationMap().get(ENVIRONMENT);
        return environmentVariableAccessor.splitEnvironmentAssignments(environment);
    }

    @NotNull
    List<String> getOptions(final TaskContext taskContext)
    {
        final String options = taskContext.getConfigurationMap().get(OPTIONS);

        return ImmutableList.copyOf(CommandlineStringUtils.tokeniseCommandline(options));
    }

    private String getVisualStudioInstallationDirectory(final String label)
    {
        final Capability capability = capabilityContext.getCapabilitySet().getCapability(VS_CAPABILITY_PREFIX + "." + label);
        Preconditions.checkNotNull(capability, "Capability");
        return new File(capability.getValue()).getParentFile().getParent();
    }
}
