package com.atlassian.bamboo.plugin.dotnet.tests.mstest;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MSTestParserExporter implements TaskDefinitionExporter {
    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties) {
        final TestParserTaskProperties testParserTaskProperties = Narrow.downTo(taskProperties, TestParserTaskProperties.class);
        if (testParserTaskProperties != null) {
            final Map<String, String> cfg = new HashMap<>();
            cfg.put(MSTestCollectorTaskType.TEST_DIRECTORY, Joiner.on(", ").join(testParserTaskProperties.getResultDirectories()));
            cfg.put(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, Boolean.toString(testParserTaskProperties.getPickUpTestResultsCreatedOutsideOfThisBuild()));
            return cfg;
        }
        throw new IllegalStateException("Don't know how to import task properties of type: " + taskProperties.getClass().getName());
    }

    @NotNull
    @Override
    public Task toSpecsEntity(@NotNull TaskDefinition taskDefinition) {
        final Map<String, String> taskConfiguration = taskDefinition.getConfiguration();
        final TestParserTask taskBuilder = new TestParserTask(TestParserTaskProperties.TestType.MSTEST);

        if (taskConfiguration.containsKey(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE)) {
            final Boolean pickUpTestResultsCreatedOutsideOfThisBuild =
                    Boolean.parseBoolean(taskConfiguration.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE));
            taskBuilder.pickUpTestResultsCreatedOutsideOfThisBuild(pickUpTestResultsCreatedOutsideOfThisBuild);
        }
        Splitter.on(",")
                .omitEmptyStrings()
                .trimResults()
                .split(taskConfiguration.get(MSTestCollectorTaskType.TEST_DIRECTORY))
                .forEach(taskBuilder::resultDirectories);
        return taskBuilder;
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        return Collections.emptyList();
    }
}
