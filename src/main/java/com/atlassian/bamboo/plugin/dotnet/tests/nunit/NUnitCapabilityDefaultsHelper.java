package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.plugin.dotnet.msbuild.WindowsSystemPaths;
import com.atlassian.bamboo.utils.Which;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX;

/**
 * Automatically detects the capability of Bamboo agents to execute NUnit tests.
 */
public class NUnitCapabilityDefaultsHelper implements CapabilityDefaultsHelper
{
    // Example matches: "NUnit 2.4.8", "NUnit 2.5.10", "NUnit 2.6"
    private static final Pattern NUNIT_DIRECTORY_PATTERN = Pattern.compile("NUnit \\d+\\.\\d+(\\.\\d+)?");

    private static final String NUNIT3_CONSOLE_EXE = "nunit3-console.exe";
    private static final String NUNIT_CONSOLE_EXE = "nunit-console.exe";
    private static final String NUNIT_CONSOLE_X86_EXE = "nunit-console-x86.exe";
    
    private static final FilenameFilter NUNIT_DIRECTORY_MATCHER = (dir, name) ->
            NUNIT_DIRECTORY_PATTERN.matcher(name).matches();
    
    @Override
    @NotNull
    public CapabilitySet addDefaultCapabilities(@NotNull final CapabilitySet capabilitySet)
    {
        return SystemUtils.IS_OS_WINDOWS ? getWindowsCapabilityDefaults(capabilitySet) : getGenericCapabilityDefaults(capabilitySet);
    }

    @NotNull
    private static CapabilitySet getGenericCapabilityDefaults(@NotNull final CapabilitySet capabilitySet)
    {
        addExecutable(capabilitySet, NUNIT_CONSOLE_EXE, NUNIT_CAPABILITY_PREFIX + ".NUnit");
        addExecutable(capabilitySet, NUNIT3_CONSOLE_EXE, NUNIT3_CAPABILITY_PREFIX + ".NUnit 3");
        addExecutable(capabilitySet, NUNIT_CONSOLE_X86_EXE, NUNIT_CAPABILITY_PREFIX + ".NUnit (32bit)");

        return capabilitySet;
    }

    private static void addExecutable(@NotNull final CapabilitySet capabilitySet, final String executableName, final String capabilityKey)
    {
        final String path = Which.execute(executableName);
        if (!StringUtils.isBlank(path))
        {
            final Capability capability = new CapabilityImpl(capabilityKey, path);
            capabilitySet.addCapability(capability);
        }
    }

    @NotNull
    private static CapabilitySet getWindowsCapabilityDefaults(@NotNull final CapabilitySet capabilitySet)
    {
        // By default, NUnit is not installed on the system %PATH%. To target the path of least resistance, let's
        // enumerate the default NUnit install directories looking for runners.
        for (final File installDirectory : getNunitInstallDirectories())
        {
            if (!installDirectory.isDirectory())
            {
                continue;
            }
            // Probe this directory for the console executables. Look for both the "Any CPU" and "x86" versions.
            // Look in both the "bin/" and "/bin/net-2.0/" directories.
            probeDirectory(installDirectory, capabilitySet);
        }
        return capabilitySet;
    }

    private static Iterable<File> getNunitInstallDirectories()
    {
        return WindowsSystemPaths.getProgramFilesDirectories()
                .stream()
                .map(NUnitCapabilityDefaultsHelper::listNunitDirs)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private static List<File> listNunitDirs(final File dir)
    {
        return Arrays.asList(MoreObjects.firstNonNull(dir.listFiles(NUNIT_DIRECTORY_MATCHER), new File[0]));
    }

    private static void probeDirectory(@NotNull final File installDirectory, @NotNull final CapabilitySet capabilitySet)
    {
        // The actual location of NUnit's binaries in the install directory may vary, depending on the version of NUnit
        // installed.  The 2.5.x installers like to put things in an ancillary "net-2.0" folder. The 2.4.x and 2.6.x
        // installers put everything directly in the "bin" directory.
        // Examples: "C:\Program Files\NUnit 2.5.9\bin\net-2.0\nunit-console.exe"
        //           "C:\Program Files (x86)\NUnit 2.6\bin\nunit-console-x86.exe"
        
        final String nunitCapabilityKey = NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX + "." + installDirectory.getName();
        final String nunit3CapabilityKey = NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX + "." + installDirectory.getName();
        
        addAsCapabilityIfExists(new File(installDirectory, "bin/net-2.0/" + NUNIT_CONSOLE_EXE), capabilitySet, nunitCapabilityKey);
        addAsCapabilityIfExists(new File(installDirectory, "bin/net-2.0/" + NUNIT_CONSOLE_X86_EXE), capabilitySet, nunitCapabilityKey + " (32bit)");
        addAsCapabilityIfExists(new File(installDirectory, "bin/" + NUNIT_CONSOLE_EXE), capabilitySet, nunitCapabilityKey);
        addAsCapabilityIfExists(new File(installDirectory, "bin/" + NUNIT_CONSOLE_X86_EXE), capabilitySet, nunitCapabilityKey + " (32bit)");
        
        addAsCapabilityIfExists(new File(installDirectory, "bin/" + NUNIT3_CONSOLE_EXE), capabilitySet, nunit3CapabilityKey);
    }

    private static void addAsCapabilityIfExists(final File executable, @NotNull final CapabilitySet capabilitySet, final String capabilityKey)
    {
        if (executable.isFile())
        {
            final Capability capability = new CapabilityImpl(capabilityKey, executable.getAbsolutePath());
            capabilitySet.addCapability(capability);
        }
    }
}
