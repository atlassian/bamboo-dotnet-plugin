package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

/**
 * Defines configuration requirements for {@link NUnitRunnerTaskType} tasks. For full details on the features and
 * switches supported by the nunit console, see @see <a href="http://www.nunit.org/index.php?p=consoleCommandLine&r=2.4">NUnit's documentation</a>.
 */
public class NUnitRunnerConfigurator extends AbstractDotNetTaskConfigurator implements TaskTestResultsSupport, BuildTaskRequirementSupport
{
    private static final String DEFAULT_TEST_FILE = "TestResult.xml";

    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            NUnitRunnerTaskType.CAPABILITY_KEY,
            NUnitRunnerTaskType.TEST_FILES,
            NUnitRunnerTaskType.RESULTS_FILE,
            NUnitRunnerTaskType.TESTS_TO_RUN,
            NUnitRunnerTaskType.INCLUDED_TEST_CATEGORIES,
            NUnitRunnerTaskType.EXCLUDED_TEST_CATEGORIES,
            NUnitRunnerTaskType.COMMAND_OPTIONS,
            NUnitRunnerTaskType.ENVIRONMENT
    );

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final String label = params.getString(NUnitRunnerTaskType.CAPABILITY_KEY);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(NUnitRunnerTaskType.CAPABILITY_KEY, textProvider.getText("nunit.label.error"));
        }

        final String testFiles = params.getString(NUnitRunnerTaskType.TEST_FILES);
        if (StringUtils.isEmpty(testFiles))
        {
            errorCollection.addError(NUnitRunnerTaskType.TEST_FILES, textProvider.getText("nunit.testFiles.error"));
        }

        final String resultsFile = params.getString(NUnitRunnerTaskType.RESULTS_FILE);
        if (StringUtils.isNotBlank(resultsFile))
        {
            if (!resultsFile.endsWith(".xml"))
            {
                errorCollection.addError(NUnitRunnerTaskType.RESULTS_FILE, textProvider.getText("nunit.resultsFile.incorrectExtension"));
            }
        }
        else
        {
            errorCollection.addError(NUnitRunnerTaskType.RESULTS_FILE, textProvider.getText("nunit.resultsFile.missing"));
        }
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        // Fill in defaults
        super.populateContextForCreate(context);
        context.put(NUnitRunnerTaskType.RESULTS_FILE, DEFAULT_TEST_FILE);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }

    @Override
    public boolean taskProducesTestResults(@NotNull final TaskDefinition taskDefinition)
    {
        return true;
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull final TaskDefinition taskDefinition, @NotNull final Job job)
    {
        return Sets.newHashSet(taskConfiguratorHelper.createRequirementFromConfiguration(taskDefinition, NUnitRunnerTaskType.CAPABILITY_KEY));
    }
}
