package com.atlassian.bamboo.plugin.dotnet.nant;

import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.ExecutablePathUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class NantCapabilityDefaultsHelper implements CapabilityDefaultsHelper
{
    private static final Logger log = Logger.getLogger(NantCapabilityDefaultsHelper.class);
    // ------------------------------------------------------------------------------------------------------- Constants

    private static final String EXECUTABLE_NAME = SystemUtils.IS_OS_WINDOWS ? "nant.exe" : "nant";
    private static final String NANT_LABEL = "Nant";

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods

    @Override
    @NotNull
    public CapabilitySet addDefaultCapabilities(@NotNull final CapabilitySet capabilitySet)
    {

        final String executablePath = ExecutablePathUtils.findExecutablePath(EXECUTABLE_NAME);

        if (StringUtils.isNotEmpty(executablePath))
        {
            final File file = new File(executablePath);
            if (file.exists() && file.isFile())
            {
                final File home = ExecutablePathUtils.getHomeFromExecutableInHomeBin(file);
                if (home != null && home.isDirectory())
                {
                    Capability capability = new CapabilityImpl(NantTaskType.NANT_CAPABILITY_PREFIX + "." + NANT_LABEL, home.getAbsolutePath());
                    capabilitySet.addCapability(capability);
                }
                else
                {
                    log.error("Found NAnt executable at '" + executablePath + "' but could not get the home directory from the path");
                }
            }
        }

        return capabilitySet;
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
