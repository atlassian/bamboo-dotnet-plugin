package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.web.Condition;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Contains logic to control whether a 'NCover' tab is displayed on the Results
 * results page.
 *
 * @author Ross Rowe
 *
 */
public class NCoverResultWebItemCondition implements Condition {

	@Autowired
	@BambooImport
	private ResultsSummaryManager resultsSummaryManager;

	@Override
    public void init(Map map) throws PluginParseException {

	}

	/**
	 * Only display the NCover tab item if the appropriate configuration
	 * settings have been set.
	 *
	 * @param context
	 * @return boolean indicating whether NCover tab item should be displayed
	 */
	@Override
    public boolean shouldDisplay(Map<String, Object> context) {

		String planKey = (String) context.get(NCoverBuildProcessor.BUILD_KEY);
		String buildNumber = (String) context.get(NCoverBuildProcessor.BUILD_NUMBER);

		if (planKey == null || buildNumber == null)
			return false;

        PlanResultKey planResultKey = PlanKeys.getPlanResultKey(planKey, Integer.parseInt(buildNumber));
        ResultsSummary summary = resultsSummaryManager.getResultsSummary(planResultKey);
		if (summary == null) {
			return false;
		} else {
			Map<String, String> customData = summary.getCustomBuildData();
			return customData.containsKey(NCoverBuildProcessor.NCOVER_LINE_RATE);
		}
	}
}
