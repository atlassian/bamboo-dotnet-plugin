package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitTestReportCollector;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.utils.FileVisitor;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Set;

/**
 * {@link FileVisitor} subclass that handles identifying NUnit result files.
 * 
 * @author Ross Rowe
 * 
 */
public class TestResultFileVisitor extends FileVisitor {

	private static final Logger log = Logger
			.getLogger(TestResultFileVisitor.class);

	int[] numberOfFilesFound;

	Set<TestResults> failedTestResults;

	Set<TestResults> successfulTestResults;

	private TestResultsParser parser;

	protected TestResultFileVisitor(File file) {
		super(file);
	}

	/**
	 * Constructs a {@link TestResultFileVisitor} instance.
	 * 
	 * @param file
	 * @param numberOfFilesFound
	 * @param failedTestResults
	 * @param successfulTestResults
	 */
	protected TestResultFileVisitor(File file, int[] numberOfFilesFound,
			Set<TestResults> failedTestResults,
			Set<TestResults> successfulTestResults, TestResultsParser parser) {
		this(file);
		this.numberOfFilesFound = numberOfFilesFound;
		this.failedTestResults = failedTestResults;
		this.successfulTestResults = successfulTestResults;
		this.parser = parser;
	}

	/**
	 * If the <code>file</code>'s name ends with 'xml', then invoke the
	 * {@link NUnitTestReportCollector} on the result file.
	 * 
	 * @param file
	 *            {@link File} being visited
	 */
	@Override
	public void visitFile(File file) {
		log.debug("Processing file: " + file.getAbsolutePath());
		if (file.getName().endsWith("xml") || file.getName().endsWith("trx"))
			try {
                log.info("Running parse on file: " + file.getName());
				numberOfFilesFound[0]++;
				TestReportCollector testReportCollector = new TestReportCollectorImpl(parser);
				TestCollectionResult result = testReportCollector.collect(file);
				failedTestResults.addAll(result.getFailedTestResults());
				successfulTestResults.addAll(result.getSuccessfulTestResults());
			} catch (Exception e) {
				log.error("Failed to parse test result files \""
						+ file.getName() + "\"", e);
			}
	}

}