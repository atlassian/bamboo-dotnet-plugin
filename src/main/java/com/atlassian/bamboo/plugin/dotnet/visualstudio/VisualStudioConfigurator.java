package com.atlassian.bamboo.plugin.dotnet.visualstudio;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugin.dotnet.support.AbstractDotNetTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

public class VisualStudioConfigurator extends AbstractDotNetTaskConfigurator implements BuildTaskRequirementSupport
{
    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            VisualStudioTaskType.LABEL,
            VisualStudioTaskType.OPTIONS,
            VisualStudioTaskType.SOLUTION,
            VisualStudioTaskType.VS_ENVIRONMENT,
            VisualStudioTaskType.ENVIRONMENT,
            TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY
    );
    public static final String DEFAULT_VS_ENVIRONMENT = "x86";
    private static final String[] environments = new String[] {DEFAULT_VS_ENVIRONMENT, "ia64" , "amd64" , "x86_amd64" , "x86_ia64"};
    private static final String ENVIRONMENTS = "environments";

    @Override
    @NotNull
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Job buildable)
    {
        final String label = taskDefinition.getConfiguration().get(VisualStudioTaskType.LABEL);
        return Sets.newHashSet(new RequirementImpl(VisualStudioTaskType.VS_CAPABILITY_PREFIX + "." + label, true, ".*"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap actionParametersMap, @NotNull final ErrorCollection errorCollection)
    {
        final String solution = actionParametersMap.getString(VisualStudioTaskType.SOLUTION);
        if (StringUtils.isEmpty(solution))
        {
            errorCollection.addError(VisualStudioTaskType.SOLUTION, textProvider.getText("visualstudio.solution.error"));
        }

        final String options = actionParametersMap.getString(VisualStudioTaskType.OPTIONS);
        if (StringUtils.isEmpty(options))
        {
            errorCollection.addError(VisualStudioTaskType.OPTIONS, textProvider.getText("visualstudio.options.error"));
        }

        final String label = actionParametersMap.getString(VisualStudioTaskType.LABEL);
        if (StringUtils.isEmpty(label))
        {
            errorCollection.addError(VisualStudioTaskType.LABEL, textProvider.getText("visualstudio.label.error"));
        }
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        context.put(ENVIRONMENTS, environments);
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(ENVIRONMENTS, environments);
        context.put(VisualStudioTaskType.VS_ENVIRONMENT, DEFAULT_VS_ENVIRONMENT);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }
}
