package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.build.FilterController;
import com.atlassian.bamboo.plugin.dotnet.DotNetPlugin;
import com.atlassian.bamboo.reports.collector.ReportCollector;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.ww2.actions.BuildActionSupport;
import com.atlassian.bamboo.ww2.aware.ResultsListAware;
import com.atlassian.bamboo.ww2.aware.permissions.PlanReadSecurityAware;
import com.atlassian.plugin.ModuleDescriptor;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

import java.util.Collections;
import java.util.List;

/**
 * Action class used as part of NCover report display.
 * 
 * @author Ross Rowe
 * 
 */
public class ViewNCoverCoverageSummary extends BuildActionSupport implements ResultsListAware, PlanReadSecurityAware
{
	private static final long serialVersionUID = -3731695875856341713L;

    private static final String COVERAGE_REPORT_KEY = DotNetPlugin.PLUGIN_KEY + ":ncoverCoverage";

	// Charting
	List<? extends ResultsSummary> resultsList;

	XYDataset dataset;

	String reportKey = COVERAGE_REPORT_KEY; // default.

	// --------------------------------------------------------------------------------------------------
	// Filter
	FilterController filterController;

	/**
	 * Runs the viewCoverage action.
	 * 
	 * @return the success status
	 */
	public String doViewCoverage()
	{
		setReportKey(COVERAGE_REPORT_KEY);
		return run();
	}

	/**
	 * 
	 * @return the success status
	 */
	@Override
    public String execute()
	{
		return run();
	}

	/**
	 * Sets the dataset variable as per the <code>ReportCollector</code> that
	 * is applicable for the plugin descriptor.
	 * 
	 * @return the success status
	 */
	private String run() {
		if (resultsList != null && !resultsList.isEmpty()) {
			ModuleDescriptor descriptor = getPluginAccessor().getPluginModule(getReportKey());

			if (descriptor != null) {
				ReportCollector collector = (ReportCollector) descriptor
						.getModule();
				collector.setResultsList(getResultsList());
				collector.setParams(Collections.emptyMap());
				dataset = (TimeTableXYDataset) collector.getDataset();
			}
		}

		return SUCCESS;
	}

	@Override
    public List<? extends ResultsSummary> getResultsList() {
		return resultsList;
	}

	@Override
    public void setResultsList(List<? extends ResultsSummary> resultsList) {
		this.resultsList = resultsList;
	}

	public XYDataset getDataset() {
		return dataset;
	}

	public void setDataset(XYDataset dataset) {
		this.dataset = dataset;
	}

	public String getReportKey() {
		return reportKey;
	}

	public void setReportKey(String reportKey) {
		this.reportKey = reportKey;
	}

	// Dependencies
	public FilterController getFilterController() {
		return filterController;
	}

	public void setFilterController(FilterController filterController) {
		this.filterController = filterController;
	}
}
