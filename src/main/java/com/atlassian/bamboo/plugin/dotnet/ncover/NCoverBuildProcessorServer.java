package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.bamboo.build.CustomBuildProcessorServer;
import com.atlassian.bamboo.resultsummary.ResultDataRead;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.util.NumberUtils;
import com.atlassian.bamboo.v2.build.BaseConfigurableBuildPlugin;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class NCoverBuildProcessorServer extends BaseConfigurableBuildPlugin
        implements CustomBuildProcessorServer {

    public static final String LINE_SEPARATOR = System
            .getProperty("line.separator");

    private static final Logger log = Logger.getLogger(NCoverBuildProcessorServer.class);

    @Inject
    @BambooImport
    private ResultsSummaryManager resultsSummaryManager;

    @Inject
    @BambooImport
    private TransactionTemplate transactionTemplate;

    @Override
    @NotNull
    public BuildContext call()
    {
        CurrentBuildResult buildResult = buildContext.getBuildResult();

        // only calculate delta when NCover is enabled
        final Map<String, String> customConfiguration = buildContext.getBuildDefinition()
                .getCustomConfiguration();
        if (Boolean.parseBoolean(customConfiguration.get(NCoverBuildProcessor.NCOVER_EXISTS)))
        {
            calculateDelta(buildResult.getCustomBuildData());
        }
        return buildContext;
    }

    private void calculateDelta(Map<String, String> results) {
        transactionTemplate.execute(() -> {
            try
            {
                ResultsSummary previousSummary = resultsSummaryManager.getLastSuccessfulResultSummary(buildContext.getPlanResultKey().getPlanKey());

                if (previousSummary != null && previousSummary.getCustomBuildData() != null && previousSummary.getCustomBuildData().containsKey(
                        NCoverBuildProcessor.NCOVER_LINE_RATE))
                {
                    String prevCoverStr = previousSummary
                            .getCustomBuildData().get(NCoverBuildProcessor.NCOVER_LINE_RATE);
                    String lineRate = results
                            .get(NCoverBuildProcessor.NCOVER_LINE_RATE);
                    if (!prevCoverStr.equals(lineRate))
                    {
                        runCoverageChangeComparison(previousSummary, results);
                    }

                    double prevCoverDbl = NumberUtils.unlocalizedStringToDouble(prevCoverStr);
                    double coverage = NumberUtils.unlocalizedStringToDouble(lineRate);
                    results.put(NCoverBuildProcessor.NCOVER_COVERAGE_DELTA, Double
                            .toString(coverage - prevCoverDbl));
                }

            } catch (NumberFormatException e) {
                log.error(e);
            }
            return null;
        });
    }

    /**
     * Compares the class/line rates from this build and the previous build.
     * Any differences between coverage line rates are recorded in
     * {@link NCoverCoverageInformation} instances, which are written
     * into the {@link com.atlassian.bamboo.resultsummary.BuildResultsSummary}'s custom data map.
     *
     * @param previousSummary the {@link com.atlassian.bamboo.resultsummary.BuildResultsSummary} instance for the previous
     *                        build
     */
    @SuppressWarnings("unchecked")
    private void runCoverageChangeComparison(ResultsSummary previousSummary, Map<String, String> results)
    {
        List<NCoverCoverageInformation> coverageInfoList = new ArrayList<>();
        List<NCoverCoverageInformation> coverageChangeList = new ArrayList<>();

        // build map of current class/coverages
        Map<String, String> currentMap = buildClassCoverageMap(results
                .get(NCoverBuildProcessor.NCOVER_RESULT_CONTENTS));
        String previousCsv = previousSummary.getCustomBuildData()
                .get(NCoverBuildProcessor.NCOVER_RESULT_CONTENTS);
        if (previousCsv == null || StringUtils.isEmpty(previousCsv))
            return;
        Map<String, String> previousMap = buildClassCoverageMap(previousCsv);
        for (Object o : currentMap.keySet()) {
            String className = (String) o;
            String lineRate = currentMap.get(className);
            String oldLineRate = previousMap.get(className);
            if (StringUtils.isEmpty(lineRate)
                    || StringUtils.isEmpty(oldLineRate))
                continue;
            Double difference = NumberUtils.unlocalizedStringToDouble(lineRate)
                    - NumberUtils.unlocalizedStringToDouble(oldLineRate);

            // if the line rates are different
            if (!difference.equals(0.0D))
                coverageInfoList
                        .add(new NCoverCoverageInformation(className,
                                NumberUtils.unlocalizedStringToDouble(lineRate), difference));
        }
        if (coverageInfoList.isEmpty()) {
            log.info("No differences in coverage found");
        } else {
            // sort collection
            Collections.sort(coverageInfoList);
            int size = Math.min(coverageInfoList.size(), 10);
            // TODO populate two collections (positive/negative changes)?
            for (int i = 0; i < size; i++) {

                coverageChangeList.add(coverageInfoList.get(i));
            }
            results.put(NCoverBuildProcessor.NCOVER_COVERAGE_CHANGES,
                    convertToCsv(coverageChangeList));
        }
    }

    /**
     * Converts a list of {@link NCoverCoverageInformation} into a CSV
     * format.
     *
     * @return String in CSV format
     */
    private String convertToCsv(
            List<NCoverCoverageInformation> coverageChangeList) {
        StringBuffer buffer = new StringBuffer();
        for (NCoverCoverageInformation information : coverageChangeList) {
            buffer.append(information.getClassName()).append(",");
            buffer.append(information.getLineRate()).append(",");
            buffer.append(information.getDelta());
            buffer.append(LINE_SEPARATOR);
        }
        return buffer.toString();
    }

    /**
     * Builds a Map of classes/line rates of the classes that were covered
     * in the NCover coverage report.
     *
     * @param csv
     * @return Map of class names/line rates
     */
    private Map<String, String> buildClassCoverageMap(String csv) {
        Map<String, String> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new StringReader(csv));
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(line, ",");
                String className = token.nextToken();
                String lineRate = token.nextToken();
                map.put(className, lineRate);
            }
        } catch (IOException e) {
            log.error("Error parsing csv", e);
        }

        return map;
    }

    @VisibleForTesting
    ResultsSummaryManager getResultsSummaryManager()
    {
        return resultsSummaryManager;
    }
}
