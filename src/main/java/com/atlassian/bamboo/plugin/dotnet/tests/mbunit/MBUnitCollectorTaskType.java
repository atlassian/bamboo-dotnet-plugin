package com.atlassian.bamboo.plugin.dotnet.tests.mbunit;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.plugin.dotnet.tests.AbstractDotNetTestCollectorTaskType;
import com.atlassian.bamboo.plugin.dotnet.tests.AbstractDotNetTestReportCollector;
import com.atlassian.bamboo.task.TaskContext;
import org.jetbrains.annotations.NotNull;

public class MBUnitCollectorTaskType extends AbstractDotNetTestCollectorTaskType
{
    public static final String TEST_DIRECTORY = "mbunitTestResultsDirectory";

    public MBUnitCollectorTaskType(final TestCollationService testCollationService)
    {
        super(testCollationService);
    }

    @Override
    protected AbstractDotNetTestReportCollector getTestReportCollector()
    {
        return new MBUnitTestReportCollector();
    }

    @Override
    protected String getFilePattern(@NotNull final TaskContext taskContext)
    {
        return taskContext.getConfigurationMap().get(TEST_DIRECTORY);
    }
}
