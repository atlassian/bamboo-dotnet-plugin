package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.reports.charts.BambooReportLineChart;
import com.atlassian.bamboo.util.NumberUtils;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

/**
 * {@link BambooReportLineChart} subclass that is used as part of displaying the line rate chart.
 * 
 * @author Ross Rowe
 *
 */
public class BuildSummaryCoverageLineChart extends BambooReportLineChart implements XYToolTipGenerator
{

	/**
	 * Constructs a new BuildSummaryCoverageLineChart instance.
	 *
	 */
    public BuildSummaryCoverageLineChart()
    {
        setyAxisLabel("% Line Rate");
    }

    /**
     * Generates the tool tip to show.
     * 
     * @param xyDataset
     * @param series
     * @param item
     * @return the tool tip to show
     */
    @Override
    public String generateToolTip(XYDataset xyDataset, int series, int item)
    {
        TimeTableXYDataset dataset = (TimeTableXYDataset) xyDataset;

        double percentageCovered = dataset.getYValue(series, item);
        String buildKey = (String) dataset.getSeriesKey(series);
        TimePeriod timePeriod = dataset.getTimePeriod(item);

        return "Tests in " +timePeriod+ " covered " + NumberUtils.round(percentageCovered, 1) + "% of code in build " + buildKey;
    }
}
