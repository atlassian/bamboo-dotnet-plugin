package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.results.tests.TestResults;
import com.google.common.collect.ImmutableList;

import java.io.InputStream;

/**
 * @author Ross Rowe
 *
 */
public interface TestResultsParser
{
	/**
	 * 
	 * @param inputStream
	 */
	void parse(InputStream inputStream);

	ImmutableList<TestResults> getSuccessfulTests();
	
	ImmutableList<TestResults> getFailedTests();

	ImmutableList<TestResults> getInconclusiveTests();
}
