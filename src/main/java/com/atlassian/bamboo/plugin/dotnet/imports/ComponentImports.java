package com.atlassian.bamboo.plugin.dotnet.imports;

import com.atlassian.bamboo.build.CookieCutter;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.web.WebInterfaceManager;

/*
* Required for spring-scanner.
*/
@SuppressWarnings("unused")
public class ComponentImports {
    @BambooImport
    private TemplateRenderer templateRenderer;
    @BambooImport
    private CookieCutter cookieCutter;
    @BambooImport
    private BambooPermissionManager bambooPermissionManager;
    @BambooImport
    private WebInterfaceManager webInterfaceManager;
    @BambooImport
    private PluginAccessor pluginAccessor;
}
