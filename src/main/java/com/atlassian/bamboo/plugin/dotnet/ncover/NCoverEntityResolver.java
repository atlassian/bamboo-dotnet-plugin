package com.atlassian.bamboo.plugin.dotnet.ncover;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

import java.io.InputStream;

/**
 * @author Ross Rowe
 */
public class NCoverEntityResolver
        implements EntityResolver {
    @Override
    public InputSource resolveEntity(String publicId, String systemId)
    {
        InputStream in = getClass().getResourceAsStream("/coverage-03.dtd");
        return new InputSource(in);
    }
}