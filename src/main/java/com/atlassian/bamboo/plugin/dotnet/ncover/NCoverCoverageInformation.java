/**
 * 
 */
package com.atlassian.bamboo.plugin.dotnet.ncover;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * POJO that records information retrieved from the NCover XML report
 * including the class name and line rate, as well as the difference in line
 * rate since the previous build (delta).
 * 
 * @author Ross Rowe
 * 
 */
public class NCoverCoverageInformation implements Serializable, Comparable {

	private static final long serialVersionUID = 6630324303579285331L;

	private String className;

	private Double lineRate;

	private Double delta;

	public NCoverCoverageInformation(String className, Double lineRate,
			Double delta) {
		this.className = className;
		this.lineRate = lineRate;
		this.delta = delta;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Double getDelta() {
		return delta;
	}

	public void setDelta(Double delta) {
		this.delta = delta;
	}

	public Double getLineRate() {
		return lineRate;
	}

	public void setLineRate(Double lineRate) {
		this.lineRate = lineRate;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof NCoverCoverageInformation) {
			NCoverCoverageInformation coverageInfo = (NCoverCoverageInformation) object;
			return coverageInfo.getClassName().equals(this.getClassName());
		}
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(1, 2).append(className).append(lineRate)
				.append(delta).toHashCode();
	}

	/**
	 * Compares the delta of two NCoverCoverageInformation instances.
	 * 
	 * @param object
	 *            Object being compared
	 * @return
	 */
	@Override
	public int compareTo(Object object) {
		if (object instanceof NCoverCoverageInformation) {
			NCoverCoverageInformation coverageInfo = (NCoverCoverageInformation) object;
			return coverageInfo.getDelta() > this.getDelta() ? 1 : 0;
		}
		return 1;
	}

}
