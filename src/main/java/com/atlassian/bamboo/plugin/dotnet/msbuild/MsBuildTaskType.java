package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.logger.interceptors.LogMemorisingInterceptor;
import com.atlassian.bamboo.plugin.dotnet.support.DotNetLogHelper;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.util.SecureTemporaryFiles;
import com.atlassian.bamboo.utils.SystemProperty;
import com.atlassian.bamboo.v2.build.CurrentResult;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MsBuildTaskType implements CommonTaskType {
    public static final String MSBUILD_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".msbuild";
    public static final String PROPERTY_USE_RESPONSE_FILE = "bamboo.plugin.dotnet.msbuild.useResponseFile";

    private static final int LINES_TO_PARSE_FOR_ERRORS = 200;
    // TODO next upgrade should use response file as default option
    private static final SystemProperty.BooleanSystemProperty USE_RESPONSE_FILE = new SystemProperty.BooleanSystemProperty(false, true, PROPERTY_USE_RESPONSE_FILE);

    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;

    public MsBuildTaskType(final ProcessService processService,
                           final EnvironmentVariableAccessor environmentVariableAccessor,
                           final CapabilityContext capabilityContext) {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
    }

    @Override
    public TaskResult execute(@NotNull CommonTaskContext taskContext) throws TaskException {
        final BuildLogger buildLogger = taskContext.getBuildLogger();
        final CurrentResult currentBuildResult = taskContext.getCommonContext().getCurrentResult();

        LogMemorisingInterceptor recentLogLines = new LogMemorisingInterceptor(LINES_TO_PARSE_FOR_ERRORS);
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();

        buildLogger.getInterceptorStack().add(recentLogLines);
        buildLogger.getInterceptorStack().add(errorLines);

        File intermediateFile = null;
        try {
            final MsBuildConfig msBuildConfig = MsBuildConfig.build(taskContext, capabilityContext, environmentVariableAccessor);

            final ExternalProcessBuilder epb;
            if (USE_RESPONSE_FILE.getTypedValue()) {
                intermediateFile = createResponseFile(taskContext, msBuildConfig);
                epb = epbWithResponseFile(msBuildConfig, intermediateFile);
            } else {
                intermediateFile = createBatchFile(msBuildConfig);
                epb = epbWithCommandLine(msBuildConfig, intermediateFile);
            }
            epb
                    .env(msBuildConfig.getEnvironmentMap())
                    .workingDirectory(taskContext.getWorkingDirectory());

            return TaskResultBuilder.newBuilder(taskContext)
                    .checkReturnCode(processService.executeExternalProcess(taskContext, epb))
                    .build();
        } finally {
            FileUtils.deleteQuietly(intermediateFile);
            currentBuildResult.addBuildErrors(errorLines.getErrorStringList());
            currentBuildResult.addBuildErrors(DotNetLogHelper.parseErrorOutput(recentLogLines.getLogEntries()));
        }
    }

    // -------------------------------------------------------------------------------------------------- Helper Methods

    private ExternalProcessBuilder epbWithCommandLine(MsBuildConfig msBuildConfig, @NotNull final File batchFile) {
        return new ExternalProcessBuilder()
                .command(Arrays.asList(batchFile.getAbsolutePath(), getCommand(msBuildConfig)));
    }

    private File createBatchFile(MsBuildConfig msBuildConfig) {
        final String command = getCommand(msBuildConfig);

        final SecureTemporaryFiles.FileSpecBuilder fileSpecBuilder =
                SecureTemporaryFiles.builder()
                        .setPrefix("epb")
                        .setSuffix(".bat")
                        .setExecutable(true);

        File batchFile;
        try {
            batchFile = SecureTemporaryFiles.create(fileSpecBuilder.build());
            FileUtils.writeStringToFile(batchFile, command, Charset.defaultCharset());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        batchFile.setReadOnly();
        return batchFile;
    }

    private String getCommand(MsBuildConfig msBuildConfig) {
        return "\"" + msBuildConfig.getMsBuildExecutable() + "\" " +
                Optional.ofNullable(msBuildConfig.getOptions()).map(opt -> opt + " ").orElse("") +
                msBuildConfig.getSolution();
    }

    private ExternalProcessBuilder epbWithResponseFile(@NotNull final MsBuildConfig msBuildConfig, @NotNull final File responseFile) {
        final List<String> command = Arrays.asList(msBuildConfig.getMsBuildExecutable(),
                "@" + responseFile.getAbsolutePath());

        return new ExternalProcessBuilder()
                .command(command);
    }

    private File createResponseFile(@NotNull CommonTaskContext taskContext, @NotNull MsBuildConfig msBuildConfig) throws TaskException {
        try {
            final String prefix = taskContext.getCommonContext().getResultKey().getKey() + "-" + getClass().getSimpleName() + "-";

            final File responseFile = File.createTempFile(prefix, ".rsp");
            FileUtils.writeLines(
                    responseFile,
                    Stream.of("# MSBuild response file generated by Atlassian Bamboo", msBuildConfig.getOptions(), msBuildConfig.getSolution())
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList())
            );
            responseFile.setReadOnly();
            return responseFile;
        } catch (IOException e) {
            throw new TaskException("Error while creating temporary file for script", e);
        }
    }
}
