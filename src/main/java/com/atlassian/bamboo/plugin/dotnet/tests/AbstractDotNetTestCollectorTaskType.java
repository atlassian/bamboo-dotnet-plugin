package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractDotNetTestCollectorTaskType implements TaskType
{
    private final TestCollationService testCollationService;

    protected AbstractDotNetTestCollectorTaskType(final TestCollationService testCollationService)
    {
        this.testCollationService = testCollationService;
    }

    @Override
    @NotNull
    public TaskResult execute(@NotNull final TaskContext taskContext)
    {
        testCollationService.collateTestResults(taskContext, getFilePattern(taskContext), getTestReportCollector(), getPickupOutdatedFiles(taskContext));
        return TaskResultBuilder.newBuilder(taskContext).checkTestFailures().build();
    }

    /**
     * The {@link AbstractDotNetTestReportCollector} implementation used for collecting {@link TestResults}
     * @return collector
     */
    protected abstract AbstractDotNetTestReportCollector getTestReportCollector();

    /**
     * The file pattern to use to collect test files
     *
     * @param taskContext@return filePattern
     */
    protected abstract String getFilePattern(@NotNull TaskContext taskContext);

    protected boolean getPickupOutdatedFiles(@NotNull final TaskContext taskContext)
    {
        return taskContext.getConfigurationMap().getAsBoolean(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE);
    }
}
