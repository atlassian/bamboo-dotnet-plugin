package com.atlassian.bamboo.plugin.dotnet.support;

import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.struts.TextProvider;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import java.util.Map;

/**
 * Adds the {@link UIConfigSupport} to all the context maps and provides a {@link TextProvider} on the configurator
 */
public abstract class AbstractDotNetTaskConfigurator extends AbstractTaskConfigurator
{
    private static final String CTX_UI_CONFIG_SUPPORT = "uiConfigSupport";

    @Autowired
    @BambooImport
    protected TaskConfiguratorHelper taskConfiguratorHelper;
    @Autowired
    @BambooImport
    protected TextProvider textProvider;
    @Autowired
    @BambooImport
    protected UIConfigSupport uiConfigSupport;

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        populateContextForAllOperations(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAllOperations(context);
    }

    // -------------------------------------------------------------------------------------------------- Action Methods

    private void populateContextForAllOperations(@NotNull Map<String, Object> context)
    {
        context.put(CTX_UI_CONFIG_SUPPORT, uiConfigSupport);
    }

    //backward compatibility with pre-5 platform
    public void setTextProvider(final TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }

    //backward compatibility with pre-5 platform
    public void setUiConfigSupport(UIConfigSupport uiConfigSupport)
    {
        this.uiConfigSupport = uiConfigSupport;
    }
}
