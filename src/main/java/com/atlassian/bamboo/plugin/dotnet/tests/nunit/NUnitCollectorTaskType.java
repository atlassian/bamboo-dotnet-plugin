package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.plugin.dotnet.tests.AbstractDotNetTestCollectorTaskType;
import com.atlassian.bamboo.plugin.dotnet.tests.AbstractDotNetTestReportCollector;
import com.atlassian.bamboo.task.TaskContext;
import org.jetbrains.annotations.NotNull;

public class NUnitCollectorTaskType extends AbstractDotNetTestCollectorTaskType
{
    public static final String TEST_DIRECTORY = "testResultsDirectory";

    public NUnitCollectorTaskType(final TestCollationService testCollationService)
    {
        super(testCollationService);
    }

    @Override
    protected AbstractDotNetTestReportCollector getTestReportCollector()
    {
        return new NUnitTestReportCollector();
    }

    @Override
    protected String getFilePattern(@NotNull final TaskContext taskContext)
    {
        return taskContext.getConfigurationMap().get(TEST_DIRECTORY);
    }
}
