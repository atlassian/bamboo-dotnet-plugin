package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.google.common.collect.Sets;
import net.jcip.annotations.ThreadSafe;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;

@ThreadSafe
public abstract class AbstractDotNetTestReportCollector implements TestReportCollector
{
    @Override
    @NotNull
    public TestCollectionResult collect(@NotNull final File file) throws Exception
    {
        final TestResultsParser testResultsParser = getParser();
        try (InputStream inputStream = new FileInputStream(file))
        {
            testResultsParser.parse(inputStream);
        }

        return new TestCollectionResultBuilder()
                .addSuccessfulTestResults(testResultsParser.getSuccessfulTests())
                .addFailedTestResults(testResultsParser.getFailedTests())
                .addSkippedTestResults(testResultsParser.getInconclusiveTests())
                .build();
    }

    @Override
    @NotNull
    public Set<String> getSupportedFileExtensions()
    {
        return Sets.newHashSet("trx", "xml");
    }

    /**
     * Descendant classes must provide new instance of non thread safe parsers or reuse existing instance of thread safe parser..
     *
     * @return {@link TestResultsParser} object
     */
    @NotNull
    protected abstract TestResultsParser getParser();
}
