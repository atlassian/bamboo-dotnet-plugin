package com.atlassian.bamboo.plugin.dotnet.tests.vstest;

import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bill Tutt
 */
public class VSTestCapabilityDefaultsHelper implements CapabilityDefaultsHelper
{
    private static final Logger log = Logger.getLogger(VSTestCapabilityDefaultsHelper.class);

    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String SUPPORTED_FILE_TYPES = "Supported File Types:";
    private static final String DEFAULT_EXECUTOR_URI = "Default Executor Uri:";

    // Executor URIs that are allowed a friendly name instead of the assembly name.
    // This list comes from built in Visual Studio adapters and test adapters that
    // claimed to support VS 2013.
    private static final String genericTestUri = "executor://generictestadapter/v1";
    private static final String msappContainerUri = "executor://msappcontainertestadapter/v1";
    private static final String mswpContainerUri = "executor://mswptestadapter/v1";
    private static final String orderedTestsUri = "executor://orderedtestadapter/v1";
    private static final String msTestUri = "executor://mstestadapter/v1";
    private static final String cppWpUri = "executor://cppwpunittestexecutor/v1";
    private static final String cppUri = "executor://cppunittestexecutor/v1";
    private static final String pesterUri = "executor://pestertestexecutor/v1";
    private static final String psateUri = "executor://psatetestexecutor/v1";
    private static final String pythonUri = "executor://pythontestexecutor/v1";
    private static final String nunitUri = "executor://nunittestexecutor/";
    private static final String gtestUri = "executor://googletestrunner/v1";
    private static final String mspecUri = "executor://machine.vstestadapter/";
    private static final String chutzpahUri = "executor://chutzpah-js/";
    private static final String typeScriptUri = "executor://typescriptexecutor/v1";
    private static final String xunitUri = "executor://xunit/VsTestRunner2";
    private static final String nodejsToolsUri = "executor://nodejstestexecutor/v1";

    // ------------------------------------------------------------------------------------------------- Type Properties

    private final Map<String, String> uriToFriendlyNames = new HashMap<>();

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods

    @NotNull
    public CapabilitySet addDefaultCapabilities(@NotNull final CapabilitySet capabilitySet)
    {
        if (SystemUtils.IS_OS_WINDOWS)
        {
            // VSTest is only in VS 2012 and newer.
            final String vs2012Location = System.getenv("VS110COMNTOOLS");
            final String vs2013Location = System.getenv("VS120COMNTOOLS");

            createCapabilityForVisualStudio("2012", capabilitySet, vs2012Location);
            createCapabilityForVisualStudio("2013", capabilitySet, vs2013Location);
        }
        return capabilitySet;
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators

    private void createCapabilityForVisualStudio(@NotNull final String label, @NotNull final CapabilitySet capabilitySet, @Nullable final String path)
    {
        if (path == null)
        {
            return;
        }

        final String key = VSTestRunnerTaskType.VSTESTEXE_CAPABILITY_PREFIX + "." + "VSTest Console (VS " + label + ")";

        if (StringUtils.isNotEmpty(path))
        {
            File file = new File(new File(new File(path).getParentFile(), "IDE").getAbsolutePath(), "CommonExtensions");
            file = new File(file.getAbsolutePath(), "Microsoft");
            file = new File(file.getAbsolutePath(), "TestWindow");
            file = new File(file.getAbsolutePath(), VSTestRunnerTaskType.VSTESTEXE);
            if (file.exists() && file.isFile())
            {
                Capability capability = new CapabilityImpl(key, file.getAbsolutePath());
                capabilitySet.addCapability(capability);
                discoverTestDiscoverers(capabilitySet, label, file.getAbsolutePath());
            }
        }
    }

    private void discoverTestDiscoverers(@NotNull final CapabilitySet capabilitySet, @NotNull final String label, @NotNull final String executablePath )
    {
        // VSTest supports test adapters installed via a number of mechanisms.
        // Expose capabilities for each reported discoverer.
        // The capabilities also have friendly names for known Test Discoverers.
        loadFriendlyMap();

        ProcessBuilder processBuilder = new ProcessBuilder();
        List<String> args = new ArrayList<>();
        args.add(executablePath);
        args.add("/UseVsixExtensions:true");
        args.add("/ListDiscoverers");
        processBuilder.command(args);

        Process process = null;
        try
        {
            process = processBuilder.start();
            process.waitFor();
        }
        catch (IOException exception)
        {
            log.debug("IOException caught trying to launch VSTest test discoverers.", exception);
            return;
        }
        catch (InterruptedException e)
        {
            log.debug("InterruptedException caught trying to launch VSTest test discoverers.", e);
            return;
        }

        try (LineNumberReader reader = new LineNumberReader(new InputStreamReader(process.getInputStream())))
        {
            String line;
            String prevLine = null;
            String adapterLine = null;
            String extensionLine;
            String uriLine = null;

            line = reader.readLine();
            while (line != null)
            {
                if (prevLine != null)
                {
                    // Test if line matches Default Executor Uri pattern:
                    // for more information see the example output below.
                    if (line.contains(DEFAULT_EXECUTOR_URI))
                    {
                        // adapterLine now contains the discoverer.
                        adapterLine = prevLine.trim();
                        uriLine = line.substring(line.indexOf(DEFAULT_EXECUTOR_URI) + DEFAULT_EXECUTOR_URI.length()).trim();
                    }
                    else if (line.contains(SUPPORTED_FILE_TYPES))
                    {
                        extensionLine = line.trim();
                        String types = extensionLine.substring(
                                extensionLine.indexOf(SUPPORTED_FILE_TYPES));
                        types = types.trim();

                        // Add the capability for the Test Discoverer.
                        // The value is unimportant in this case, so have the value be the supported file type extensions.
                        // The Bamboo build tasks will be configured to know
                        // which discoverers are required (if they aren't a default discoverer)
                        // i.e. like Python or PowerShell.

                        // Map the URI to a friendly name if we can.
                        String friendlyName = uriToFriendlyNames.get(uriLine);
                        String name = friendlyName == null ? adapterLine : friendlyName;
                        String key = VSTestDiscoverCapabilityTypeModule.VSTEST_CAPABILITY_PREFIX + ".VS " + label + "." + name;
                        if (capabilitySet.getCapability(key) == null)
                        {
                            Capability capability = new CapabilityImpl(key, types);
                            capabilitySet.addCapability(capability);
                        }
                    }
                }
                prevLine = line;
                line = reader.readLine();
            }
        }
        catch (IOException e)
        {
            log.debug("IOException caught trying to parse VSTest Test Discoverers List.", e);
        }
    }

    private void loadFriendlyMap()
    {
        if (uriToFriendlyNames.size() == 0)
        {
            uriToFriendlyNames.put(genericTestUri, "Generic Test Discoverer");
            uriToFriendlyNames.put(msappContainerUri, "MS App Container Discoverer");
            uriToFriendlyNames.put(mswpContainerUri, "MS Windows Phone Container Discoverer");
            uriToFriendlyNames.put(orderedTestsUri, "Ordered Test Discoverer");
            uriToFriendlyNames.put(msTestUri, "MS Test Discoverer");
            uriToFriendlyNames.put(cppWpUri, "MS C++ Windows Phone Test Discoverer");
            uriToFriendlyNames.put(cppUri, "MS C++ Test Discoverer");
            uriToFriendlyNames.put(pesterUri, "Pester PowerShell Test Discoverer");
            uriToFriendlyNames.put(psateUri, "Psate PowerShell Test Discoverer");
            uriToFriendlyNames.put(pythonUri, "Python Test Discoverer");
            uriToFriendlyNames.put(nunitUri, "NUnit Test Discoverer");
            uriToFriendlyNames.put(gtestUri, "Google C++ Test Discoverer");
            uriToFriendlyNames.put(mspecUri, "MSpec Test Discoverer");
            uriToFriendlyNames.put(chutzpahUri, "Chutzpah JavaScript Discoverer");
            uriToFriendlyNames.put(typeScriptUri, "TypeScript Test Discoverer");
            uriToFriendlyNames.put(xunitUri, "xUnit Test Discoverer");
            uriToFriendlyNames.put(nodejsToolsUri, "Node.js Tools Test Discoverer");
        }
    }
}
// Sample output to parse: (from VS 2013)
//
//Microsoft (R) Test Execution Command Line Tool Version 12.0.30723.0
//Copyright (c) Microsoft Corporation.  All rights reserved.
//
//The following Test Discovery Add-Ins are available:
//    Microsoft.VisualStudio.TestPlatform.Extensions.GenericTestAdapter.GenericTestDiscoverer
//        Default Executor Uri: executor://generictestadapter/v1
//        Supported File Types: .generictest
//    Microsoft.VisualStudio.TestPlatform.Extensions.MSAppContainerAdapter.MSAppContainerTestDiscoverer
//        Default Executor Uri: executor://msappcontainertestadapter/v1
//        Supported File Types: .exe, .dll, .appx
//    Microsoft.VisualStudio.TestPlatform.Extensions.MSAppContainerAdapter.MSPhoneTestDiscoverer
//        Default Executor Uri: executor://mswptestadapter/v1
//        Supported File Types: .dll, .exe, .appx, .xap
//    Microsoft.VisualStudio.TestPlatform.Extensions.OrderedTestAdapter.OrderedTestDiscoverer
//        Default Executor Uri: executor://orderedtestadapter/v1
//        Supported File Types: .orderedtest
//    Microsoft.VisualStudio.TestPlatform.Extensions.VSTestIntegration.MSTestDiscoverer
//        Default Executor Uri: executor://mstestadapter/v1
//        Supported File Types: .dll, .exe
//    Microsoft.VisualStudio.TestTools.CppUnitTestFramework.CppUnitTestExtension.TestDiscoverer
//        Default Executor Uri: executor://cppwpunittestexecutor/v1
//        Supported File Types: .dll
//    Microsoft.VisualStudio.TestTools.CppUnitTestFramework.CppUnitTestExtension.TestDiscoverer
//        Default Executor Uri: executor://cppunittestexecutor/v1
//        Supported File Types: .dll
//    PowerShellTools.TestAdapter.Pester.PesterTestDiscoverer
//        Default Executor Uri: executor://pestertestexecutor/v1
//        Supported File Types: .ps1
//    PowerShellTools.TestAdapter.PsateTestDiscoverer
//        Default Executor Uri: executor://psatetestexecutor/v1
//        Supported File Types: .ps1
//    Microsoft.PythonTools.TestAdapter.TestDiscoverer
//        Default Executor Uri: executor://pythontestexecutor/v1
//        Supported File Types: .pyproj
