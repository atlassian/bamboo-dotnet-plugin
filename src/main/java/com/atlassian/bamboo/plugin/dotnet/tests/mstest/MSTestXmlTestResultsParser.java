package com.atlassian.bamboo.plugin.dotnet.tests.mstest;

import com.atlassian.bamboo.plugin.dotnet.tests.TestResultContentHandler;
import com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParser;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultError;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.atlassian.security.xml.libs.SecureDom4jFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import net.jcip.annotations.NotThreadSafe;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ross Rowe
 */
@NotThreadSafe
public class MSTestXmlTestResultsParser extends TestResultContentHandler implements TestResultsParser
{
    private static final Logger log = Logger.getLogger(MSTestXmlTestResultsParser.class);

    private List<TestResults> failedTests;
    private List<TestResults> inconclusiveTests;
    private List<TestResults> passedTests;

    private static final String VS_2006_PREFIX = "a";
    private static final String VS_2010_PREFIX = "b";

    private static final Pattern DURATION_FORMAT = Pattern.compile("(\\d{2}):(\\d{2}):(\\d{2})\\.(\\d{3})\\d+");

    /**
     * @param inputStream
     */
    @Override
    public void parse(InputStream inputStream)
    {
        log.info("Beginning parse of MSTest results file");
        failedTests = new ArrayList<>();
        passedTests = new ArrayList<>();
        inconclusiveTests = new ArrayList<>();
        try
        {
            Map<String, String> uris = ImmutableMap.of(
                    VS_2006_PREFIX, "http://microsoft.com/schemas/VisualStudio/TeamTest/2006",
                    VS_2010_PREFIX, "http://microsoft.com/schemas/VisualStudio/TeamTest/2010");

            DocumentFactory factory = new DocumentFactory();
            factory.setXPathNamespaceURIs(uris);

            // parse or create a document
            final SAXReader reader = SecureDom4jFactory.newSaxReader();
            reader.setDocumentFactory(factory);
            Document document = reader.read(inputStream);
            parse(document);
            log.info("Finished parse of MSTest results file");
        }
        catch (Exception e)
        {
            log.error("Failed to parse xml test results.", e);
        }
    }

    private static String getContent(final Element element)
    {
        if (element.isTextOnly())
        {
            return element.getText();
        }
        final StringBuilder sb = new StringBuilder();
        final Iterator<Element> iterator = element.elementIterator();
        while (iterator.hasNext())
        {
            sb.append(iterator.next().asXML());
        }
        return sb.toString();
    }

    /**
     * @param document
     */
    private void parse(Document document)
    {
        String namespacePrefix = "";
        List<Node> results = document.selectNodes("//Results/*");
        if (results.isEmpty())
        {
            results = getVS2006Results(document);
            namespacePrefix = VS_2006_PREFIX + ":";
        }
        if (results.isEmpty())
        {
            results = getVS2010Results(document);
            namespacePrefix = VS_2010_PREFIX + ":";
        }
        final Map<String, Node> unitTestNodes = cacheUnitTestNodes(document, namespacePrefix);
        for (Node testNode : results)
        {
            String testName = testNode.selectSingleNode("@testName").getStringValue();
            //default className to be the same as testName
            String className = testName;
            //find class for object id
            String objectId = testNode.selectSingleNode("@testId").getStringValue();
            //find className
            Node unitTestNode = unitTestNodes.get(objectId);
            if (unitTestNode != null)
            {
                className = unitTestNode.selectSingleNode(namespacePrefix + "TestMethod/@className").getStringValue();
                //className may include version/culture information, so grab everything before the first ,
                className = className.split(",")[0];
            }
            final Long testDuration = parseDuration(testNode)
                    .map(Duration::toMillis)
                    .orElse(null);

            final Node outcomeAttr = testNode.selectSingleNode("@outcome");
            // MSTest doesn't add the outcome attribute in a TestResult when the test raises an error.
            // This situation occurs when an exception is thrown but none was expected.
            final String result = (outcomeAttr != null) ? outcomeAttr.getStringValue() : "Failed";

            if (result == null)
            {
                continue;
            }
            final TestResults testResult = new TestResults(className, testName, testDuration);
            Node stdOutData = testNode.selectSingleNode(namespacePrefix + "Output/" + namespacePrefix+"StdOut");
            if (stdOutData != null && stdOutData.hasContent())
            {
                Element e = (Element)stdOutData;
                testResult.setSystemOut(getContent(e));
            }
            if (result.equalsIgnoreCase("Passed"))
            {
                testResult.setState(TestState.SUCCESS);
                passedTests.add(testResult);
            }
            else if (result.equalsIgnoreCase("Ignored"))
            {
                testResult.setState(TestState.SKIPPED);
                inconclusiveTests.add(testResult);
            }
            else
            {
                // if we have an error, store it
                final Optional<TestCaseResultError> error = parseError(testNode, namespacePrefix);
                error.ifPresent(testResult::addError);

                if (result.equalsIgnoreCase("Inconclusive"))
                {
                    testResult.setState(TestState.SKIPPED);
                    inconclusiveTests.add(testResult);
                }
                else if (result.equalsIgnoreCase("NotExecuted"))
                {
                    testResult.setState(TestState.SKIPPED);
                    inconclusiveTests.add(testResult);
                }
                else
                {
                    testResult.setState(TestState.FAILED);
                    failedTests.add(testResult);
                }
            }
        }
    }

    @NotNull
    private static Map<String, Node> cacheUnitTestNodes(final Node document, final String namespacePrefix)
    {
        final Map<String, Node> cachedTestInfo = new HashMap<>();
        document.selectNodes("//" + namespacePrefix + "UnitTest")
                .stream()
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .forEach(e -> cachedTestInfo.put(e.attributeValue("id"), e));
        return cachedTestInfo;
    }

    private Optional<Duration> parseDuration(@NotNull Node testNode)
    {
        // duration will be in hh:mm:ss.mmmmmmm format
        final Node durationAttr = testNode.selectSingleNode("@duration");
        if (durationAttr != null)
        {
            return convertDuration(durationAttr.getStringValue());
        }

        return Optional.empty();
    }

    private Optional<TestCaseResultError> parseError(@NotNull Node testNode, @NotNull String namespacePrefix)
    {
        final Node messageNode = getErrorMessageNode(testNode, namespacePrefix);

        if (messageNode != null)
        {
            return Optional.of(new TestCaseResultErrorImpl(messageNode.getStringValue()));
        }

        return Optional.empty();
    }

    @Nullable
    private Node getErrorMessageNode(@NotNull Node testNode, @NotNull String namespacePrefix)
    {
        if (namespacePrefix.equals(VS_2006_PREFIX))
        {
            return testNode.selectSingleNode(namespacePrefix + "Output");
        }
        else
        {
            return testNode.selectSingleNode(namespacePrefix + "Output/" + namespacePrefix + "ErrorInfo/" + namespacePrefix + "Message");
        }
    }

    /**
     * Convert hh:mm:ss.mmmmmmm format to Duration
     * @param input
     * @return
     */
    private Optional<Duration> convertDuration(String input)
    {
        final Matcher matcher = DURATION_FORMAT.matcher(input);

        if (matcher.matches())
        {
            final long hh = Long.parseLong(matcher.group(1));
            final long mm = Long.parseLong(matcher.group(2));
            final long ss = Long.parseLong(matcher.group(3));
            final long ms = Long.parseLong(matcher.group(4));

            return Optional.of(Duration.ofHours(hh)
                                       .plusMinutes(mm)
                                       .plusSeconds(ss)
                                       .plusMillis(ms));
        }
        else
        {
            log.warn(String.format("Cannot parse duration string [%s]", input));
            return Optional.empty();
        }
    }

    private List<Node> getVS2006Results(Document document)
    {
        return document.selectNodes("//" + VS_2006_PREFIX + ":Results/*");
    }

    private List<Node> getVS2010Results(Document document)
    {
        return document.selectNodes("//" + VS_2010_PREFIX + ":Results/*");
    }

    @Override
    public ImmutableList<TestResults> getSuccessfulTests()
    {
        return ImmutableList.copyOf(passedTests);
    }

    @Override
    public ImmutableList<TestResults> getFailedTests()
    {
        return ImmutableList.copyOf(failedTests);
    }

    @Override
    public ImmutableList<TestResults> getInconclusiveTests()
    {
        return ImmutableList.copyOf(inconclusiveTests);
    }
}
