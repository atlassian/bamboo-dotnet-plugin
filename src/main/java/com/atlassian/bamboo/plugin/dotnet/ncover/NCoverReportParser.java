package com.atlassian.bamboo.plugin.dotnet.ncover;


import com.atlassian.security.xml.libs.SecureDom4jFactory;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Handles parsing a NCover XML Report document.
 *
 * @author Ross Rowe
 */
public class NCoverReportParser {
    private static final Logger log = Logger.getLogger(NCoverReportParser.class);

    private long classes;
    private long methods;
    private double lineRate;
    private String csv;

    public Long getLinesExecuted() {
        return linesExecuted;
    }

    public Long getLinesOfCode() {
        return linesOfCode;
    }

    private Long linesOfCode;
    private long assemblies;
    private Long linesExecuted;
    private Long linesNotExecuted;

    /**
     * Runs a SAX parse over the given {@link InputStream}, and invokes the parse() on the Document.
     *
     * @param stream is the <code>InputStream</code> to read from
     */
    public void parse(InputStream stream) throws XPathExpressionException, DocumentException {
        log.debug("About to parse");
        SAXReader reader = SecureDom4jFactory.newSaxReader();
        reader.setEntityResolver(new NCoverEntityResolver());
        Document document = reader.read(stream);
        parse(document);
    }

    /**
     * Populates the instance variables based on the contents of the <code>Document</code>.
     *
     */
    public void parse(Document document) throws XPathExpressionException {

        log.debug("About to parse document");
        linesOfCode = getLongValueFromNode(document, "count(//seqpnt)");
        linesExecuted = getLongValueFromNode(document, "count(//seqpnt[not(@vc=0 or @visitcount=0)])");
        lineRate = linesExecuted.doubleValue() / linesOfCode.doubleValue();
        linesNotExecuted = linesOfCode - linesExecuted;
        classes = getLongValueFromNode(document, "count(//class)");
        methods = getLongValueFromNode(document, "count(//method)");
        assemblies = getLongValueFromNode(document, "count(/coverage/module)");
        //NCover 1.5.x support
        if (classes == 0) {
            Set<String> processedClasses = getProcessedClasses(document);
            classes = processedClasses.size();
        } else {
            csv = processClasses(document);
        }

    }

    private Set<String> getProcessedClasses(Document document) {
        Set<String> classes = new HashSet<>();
        List nodes = document.selectNodes("//method");
        for (Object o : nodes) {
            Node node = (Node) o;
            Node nameAttribute = (Node) node.selectObject("@class");
            classes.add(nameAttribute.getStringValue());

        }
        return classes;
    }

    private String processClasses(Document document) {
        StringBuffer buffer = new StringBuffer();
        List nodes = document.selectNodes("//class");
        for (Object o : nodes) {
            Node node = (Node) o;
            Node nameAttribute = (Node) node.selectObject("@name");
            log.debug("Processing class: " + nameAttribute.getStringValue());
            Double total = (Double) node.selectObject("count(./method/seqpnt)");
            Double visited = (Double) node.selectObject("count(./method/seqpnt[not(@vc=0 or @visitcount=0)])");
            buffer.append(nameAttribute.getStringValue()).append(',');
            buffer.append(visited / total).append(
                    NCoverBuildProcessor.LINE_SEPARATOR);
        }
        return buffer.toString();
    }


    /**
     * Runs a SAX parse over the given {@link File}, and invokes the parse() on the Document.
     *
     * @param file is the <code>File</code> to read from
     * @throws MalformedURLException if a URL could not be made for the given <code>File</code>
     * @throws DocumentException     if an error occurs during parsing
     */
    public void parse(File file) throws IOException, DocumentException, XPathExpressionException {
        try (InputStream is = new FileInputStream(file)) {
            parse(is);
        }
    }

    /**
     * Retrieves the number value for the xpathExpression, and converts the result into a <code>long</code>.
     *
     * @param node
     * @param xpathExpression
     * @return
     */
    private long getLongValueFromNode(Node node, String xpathExpression) {
        log.debug("Running xpath: " + xpathExpression);
        Number number = node.numberValueOf(xpathExpression);

        if (number != null) {
            return number.longValue();
        } else {
            return 0;
        }
    }

    public long getClasses() {
        return classes;
    }

    public long getMethods() {
        return methods;
    }

    public double getLineRate() {
        return lineRate;
    }

    public String getCsv() {
        return csv;
    }

    public long getAssemblies() {
        return assemblies;
    }

    public long getLinesNotExecuted() {
        return linesNotExecuted;
    }
}

