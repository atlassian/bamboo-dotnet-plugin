package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.utils.process.ExternalProcess;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Executes and parses test results from NUnit test runs executed via the command line.
 */
public class NUnitRunnerTaskType implements TaskType
{
    public static final String NUNIT_SHORT_KEY = "nunit";
    public static final String NUNIT3_SHORT_KEY = "nunit3";
    public static final String NUNIT_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + "." + NUNIT_SHORT_KEY;
    public static final String NUNIT3_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + "." + NUNIT3_SHORT_KEY;

    public static final String CAPABILITY_KEY = "label";
    public static final String TEST_FILES = "nunitTestFiles";
    public static final String RESULTS_FILE = "nunitResultsFile";
    public static final String TESTS_TO_RUN = "run"; // eg. /run:NUnit.Tests.AssertionTests,NUnit.Tests.ConstraintTests
    public static final String INCLUDED_TEST_CATEGORIES = "include"; // eg. /include:TheseTests,AndTheseTests
    public static final String EXCLUDED_TEST_CATEGORIES = "exclude"; // eg. /exclude:BrokenTests
    public static final String COMMAND_OPTIONS = "commandLineOptions";
    public static final String ENVIRONMENT = "environmentVariables";
    static final String WHERE_PARAMETER = "--where";
    static final String INCLUDE_PARAMETER = "-include=";
    static final String EXCLUDE_PARAMETER = "-exclude=";

    private final ProcessService processService;
    private final TestCollationService testCollationService;
    private final CapabilityContext capabilityContext;
    private final EnvironmentVariableAccessor environmentVariableAccessor;

    public NUnitRunnerTaskType(final ProcessService processService, final TestCollationService testCollationService, final CapabilityContext capabilityContext, final EnvironmentVariableAccessor environmentVariableAccessor)
    {
        this.processService = processService;
        this.testCollationService = testCollationService;
        this.capabilityContext = capabilityContext;
        this.environmentVariableAccessor = environmentVariableAccessor;
    }

    @Override
    @NotNull
    public TaskResult execute(@NotNull TaskContext taskContext)
    {
        final ErrorMemorisingInterceptor errorLines = ErrorMemorisingInterceptor.newInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);
        try
        {
            final ConfigurationMap configuration = taskContext.getConfigurationMap();
            final String resultsFile = configuration.get(RESULTS_FILE);
            Preconditions.checkNotNull(resultsFile, "Results File Name");

            final List<String> command = getCommand(configuration, resultsFile);

            final String environment = configuration.get(ENVIRONMENT);
            final Map<String, String> env = environmentVariableAccessor.splitEnvironmentAssignments(environment);
            final ExternalProcess externalProcess = processService.executeExternalProcess(taskContext,
                    new ExternalProcessBuilder()
                            .env(env)
                            .command(command)
                            .workingDirectory(taskContext.getWorkingDirectory()));

            testCollationService.collateTestResults(taskContext, resultsFile, new NUnitTestReportCollector());

            return TaskResultBuilder.newBuilder(taskContext)
                    .checkTestFailures()
                    .checkReturnCode(externalProcess)
                    .build();
        }
        finally
        {
            taskContext.getBuildContext().getBuildResult().addBuildErrors(errorLines.getErrorStringList());
        }
    }

    @VisibleForTesting
    @NotNull
    List<String> getCommand(ConfigurationMap configuration, String resultsFile)
    {
        final String testFiles = configuration.get(TEST_FILES);
        Preconditions.checkNotNull(testFiles, "One or more test files must be specified");

        final String capabilityKeyOrLabel = configuration.get(CAPABILITY_KEY);
        Preconditions.checkNotNull(capabilityKeyOrLabel, "You must select an NUnit Runner before this task can execute.");

        final String capabilityKey = getCapabilityKey(capabilityKeyOrLabel);
        final Capability capability = capabilityContext.getCapabilitySet().getCapability(capabilityKey);
        Preconditions.checkNotNull(capability, "Capability");

        final boolean isNunit3 = capabilityKey.startsWith(NUNIT3_CAPABILITY_PREFIX);

        final File nunitConsoleExe = new File(capability.getValue());
        Preconditions.checkArgument(nunitConsoleExe.exists(), "NUnit Console Executable " + nunitConsoleExe + " not found");
        // Note: Under the Windows operating system, options may be prefixed by either a forward slash or a hyphen.
        // Under Linux, a hyphen must be used. Options that take values may use an equal sign, a colon or a space
        // to separate the option from its value.
        final List<String> command = new ArrayList<>();

        command.add(nunitConsoleExe.getAbsolutePath());
        final String testsToRun = configuration.get(TESTS_TO_RUN);
        if (StringUtils.isNotBlank(testsToRun))
        {
            command.add("-run=" + testsToRun);
        }

        command.add(testFiles);
        command.add(getResultFileParameter(resultsFile, isNunit3));
        command.addAll(getIncludeExcludeCategories(configuration, isNunit3));
        command.addAll(CommandlineStringUtils.tokeniseCommandline(configuration.get(COMMAND_OPTIONS)));
        return command;
    }

    private List<String> getIncludeExcludeCategories(ConfigurationMap configuration, boolean isNunit3)
    {
        final String includedCategories = configuration.get(INCLUDED_TEST_CATEGORIES);
        final String excludedCategories = configuration.get(EXCLUDED_TEST_CATEGORIES);

        if (isNunit3)
        {
            return getWhereParameter(includedCategories, excludedCategories);
        }
        else
        {
            final List<String> result = new ArrayList<>();
            if (StringUtils.isNotBlank(includedCategories))
            {
                result.add(INCLUDE_PARAMETER + includedCategories);
            }

            if (StringUtils.isNotBlank(excludedCategories))
            {
                result.add(EXCLUDE_PARAMETER + excludedCategories);
            }
            return result;
        }
    }

    private String getResultFileParameter(String resultsFile, boolean isNunit3)
    {
        if (isNunit3)
        {
            return "--result=" + resultsFile + ";format=nunit2";
        }
        else
        {
            return "-xml=" + resultsFile;
        }
    }

    // NUnit Console 3 does not support -include and -exclude params
    // Add conditional logic to use new -where if Nunit3
    private List<String> getWhereParameter(String includedCategories, String excludedCategories)
    {
        final StringBuilder whereClause = new StringBuilder();
        if (StringUtils.isNotBlank(includedCategories))
        {
            whereClause.append("cat==").append(includedCategories);
        }
        if (StringUtils.isNotBlank(excludedCategories))
        {
            if (whereClause.length() > 0)
            {
                whereClause.append(" && ");
            }
            whereClause.append("cat!=").append(excludedCategories);
        }

        final List<String> result = new ArrayList<>();
        if (whereClause.length() > 0)
        {
            result.add(WHERE_PARAMETER);
            result.add("\"" + whereClause.toString() + "\"");
        }
        return result;
    }

    @NotNull
    static String getCapabilityKey(String capabilityKeyOrLabel) {
        return capabilityKeyOrLabel.startsWith(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX)
                ? capabilityKeyOrLabel
                : NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX + "." + capabilityKeyOrLabel;
    }

}
