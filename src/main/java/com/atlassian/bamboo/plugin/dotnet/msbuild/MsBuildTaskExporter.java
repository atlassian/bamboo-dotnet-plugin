package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.MsBuildTask;
import com.atlassian.bamboo.specs.model.task.MsBuildTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MsBuildTaskExporter implements TaskDefinitionExporter {
    private static final ValidationContext CONTEXT = ValidationContext.of("MsBuild task");

    @Override
    @NotNull
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull final TaskProperties taskProperties) {
        final MsBuildTaskProperties msBuildTaskProperties = Narrow.downTo(taskProperties, MsBuildTaskProperties.class);
        if (msBuildTaskProperties == null) {
            throw new IllegalStateException("Unsupported import task of type: " + taskProperties.getClass().getName());
        }

        final Map<String, String> cfg = new HashMap<>();
        cfg.put(MsBuildConfig.LABEL, msBuildTaskProperties.getExecutable());
        cfg.put(MsBuildConfig.SOLUTION, msBuildTaskProperties.getProjectFile());
        cfg.put(MsBuildConfig.OPTIONS, msBuildTaskProperties.getOptions());
        cfg.put(MsBuildConfig.ENVIRONMENT, msBuildTaskProperties.getEnvironmentVariables());
        cfg.put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, msBuildTaskProperties.getWorkingSubdirectory());
        return cfg;
    }

    @NotNull
    @Override
    public MsBuildTask toSpecsEntity(final TaskDefinition taskDefinition) {
        final Map<String, String> configuration = taskDefinition.getConfiguration();

        final MsBuildTask task = new MsBuildTask()
                .executable(configuration.get(MsBuildConfig.LABEL))
                .projectFile(configuration.get(MsBuildConfig.SOLUTION))
                .options(configuration.get(MsBuildConfig.OPTIONS))
                .environmentVariables(configuration.get(MsBuildConfig.ENVIRONMENT))
                .workingSubdirectory(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY));

        return task;
    }

    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        final List<ValidationProblem> result = new ArrayList<>();
        final MsBuildTaskProperties properties = Narrow.downTo(taskProperties, MsBuildTaskProperties.class);
        if (properties == null) {
            return result;
        }

        final String executableLabel = properties.getExecutable();
        if (StringUtils.isEmpty(executableLabel)) {
            result.add(new ValidationProblem(CONTEXT, "Executable can't be empty"));
        }
        return result;

    }
}
