package com.atlassian.bamboo.plugin.dotnet.tests.vstest;

import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContextImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyString;

public class VSTestRunnerTaskTypeTest {

    @Test
    public void getCommandDoesntAddEmptyStringWhenAdditionalArgsEmpty() {
        final CapabilityContextImpl capabilityContext = getCapabilityContext();

        final ConfigurationMapImpl configurationMap = new ConfigurationMapImpl();
        configurationMap.put(VSTestRunnerTaskType.LABEL, "label");
        configurationMap.put(VSTestRunnerTaskType.SETTINGS_FILE, "filename");
        configurationMap.put(VSTestRunnerTaskType.TESTFILE, "testfilename");
        configurationMap.put(VSTestRunnerTaskType.ADDITIONAL_ARGS, "");

        final VSTestRunnerTaskType vsTestRunnerTaskType =
                new VSTestRunnerTaskType(null, null, capabilityContext, null);

        final List<String> command =
                vsTestRunnerTaskType.getCommand(Mockito.mock(TaskContext.class), configurationMap);

        final String lastEntry = command.get(command.size() - 1);
        assertNotEquals("", lastEntry);
    }

    @Test
    public void getCommandAddsArgsWhenAdditionalArgsNotEmpty() {
        final CapabilityContextImpl capabilityContext = getCapabilityContext();

        final ConfigurationMapImpl configurationMap = new ConfigurationMapImpl();
        configurationMap.put(VSTestRunnerTaskType.LABEL, "label");
        configurationMap.put(VSTestRunnerTaskType.SETTINGS_FILE, "filename");
        configurationMap.put(VSTestRunnerTaskType.TESTFILE, "testfilename");
        configurationMap.put(VSTestRunnerTaskType.ADDITIONAL_ARGS, "additional-args");

        final VSTestRunnerTaskType vsTestRunnerTaskType =
                new VSTestRunnerTaskType(null, null, capabilityContext, null);

        final List<String> command =
                vsTestRunnerTaskType.getCommand(Mockito.mock(TaskContext.class), configurationMap);

        final String lastEntry = command.get(command.size() - 1);
        assertEquals("additional-args", lastEntry);
    }

    @NotNull
    private CapabilityContextImpl getCapabilityContext() {
        final CapabilityContextImpl capabilityContext = Mockito.mock(CapabilityContextImpl.class);
        final CapabilitySet capabilitySet = Mockito.mock(CapabilitySet.class);
        final Capability capability = new CapabilityImpl("key", "src/test/resources/mockVsTest.exe");

        Mockito.when(capabilityContext.getCapabilitySet()).thenReturn(capabilitySet);
        Mockito.when(capabilitySet.getCapability(anyString())).thenReturn(capability);
        return capabilityContext;
    }
}