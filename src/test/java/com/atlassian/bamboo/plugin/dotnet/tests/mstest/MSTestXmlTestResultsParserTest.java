package com.atlassian.bamboo.plugin.dotnet.tests.mstest;

import com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParser;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.atlassian.bamboo.utils.BambooTestUtils;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParserMatcherBuilder.testResultsParser;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * @author Ross Rowe
 */
@RunWith(JUnitParamsRunner.class)
public class MSTestXmlTestResultsParserTest
{
    private static final String TEST_WITH_STDOUT = "TestResult.trx";

    private final TestResultsParser parser = new MSTestXmlTestResultsParser();

    @Parameters({
            "sample.trx, 3, 1, 0",
            "TestResult.trx, 3, 0, 0",
            // entries with same name
            "build_WIN-BLD1+2009-08-27+11_43_16.trx, 24, 0, 0",
            // vs2010 format
            "vs2010.trx, 368, 46, 4",
            // vsKalamon format
            "kalamon.trx, 1, 1, 0",
            // nant41
            "BigLebowski.trx, 370, 46, 2",
            // BSP-5353 regression
            "BSP-5353.trx, 0, 19, 0",
            // error tests
            "VS2010_ERROR.trx, 1, 4, 0",
            // not executed tests
            "vs2010_notexecuted.trx, 354, 46, 18"
    })
    @Test
    public void testParser(@NotNull String inputFile, int successfulTestCount, int failedTestCount, int inconclusiveTestCount)
    {
        parser.parse(BambooTestUtils.getInputStream(inputFile, this));

        assertThat(parser,
                   testResultsParser()
                           .successfulTests(successfulTestCount)
                           .failedTests(failedTestCount)
                           .inconclusiveTests(inconclusiveTestCount)
                           .build());
        if (inputFile.equals(TEST_WITH_STDOUT))
        {
            assertThat(parser.getSuccessfulTests().get(2).getSystemOut(), equalTo("helle world"));
        }
    }

    @Test
    public void regressionBSP3531()
    {
        parser.parse(BambooTestUtils.getInputStream("BSP-3531.trx", this));

        assertThat(parser,
                   testResultsParser()
                           .successfulTests(230)
                           .failedTests(0)
                           .inconclusiveTests(0)
                           .build());

        final TestResults testResults_0 = parser.getSuccessfulTests().get(0);

        assertEquals("Ident.ObjectModel.Tests.DataTypes.ApprovalFixture", testResults_0.getClassName());
        assertEquals("Approval test data factory validation", testResults_0.getMethodName());
        assertEquals("Approval_TestDataFactoryValidation", testResults_0.getActualMethodName());
        assertEquals(TestState.SUCCESS, testResults_0.getState());
        assertThat(testResults_0.getDurationMs(), equalTo(11260L));
    }
}
