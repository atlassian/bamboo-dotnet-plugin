package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.utils.BambooTestUtils;
import com.atlassian.bamboo.utils.FileVisitor;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.bamboo.testutils.matchers.EntityMatchers.testCollectionResult;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * This test needs to be run with JDK8 (in order to access public property of private inner class implementing a public interface)
 */
@RunWith(JUnitParamsRunner.class)
public class NUnitTestReportCollectorTest
{
    private NUnitTestReportCollector testReportCollector;

    @Before
    public void setUp()
    {
        testReportCollector = new NUnitTestReportCollector();
    }

    @Test
    public void testBAM_17892() throws Exception
    {
        final TestCollectionResult result = collect("BAM-17892.dots_in_functions_arguments_break_classes_names_parsing.xml");

        assertThat(result,
                   testCollectionResult()
                           .successfulTestResults(1)
                           .build());

        final TestResults testResult = result.getSuccessfulTestResults().get(0);

        assertThat(testResult.getClassName(), is("unittests.Mapping.ValueCasterTest"));
        assertThat(testResult.getActualMethodName(), is("ShouldConvertTfsValueToIntermediateValue(synchronizer.Domain.FieldMapping,3.14159d,IntermediateFieldValue([3.14159], 3.14159))"));
    }

    @Test
    public void testBAM_19810() throws Exception {
        final TestCollectionResult result = collect("BAM-19810.test_ends_with_dot.xml");

        assertThat(result,
                testCollectionResult()
                        .failedTestResults(4)
                        .build());

        assertThat(result.getFailedTestResults().get(0).getClassName(), is("Test Suite with dot on the end."));
        assertThat(result.getFailedTestResults().get(0).getActualMethodName(), is("Im not ended with dot"));

        assertThat(result.getFailedTestResults().get(1).getClassName(), is("Test Suite with dot on the end."));
        assertThat(result.getFailedTestResults().get(1).getActualMethodName(), is("Im ended with dot."));

        assertThat(result.getFailedTestResults().get(2).getClassName(), is("Test Suite with dot on the end."));
        assertThat(result.getFailedTestResults().get(2).getActualMethodName(), is("Im ended with dot."));

        assertThat(result.getFailedTestResults().get(3).getClassName(), is("Test Suite with dot on the end."));
        assertThat(result.getFailedTestResults().get(3).getActualMethodName(), is("I have dot.In the middle with dot at the end."));
    }

    @Test
    public void testMultiThreadedCollation() throws Exception
    {
        final AtomicInteger numberOfFilesFound = new AtomicInteger(0);
        final Set<TestResults> failedTestResults = Collections.synchronizedSet(new HashSet<>());
        final Set<TestResults> successfulTestResults = Collections.synchronizedSet(new HashSet<>());

        File rootDirectory = BambooTestUtils.getFileFromResourceDirectory(this, "empty_file.xml").getParentFile();

        final ExecutorService executorService = Executors.newFixedThreadPool(8);

        try
        {
            FileVisitor fileVisitor = new FileVisitor(rootDirectory)
            {
                @Override
                public void visitFile(final File file)
                {
                    final String fileName = file.getName();

                    if (FilenameUtils.isExtension(fileName, testReportCollector.getSupportedFileExtensions()))
                    {
                        executorService.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                try
                                {
                                    numberOfFilesFound.incrementAndGet();
                                    TestCollectionResult result = testReportCollector.collect(file);
                                    failedTestResults.addAll(result.getFailedTestResults());
                                    successfulTestResults.addAll(result.getSuccessfulTestResults());
                                }
                                catch (Exception e)
                                {
                                    fail();
                                }
                            }
                        });
                    }
                }

            };

            fileVisitor.visitFilesThatMatch("BSP*.xml");
        }
        finally
        {
            executorService.shutdown();
            executorService.awaitTermination(2, TimeUnit.MINUTES);
        }

        assertEquals(20, successfulTestResults.size());
        assertEquals(0, failedTestResults.size());
    }

    @Parameters({
            "BSP-4922.Enroll.ThickFacade.Tests.nunit-results.xml, 13, 0, 0",
            "BSP-4922.IDGrid.Tests.nunit-results.xml, 7, 0, 0",
            "BSP-4960.testresult_with_no_results.xml, 0, 0, 0",
            "nunit_org_2_5_sample.xml, 18, 2, 8",
            "BAM-15693.FiftySevenInconclusiveTestResult.xml, 2497, 0, 99",
            "BAM-17096.xml, 14, 0, 0",
            "BAM-18415.xml, 1, 1, 0",
    })
    @Test
    public void testCollector(String filename, int expectedSuccessful, int expectedFailed, int expectedSkipped) throws Exception
    {
        final TestCollectionResult result = collect(filename);

        assertThat(result,
                   testCollectionResult()
                           .successfulTestResults(expectedSuccessful)
                           .failedTestResults(expectedFailed)
                           .skippedTestResults(expectedSkipped)
                           .build());
    }

    private TestCollectionResult collect(@NotNull String filename) throws Exception
    {
        return testReportCollector.collect(BambooTestUtils.getFileFromResourceDirectory(this, filename));
    }
}
