package com.atlassian.bamboo.plugin.dotnet.nant;

import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskContextImpl;
import com.atlassian.bamboo.task.TaskExecutionContext;
import com.atlassian.bamboo.task.TaskRootDirectorySelector;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.atlassian.bamboo.vcs.module.VcsRepositoryManager;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NantTaskTypeTest
{
    @Rule public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private CapabilityContext mockCapabilityContext;
    @Mock private EnvironmentVariableAccessor mockEnvironmentVariableAccessor;
    @Mock private ProcessService mockProcessService;
    @Mock private ReadOnlyCapabilitySet mockCapabilitySet;
    @Mock private VcsRepositoryManager vcsRepositoryManager;

    @InjectMocks private NantTaskType taskType;

    @Before
    public void setUp()
    {
        when(mockCapabilityContext.getCapabilitySet()).thenReturn(mockCapabilitySet);
    }

    @Test
    public void testGetExecutableFile()
    {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        assertGetExecutableFile(
                StringUtils.join(
                        Arrays.asList("d:\\bamboo-agent4-home\\xml-data\\build-dir\\BUILDDEPLOY-NANTTEMPLATE-JOB1", ".\\tools\\nant", "bin", NantTaskType.EXECUTABLE_NAME),
                        File.separator
                ),
                "d:\\bamboo-agent4-home\\xml-data\\build-dir\\BUILDDEPLOY-NANTTEMPLATE-JOB1", ".\\tools\\nant"
        );
        assertGetExecutableFile(
                StringUtils.join(
                        Arrays.asList("\\\\build-basis-w2k3-01\\e$\\bamboo-agent4-home\\xml-data\\build-dir\\BUILDDEPLOY-NANTTEMPLATE-JOB1", ".\\tools\\nant", "bin", NantTaskType.EXECUTABLE_NAME),
                        File.separator
                ),
                "\\\\build-basis-w2k3-01\\e$\\bamboo-agent4-home\\xml-data\\build-dir\\BUILDDEPLOY-NANTTEMPLATE-JOB1", ".\\tools\\nant"
        );
        assertGetExecutableFile(
                StringUtils.join(
                        Arrays.asList("d:\\tools\\nant", "bin", NantTaskType.EXECUTABLE_NAME),
                        File.separator
                ),
                "d:\\bamboo-agent4-home\\xml-data\\build-dir\\BUILDDEPLOY-NANTTEMPLATE-JOB1", "d:\\tools\\nant"
        );
        assertGetExecutableFile(
                StringUtils.join(
                        Arrays.asList("d:\\tools\\nant", "bin", NantTaskType.EXECUTABLE_NAME),
                        File.separator
                ),
                "\\\\build-basis-w2k3-01\\e$\\bamboo-agent4-home\\xml-data\\build-dir\\BUILDDEPLOY-NANTTEMPLATE-JOB1", "d:\\tools\\nant"
        );
    }

    private void assertGetExecutableFile(@NotNull String expectedPath, @NotNull String rootDirectory, @NotNull String grailsExecutablePath)
    {
        final String nantLabel = "Nant";
        final String capabilityKey = NantTaskType.NANT_CAPABILITY_PREFIX + "." + nantLabel;

        when(mockCapabilitySet.getCapability(capabilityKey)).thenReturn(new CapabilityImpl(capabilityKey, grailsExecutablePath));

        Map<String, String> substitutedConfiguration = ImmutableMap.of(
                NantTaskType.LABEL, nantLabel
        );
        final TaskExecutionContext taskExecutionContext = mock(TaskExecutionContext.class);
        final CommonContext commonContext = mock(CommonContext.class);
        when(taskExecutionContext.getCommonContext()).thenReturn(commonContext);

        final RuntimeTaskDefinition taskDefinition = mock(RuntimeTaskDefinition.class);
        when(taskDefinition.getRootDirectorySelector()).thenReturn(TaskRootDirectorySelector.DEFAULT);

        TaskContext taskContext = new TaskContextImpl(substitutedConfiguration, taskExecutionContext, taskDefinition, vcsRepositoryManager);
        when(taskContext.getRootDirectory()).thenReturn(new File(rootDirectory));
        //
        File actualFile = taskType.getExecutableFile(taskContext);

        assertEquals(expectedPath, actualFile.getPath());
    }
    //this.runtimeTaskContext = taskExecutionContext.getCommonContext().getRuntimeTaskContext().getRuntimeContextForTask(taskDefinition);
}
