package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.utils.BambooTestUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Ross Rowe
 *
 */
public class NUnitResultsParserTest
{
	private static final String TEST_RESULT_FILE = "com/atlassian/bamboo/plugin/dotnet/tests/TestResult.xml";
	private static final String TEST_FAILURE_RESULT_FILE = "com/atlassian/bamboo/plugin/dotnet/tests/TestResultFailure.xml";
	private static final String TEST_RESULT_CATEGORIES_FILE = "com/atlassian/bamboo/plugin/dotnet/tests/TestResultCategories.xml";
	private static final String XUNIT_FILE = "com/atlassian/bamboo/plugin/dotnet/tests/ark.nunit.output.xml";

    private NUnitXmlTestResultsParser parser;

    @Before
    public void setUp()
	{
        parser = new NUnitXmlTestResultsParser();
    }

    @Test
	public void testParseSampleFile()
	{
		parser.parse(BambooTestUtils.getInputStream(TEST_RESULT_FILE));
		assertFalse(parser.getSuccessfulTests().isEmpty());
		assertTrue(parser.getFailedTests().isEmpty());
	}

    @Test
    public void testParseXUnitFile()
	{
		parser.parse(BambooTestUtils.getInputStream(XUNIT_FILE));
		assertFalse(parser.getSuccessfulTests().isEmpty());
		assertTrue(parser.getFailedTests().isEmpty());
	}
	
	@Test
	public void testParseFileWithFailures()
	{
		parser.parse(BambooTestUtils.getInputStream(TEST_FAILURE_RESULT_FILE));
		assertFalse(parser.getSuccessfulTests().isEmpty());
		assertFalse(parser.getFailedTests().isEmpty());
	}

    @Test
    public void testParseFileWithCategories()
	{
		parser.parse(BambooTestUtils.getInputStream(TEST_RESULT_CATEGORIES_FILE));
		assertFalse(parser.getSuccessfulTests().isEmpty());
		assertTrue(parser.getFailedTests().isEmpty());
	}
}
