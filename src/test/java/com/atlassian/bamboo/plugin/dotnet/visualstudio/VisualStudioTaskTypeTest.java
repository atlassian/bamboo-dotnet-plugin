package com.atlassian.bamboo.plugin.dotnet.visualstudio;

import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.LogInterceptorStack;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.testutils.junit.BambooJUnit;
import com.atlassian.bamboo.testutils.junit.rule.SpringContextRule;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.atlassian.utils.process.ExternalProcess;
import com.atlassian.utils.process.ProcessHandler;
import com.google.common.base.Joiner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VisualStudioTaskTypeTest
{
    @Rule public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule public final SpringContextRule springContextRule = BambooJUnit.springContextRule();
    @Rule public final TemporaryFolder tmpFolder = new TemporaryFolder();

    @Mock private BuildDirectoryManager buildDirectoryManager;
    @Mock private ProcessService processService;
    @Mock private EnvironmentVariableAccessor environmentVariableAccessor;
    @Mock private CapabilityContext capabilityContext;

    @Mock private BuildContext buildContext;
    @Mock private BuildLogger buildLogger;
    @Mock private CurrentBuildResult currentBuildResult;
    @Mock private ExternalProcess externalProcess;
    @Mock private ProcessHandler processHandler;
    @Mock private ReadOnlyCapabilitySet readOnlyCapabilitySet;
    @Mock private TaskContext taskContext;

    @Captor private ArgumentCaptor<ExternalProcessBuilder> externalProcessBuilderCaptor;

    @InjectMocks private VisualStudioTaskType task;

    private File applicationHomeDirectory;
    private File taskWorkingDirectory;

    private final static Joiner DOT_JOINER = Joiner.on(".");
    private final static Joiner FILE_JOINER = Joiner.on(File.separator);

    @Before
    public void setUp() throws Exception
    {
        applicationHomeDirectory = tmpFolder.newFolder("application-home");
        taskWorkingDirectory = tmpFolder.newFolder("task-working-directory");

        springContextRule.mockSpringComponent("buildDirectoryManager", buildDirectoryManager);
        when(buildDirectoryManager.getApplicationHome()).thenReturn(applicationHomeDirectory);

        when(buildContext.getBuildResult()).thenReturn(currentBuildResult);
        //when(buildContext.getCurrentResult()).thenReturn(currentBuildResult);
        when(buildLogger.getInterceptorStack()).thenReturn(mock(LogInterceptorStack.class));

        when(capabilityContext.getCapabilitySet()).thenReturn(readOnlyCapabilitySet);

        when(processHandler.getExitCode()).thenReturn(0);
        when(externalProcess.getHandler()).thenReturn(processHandler);
        when(processService.executeExternalProcess(any(CommonTaskContext.class), any(ExternalProcessBuilder.class)))
                .thenReturn(externalProcess);

        when(taskContext.getBuildContext()).thenReturn(buildContext);
        when(taskContext.getBuildLogger()).thenReturn(buildLogger);
        when(taskContext.getWorkingDirectory()).thenReturn(taskWorkingDirectory);
    }

    @Test
    public void testExecute() throws Exception
    {
        final File visualStudioHome = tmpFolder.newFolder("VisualStudioFakeHome", "Common7", "IDE");
        final String label = "label";
        final String environment = "x86";
        final String solution = "solution.sln";
        final String options = "/build \"Release Unicode|x86\" \"Release|x86\"";

        final String vsCapabilityKey = DOT_JOINER.join(VisualStudioTaskType.VS_CAPABILITY_PREFIX, label);
        final Capability vsCapability = mock(Capability.class);
        when(vsCapability.getValue()).thenReturn(visualStudioHome.getAbsolutePath());
        when(readOnlyCapabilitySet.getCapability(vsCapabilityKey)).thenReturn(vsCapability);

        final ConfigurationMap configurationMap = new ConfigurationMapImpl(new HashMap<>());
        configurationMap.put(VisualStudioTaskType.LABEL, label);
        configurationMap.put(VisualStudioTaskType.SOLUTION, solution);
        configurationMap.put(VisualStudioTaskType.OPTIONS, options);
        configurationMap.put(VisualStudioTaskType.VS_ENVIRONMENT, environment);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        // execute
        task.execute(taskContext);

        // verify
        verify(processService).executeExternalProcess(eq(taskContext), externalProcessBuilderCaptor.capture());

        assertThat(externalProcessBuilderCaptor.getValue().getCommand(), contains(
                FILE_JOINER.join(applicationHomeDirectory.getAbsolutePath(), "DotNetSupport", "devenvrunner.bat"),
                taskWorkingDirectory.getAbsolutePath(),
                visualStudioHome.getParentFile().getParentFile().getAbsolutePath(),
                environment,
                solution,
                "/build", "\"Release Unicode|x86\"", "\"Release|x86\""
        ));
    }
}
