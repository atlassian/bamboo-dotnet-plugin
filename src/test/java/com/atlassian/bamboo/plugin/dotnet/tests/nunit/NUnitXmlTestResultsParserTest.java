package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class NUnitXmlTestResultsParserTest
{
    @Test
    public void parsesTestNamesCorrectly()
    {
        final String testName = "TestSetInvalidExpirationDate";

        assertThat(NUnitXmlTestResultsParser.getTestName(testName), equalTo(testName));
        assertThat(NUnitXmlTestResultsParser.getTestName("something.something." + testName), equalTo(testName));

        final String params = "(pProjectModel: MockProjectModel { AttendeeAccessLabel = \"\", AttendeeName = \"\", CreatedDate = 2016-06-20T18:23:48.2887930Z, ExpirationDate = null, Expires = False, ... })";
        final String fqTestNameWithParams = "Bluebeam.Core.Application.MockTests.Mac.SessionSettingsViewmodelTests." + testName + params;

        assertThat(NUnitXmlTestResultsParser.getTestName(fqTestNameWithParams), Matchers.startsWith(testName));

        final String testNameWithParams = testName + params;

        assertThat(NUnitXmlTestResultsParser.getTestName(testNameWithParams), Matchers.startsWith(testName));
    }
}
