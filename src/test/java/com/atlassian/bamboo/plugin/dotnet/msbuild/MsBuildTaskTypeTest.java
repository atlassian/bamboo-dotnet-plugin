package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ProcessContextFactory;
import com.atlassian.bamboo.process.ProcessContextFactoryImpl;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.process.ProcessServiceImpl;
import com.atlassian.bamboo.spring.AutowiringSupport;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.testutils.build.logger.BuildLoggerManagerForTesting;
import com.atlassian.bamboo.testutils.junit.BambooJUnit;
import com.atlassian.bamboo.testutils.junit.rule.SpringContextRule;
import com.atlassian.bamboo.utils.BambooTestUtils;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetImpl;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.utils.process.ExternalProcess;
import com.atlassian.utils.process.ProcessHandler;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assume.assumeThat;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class)
public class MsBuildTaskTypeTest {
    //private final static Joiner DOT_JOINER = Joiner.on(".");

    private static final String PLAN_NAME = "Foo Bar";
    private static final PlanResultKey PLAN_RESULT_KEY = PlanKeys.getPlanResultKey("FOO-BAR", 1);

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final SpringContextRule springContextRule = BambooJUnit.springContextRule();
    @Rule
    public final TemporaryFolder tmpFolder = new TemporaryFolder();

    @Mock
    private BuildDirectoryManager buildDirectoryManager;
    @Mock
    private EnvironmentVariableAccessor environmentVariableAccessor;
    @Mock
    private CapabilityContext capabilityContext;
    @Mock
    private CommonContext buildContext;
    @Mock
    private CurrentBuildResult currentBuildResult;
    @Mock
    private ExternalProcess externalProcess;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private ProcessHandler processHandler;
    @Mock
    private TaskContext taskContext;
    @Mock
    private VariableContext variableContext;

    private BuildLoggerManagerForTesting buildLoggerManager;
    private CapabilitySet capabilitySet;
    private ProcessService processService;
    private File msBuildProjectFile;
    private ProcessContextFactory processContextFactory;

    @Before
    public void setUp() throws Exception {
        final File applicationHomeDirectory = tmpFolder.newFolder("application-home");
        buildLoggerManager = new BuildLoggerManagerForTesting();
        final BuildLogger buildLogger = buildLoggerManager.getLogger(PLAN_RESULT_KEY);
        final File taskWorkingDirectory = tmpFolder.newFolder("task-working-directory");

        springContextRule.mockSpringComponent("buildDirectoryManager", buildDirectoryManager);
        when(buildDirectoryManager.getApplicationHome()).thenReturn(applicationHomeDirectory);

        when(variableContext.getEffectiveVariables()).thenReturn(Collections.emptyMap());
        when(variableContext.getPasswordVariables()).thenReturn(Collections.emptyList());

        when(buildContext.getCurrentResult()).thenReturn(currentBuildResult);
        when(buildContext.getDisplayName()).thenReturn(PLAN_NAME);
        when(buildContext.getResultKey()).thenReturn(PLAN_RESULT_KEY);
        when(buildContext.getVariableContext()).thenReturn(variableContext);

        // capabilities
        {
            capabilitySet = new CapabilitySetImpl();
            new MsBuildCapabilityDefaultsHelper().addDefaultCapabilities(capabilitySet);

            when(capabilityContext.getCapabilitySet()).thenReturn(capabilitySet);
        }

        when(processHandler.getExitCode()).thenReturn(0);
        when(externalProcess.getHandler()).thenReturn(processHandler);

        when(taskContext.getCommonContext()).thenReturn(buildContext);
        when(taskContext.getBuildLogger()).thenReturn(buildLogger);
        when(taskContext.getWorkingDirectory()).thenReturn(taskWorkingDirectory);

        processContextFactory = new ProcessContextFactoryImpl();
        AutowiringSupport.autowire(processContextFactory, environmentVariableAccessor, pluginAccessor, buildLoggerManager);
        processService = new ProcessServiceImpl();
        AutowiringSupport.autowire(processService, environmentVariableAccessor, processContextFactory);

        msBuildProjectFile = new File(taskWorkingDirectory, "msbuild.proj");
        FileUtils.copyFile(BambooTestUtils.getFileFromResourceDirectory(this, "msbuild.proj"), msBuildProjectFile);
    }

    /**
     * regression test for BDEV-5798
     */
    @Test
    public void testBDEV5798() throws Exception {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        final MsBuildTaskType task = new MsBuildTaskType(processService, environmentVariableAccessor, capabilityContext);

        final String msBuildCapabilityLabel = pickNativeMsBuilderCapability(capabilitySet);
        final String options = "/nologo /t:BDEV-5798 /p:BuildVersion=\"1.2.3.4\" /p:ProductsToRelease=\"Jira;Bamboo;Fisheye\" /clp:ShowCommandLine /v:diag";
        final String solution = msBuildProjectFile.getAbsolutePath();

        when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(ImmutableMap.of(
                MsBuildConfig.LABEL, msBuildCapabilityLabel,
                MsBuildConfig.SOLUTION, solution,
                MsBuildConfig.OPTIONS, options
        )));

        // execute
        task.execute(taskContext);

        // verify
        final List<String> logs = asStringLogs(buildLoggerManager.getLogger(PLAN_RESULT_KEY).getBuildLog());

        assertThat(logs, hasItem("BuildVersion = 1.2.3.4"));
        assertThat(logs, hasItem("ProductsToRelease = Jira;Bamboo;Fisheye"));
    }

    @Test
    public void testBAM11561() throws Exception {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        final MsBuildTaskType task = new MsBuildTaskType(processService, environmentVariableAccessor, capabilityContext);

        final String msBuildCapabilityLabel = pickNativeMsBuilderCapability(capabilitySet);
        final String options = "/nologo /t:BAM-11561 /p:Configuration=Release;Platform=\"Any CPU\" /clp:ShowCommandLine /v:diag";
        final String solution = msBuildProjectFile.getAbsolutePath();

        when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(ImmutableMap.of(
                MsBuildConfig.LABEL, msBuildCapabilityLabel,
                MsBuildConfig.SOLUTION, solution,
                MsBuildConfig.OPTIONS, options
        )));

        // execute
        task.execute(taskContext);

        // verify
        final List<String> logs = asStringLogs(buildLoggerManager.getLogger(PLAN_RESULT_KEY).getBuildLog());

        assertThat(logs, hasItem("Configuration = Release"));
        assertThat(logs, hasItem("Platform = Any CPU"));
    }

    @Test
    public void testBAM13608() throws Exception {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        final MsBuildTaskType task = new MsBuildTaskType(processService, environmentVariableAccessor, capabilityContext);

        final String msBuildCapabilityLabel = pickNativeMsBuilderCapability(capabilitySet);
        final String options = "/nologo /t:BAM-13608 /p:ReferencePath=\"C:\\Program Files (x86)\\WiX Toolset v3.7\\SDK;C:\\Program Files (x86)\\DGIT\\bin\" /clp:ShowCommandLine /v:diag";
        final String solution = msBuildProjectFile.getAbsolutePath();

        when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(ImmutableMap.of(
                MsBuildConfig.LABEL, msBuildCapabilityLabel,
                MsBuildConfig.SOLUTION, solution,
                MsBuildConfig.OPTIONS, options
        )));

        // execute
        task.execute(taskContext);

        // verify
        final List<String> logs = asStringLogs(buildLoggerManager.getLogger(PLAN_RESULT_KEY).getBuildLog());

        assertThat(logs, hasItem("ReferencePath = C:\\Program Files (x86)\\WiX Toolset v3.7\\SDK;C:\\Program Files (x86)\\DGIT\\bin"));
    }

    @Test
    public void testBAM22059() throws Exception {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        final MsBuildTaskType task = new MsBuildTaskType(processService, environmentVariableAccessor, capabilityContext);

        final String msBuildCapabilityLabel = pickNativeMsBuilderCapability(capabilitySet);
        final String solution = msBuildProjectFile.getAbsolutePath();

        when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(ImmutableMap.of(
                MsBuildConfig.LABEL, msBuildCapabilityLabel,
                MsBuildConfig.SOLUTION, solution
        )));

        // execute
        task.execute(taskContext);

        // verify
        assertNotNull(asStringLogs(buildLoggerManager.getLogger(PLAN_RESULT_KEY).getBuildLog()));
    }


    private List<String> asStringLogs(List<LogEntry> logs) {
        return logs.stream()
                .map(LogEntry::getUnstyledLog)
                .collect(Collectors.toList());
    }

    private boolean hasCapability(@NotNull CapabilitySet capabilitySet, @NotNull String capabilityKey) {
        return capabilitySet.getCapability(String.join(".", MsBuildTaskType.MSBUILD_CAPABILITY_PREFIX, capabilityKey)) != null;
    }

    private String pickNativeMsBuilderCapability(@NotNull CapabilitySet capabilitySet) {
        return Stream.of(
                        "MSBuild v4.0 (64bit)",
                        "MSBuild v4.0 (32bit)")
                .filter(candidate -> hasCapability(capabilitySet, candidate))
                .findFirst()
                .orElseThrow(() -> new AssertionError("Can't find native MSBuild"));
    }
}
