package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.util.List;

import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.CAPABILITY_KEY;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.EXCLUDED_TEST_CATEGORIES;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.EXCLUDE_PARAMETER;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.INCLUDED_TEST_CATEGORIES;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.INCLUDE_PARAMETER;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.RESULTS_FILE;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.TEST_FILES;
import static com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType.WHERE_PARAMETER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class NUnitRunnerTaskTypeTest
{
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock private ProcessService processService;
    @Mock private TestCollationService testCollationService;
    @Mock private CapabilityContext capabilityContext;
    @Mock private EnvironmentVariableAccessor environmentVariableAccessor;

    private enum NUnitType
    {
        NUNIT2, NUNIT3
    }

    @Parameters(method = "parametersForNunit3")
    @Test
    public void testNunitTestCategories(NUnitType isNunit, @Nullable String include, @Nullable String exclude) throws IOException
    {
        final NUnitRunnerTaskType taskType
                = new NUnitRunnerTaskType(processService, testCollationService, getCapabilityContext(), environmentVariableAccessor);
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(TEST_FILES, "*.*");
        configurationMap.put(RESULTS_FILE, "results.xml");
        if (isNunit == NUnitType.NUNIT3)
        {
            configurationMap.put(CAPABILITY_KEY, NUNIT3_CAPABILITY_PREFIX);
        }
        else
        {
            configurationMap.put(CAPABILITY_KEY, NUNIT_CAPABILITY_PREFIX);
        }
        configurationMap.put(INCLUDED_TEST_CATEGORIES, include);
        configurationMap.put(EXCLUDED_TEST_CATEGORIES, exclude);
        final List<String> command = taskType.getCommand(configurationMap, "results.txt");
        if (isNunit == NUnitType.NUNIT3)
        {
            assertThat(command, hasItem(containsString(WHERE_PARAMETER)));
            assertThat(command, not(hasItem(containsString(INCLUDE_PARAMETER))));
            assertThat(command, not(hasItem(containsString(EXCLUDE_PARAMETER))));
        }
        else
        {
            assertThat(command, not(hasItem(containsString(WHERE_PARAMETER))));
            if (StringUtils.isNotEmpty(include))
            {
                assertThat(command, hasItem(containsString(INCLUDE_PARAMETER)));
            }
            if (StringUtils.isNotEmpty(exclude))
            {
                assertThat(command, hasItem(containsString(EXCLUDE_PARAMETER)));
            }
        }
    }

    @SuppressWarnings("unused")
    private Object[][] parametersForNunit3()
    {
        return new Object[][]{
                {NUnitType.NUNIT3, "INCLUDE", null},
                {NUnitType.NUNIT3, null, "EXCLUDE"},
                {NUnitType.NUNIT3, "INCLUDE", "EXCLUDE"},
                {NUnitType.NUNIT2, "INCLUDE", null},
                {NUnitType.NUNIT2, null, "EXCLUDE"},
                {NUnitType.NUNIT2, "INCLUDE", "EXCLUDE"},
        };
    }

    private CapabilityContext getCapabilityContext() throws IOException
    {
        final CapabilitySet capabilitySet = mock(CapabilitySet.class);
        final Capability capability = mock(Capability.class);
        temporaryFolder.create();
        when(capability.getValue()).thenReturn(temporaryFolder.getRoot().getAbsolutePath());
        when(capabilitySet.getCapability(any(String.class))).thenReturn(capability);
        when(capabilityContext.getCapabilitySet()).thenReturn(capabilitySet);
        return capabilityContext;
    }

}
