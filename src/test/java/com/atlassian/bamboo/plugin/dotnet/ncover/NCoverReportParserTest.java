package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.utils.BambooTestUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Simple test to verify that the NCoverReportParser processes a test file correctly.
 * @author Ross Rowe
 *
 */
public class NCoverReportParserTest {
	
	private NCoverReportParser parser;
	
	private static final String TEST_NCOVER_REPORT = "com/atlassian/bamboo/plugin/dotnet/ncover/Coverage.xml";

    private static final String TEST_NCOVER_1_5_2_REPORT = "com/atlassian/bamboo/plugin/dotnet/ncover/Coverage-1.5.2.xml";
    private static final String TEST_LARGE_REPORT = "com/atlassian/bamboo/plugin/dotnet/ncover/NCover_Coverage_Result.xml";


    @Before
	public void setUp() {
		parser = new NCoverReportParser();
	}

    @Test
    public void dummyReportParse() throws Exception {
		InputStream inputStream = BambooTestUtils.getInputStream(TEST_NCOVER_REPORT);
		assertNotNull(inputStream);
		parser.parse(inputStream);
		
        assertEquals("covered elements should be 75.3", 0.7535421327367636, parser.getLineRate(), 0.0000000000000001);
        assertEquals("classes should be 197", 197L, parser.getClasses());
        assertEquals("methods should be 759", 759L, parser.getMethods());

	}

    @Test
    public void parse152ReportFormat() throws Exception {
		InputStream inputStream = BambooTestUtils.getInputStream(TEST_NCOVER_1_5_2_REPORT);
		assertNotNull(inputStream);
		parser.parse(inputStream);

        assertEquals("covered elements should be 39.9", 0.39913544668587897, parser.getLineRate(), 0.00000000000000001);
        assertEquals("classes should be 8", 8L, parser.getClasses());
        assertEquals("methods should be 125", 125L, parser.getMethods());

	}

//    @Test
//    public void parseLargeFile() throws Exception {
//		InputStream inputStream = BambooTestUtils.getInputStream(TEST_LARGE_REPORT);
//		assertNotNull(inputStream);
//		parser.parse(inputStream);
//
//	}

}
