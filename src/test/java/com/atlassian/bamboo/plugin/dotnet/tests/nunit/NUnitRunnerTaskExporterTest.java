package com.atlassian.bamboo.plugin.dotnet.tests.nunit;

import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.NUnitRunnerTask;
import com.atlassian.bamboo.specs.model.task.NUnitRunnerTaskProperties;
import com.atlassian.bamboo.task.TaskDefinition;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.util.Map;

import static com.google.common.base.Strings.nullToEmpty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class NUnitRunnerTaskExporterTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testToSpecsEntity()
    {
        //given
        TaskDefinition taskDefinition = getTaskDefinition(getConfiguration());

        //when
        final NUnitRunnerTask task = new NUnitRunnerTaskExporter().toSpecsEntity(taskDefinition);

        //then
        final NUnitRunnerTaskProperties properties = EntityPropertiesBuilders.build(task);
        assertThat(properties.getTestsToRun(), contains("test1", "test2"));
    }

    @Test
    public void testImportAndExport()
    {
        //given
        final ImmutableMap<String, String> expectedMap = getConfiguration();

        //when
        final Map<String, String> cfg = toTaskConfiguration(expectedMap);

        //then
        Sets.union(cfg.keySet(), expectedMap.keySet()).forEach(key -> {
            final String actual = nullToEmpty(cfg.get(key));
            final String expected = nullToEmpty(expectedMap.get(key));
            assertThat(actual, equalTo(expected));
        });
    }

    @Test
    public void testNUnit2()
    {
        //given
        final ImmutableMap<String, String> expectedMap = getConfiguration(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX + ".foobar");

        //when
        final Map<String, String> cfg = toTaskConfiguration(expectedMap);

        //then
        Sets.union(cfg.keySet(), expectedMap.keySet()).forEach(key -> {
            final String actual = nullToEmpty(cfg.get(key));
            final String expected = nullToEmpty(expectedMap.get(key));
            assertThat(actual, equalTo(expected));
        });
    }

    @Test
    public void testNUnit3()
    {
        //given
        final ImmutableMap<String, String> expectedMap = getConfiguration(NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX + ".foobar");

        //when
        final Map<String, String> cfg = toTaskConfiguration(expectedMap);

        //then
        Sets.union(cfg.keySet(), expectedMap.keySet()).forEach(key -> {
            final String actual = nullToEmpty(cfg.get(key));
            final String expected = nullToEmpty(expectedMap.get(key));
            assertThat(actual, equalTo(expected));
        });
    }

    @Test
    public void testNUnit2SpecsLabel()
    {
        //given
        final ImmutableMap<String, String> expectedMap = getConfiguration(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX + ".foobar");

        //when
        TaskDefinition taskDefinition = getTaskDefinition(expectedMap);
        final NUnitRunnerTask task = new NUnitRunnerTaskExporter().toSpecsEntity(taskDefinition);
        final NUnitRunnerTaskProperties properties = EntityPropertiesBuilders.build(task);

        //then
        assertThat(properties.getExecutable(), equalTo("foobar"));
        assertThat(properties.getNUnitVersion(), equalTo(NUnitRunnerTask.NUnitVersion.NUNIT_2));
    }

    @Test
    public void testNUnit3SpecsLabel()
    {
        //given
        final ImmutableMap<String, String> expectedMap = getConfiguration(NUnitRunnerTaskType.NUNIT3_CAPABILITY_PREFIX + ".foobar");

        //when
        TaskDefinition taskDefinition = getTaskDefinition(expectedMap);
        final NUnitRunnerTask task = new NUnitRunnerTaskExporter().toSpecsEntity(taskDefinition);
        final NUnitRunnerTaskProperties properties = EntityPropertiesBuilders.build(task);

        //then
        assertThat(properties.getExecutable(), equalTo("foobar"));
        assertThat(properties.getNUnitVersion(), equalTo(NUnitRunnerTask.NUnitVersion.NUNIT_3));
    }

    @Test
    public void testSoleLabel()
    {
        //given
        final ImmutableMap<String, String> expectedMap = getConfiguration("foobar");

        //when
        final Map<String, String> cfg = toTaskConfiguration(expectedMap);

        //then
        assertThat(cfg.get(NUnitRunnerTaskType.CAPABILITY_KEY), equalTo(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX + ".foobar"));
    }

    private Map<String, String> toTaskConfiguration(ImmutableMap<String, String> expectedMap) {
        TaskDefinition taskDefinition = getTaskDefinition(expectedMap);
        final NUnitRunnerTask task = new NUnitRunnerTaskExporter().toSpecsEntity(taskDefinition);
        final NUnitRunnerTaskProperties properties = EntityPropertiesBuilders.build(task);
        return new NUnitRunnerTaskExporter().toTaskConfiguration(() -> null, properties);
    }

    private TaskDefinition getTaskDefinition(Map<String, String> configuration) {
        TaskDefinition taskDefinition = Mockito.mock(TaskDefinition.class);
        Mockito.when(taskDefinition.getConfiguration()).thenReturn(configuration);
        return taskDefinition;
    }

    private ImmutableMap<String, String> getConfiguration() {
        return getConfiguration("system.builder.nunit.label");
    }

    private ImmutableMap<String, String> getConfiguration(String capabilitKey) {
        return ImmutableMap.of(
                NUnitRunnerTaskType.CAPABILITY_KEY, capabilitKey,
                NUnitRunnerTaskType.TEST_FILES, "files",
                NUnitRunnerTaskType.RESULTS_FILE, "result.xml",
                NUnitRunnerTaskType.TESTS_TO_RUN, "test1,test2");
    }

}
