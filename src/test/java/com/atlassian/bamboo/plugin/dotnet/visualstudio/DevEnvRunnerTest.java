package com.atlassian.bamboo.plugin.dotnet.visualstudio;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessContextFactory;
import com.atlassian.bamboo.process.ProcessContextFactoryImpl;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.process.ProcessServiceImpl;
import com.atlassian.bamboo.spring.AutowiringSupport;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskProcessCommandDecoratorModuleDescriptor;
import com.atlassian.bamboo.testutils.build.logger.BuildLoggerManagerForTesting;
import com.atlassian.bamboo.utils.BambooTestUtils;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.v2.build.CurrentBuildResultImpl;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.utils.process.ExternalProcess;
import org.apache.commons.lang3.SystemUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assume.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DevEnvRunnerTest
{
    private static final PlanResultKey PLAN_RESULT_KEY = PlanKeys.getPlanResultKey("FOO-BAR", 1);
    private static final String PLAN_NAME = "Foo Bar";
    private static final String TASK_PLUGIN_KEY = "com.atlassian.foobar";

    @Rule public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock private EnvironmentVariableAccessor mockEnvironmentVariableAccessor;
    @Mock private PluginAccessor mockPluginAccessor;
    @Mock private TaskContext mockTaskContext;

    private ProcessService processService;
    private BuildLoggerManagerForTesting buildLoggerManager;

    @Before
    public void setUp()
    {
        buildLoggerManager = new BuildLoggerManagerForTesting();
        VariableContext mockVariableContext = mock(VariableContext.class);
        when(mockVariableContext.getEffectiveVariables()).thenReturn(Collections.emptyMap());

        ProcessContextFactory processContextFactory = new ProcessContextFactoryImpl();
        AutowiringSupport.autowire(processContextFactory,  mockEnvironmentVariableAccessor, mockPluginAccessor, buildLoggerManager);
        processService = new ProcessServiceImpl();
        AutowiringSupport.autowire(processService, mockEnvironmentVariableAccessor, processContextFactory);

        CurrentBuildResult currentBuildResult = new CurrentBuildResultImpl();

        BuildContext mockBuildContext = mock(BuildContext.class);
        when(mockBuildContext.getResultKey()).thenReturn(PLAN_RESULT_KEY);
        when(mockBuildContext.getPlanResultKey()).thenReturn(PLAN_RESULT_KEY);
        when(mockBuildContext.getPlanName()).thenReturn(PLAN_NAME);
        when(mockBuildContext.getBuildResult()).thenReturn(currentBuildResult);
        when(mockBuildContext.getVariableContext()).thenReturn(mockVariableContext);

        when(mockTaskContext.getCommonContext()).thenReturn(mockBuildContext);
        when(mockTaskContext.getBuildLogger()).thenReturn(buildLoggerManager.getLogger(PLAN_RESULT_KEY));
        when(mockTaskContext.getPluginKey()).thenReturn(TASK_PLUGIN_KEY);

        final Map<String, String> environment = new HashMap<>();
        when(mockEnvironmentVariableAccessor.getEnvironment(mockTaskContext)).thenReturn(environment);
    }

    @Test
    public void testTargetWithPipe() throws Exception
    {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        when(mockEnvironmentVariableAccessor.getPaths(mockTaskContext)).thenReturn(Collections.emptyList());
        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(TaskProcessCommandDecoratorModuleDescriptor.class)).thenReturn(Collections.emptyList());

        final List<String> actualArguments = Arrays.asList(
                "C:\\bamboo\\xml-data\\build-dir\\TRUNK-WIN64-SANDBOX\\source",
                "C:\\Program Files (x86)\\Microsoft Visual Studio 10.0",
                "x86",
                "product/legasrv/legasrv-vs2010.sln",
                "/build",
                "Release Unicode|x64",
                "\"Release Unicode|x86\"",
                "\"Release|x86\""
        );
        final List<String> expectedOutput = Arrays.asList(
                "cd \"C:\\bamboo\\xml-data\\build-dir\\TRUNK-WIN64-SANDBOX\\source\"",
                "call \"C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\VC\\vcvarsall.bat\" x86",
                "call \"C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\IDE\\devenv.com\"  \"product/legasrv/legasrv-vs2010.sln\" /build \"Release Unicode|x64\" \"Release Unicode|x86\" \"Release|x86\""
        );

        final List<String> command = Stream.concat(
                Stream.of(BambooTestUtils.getFileFromResourceDirectory(this, "devenvrunner-test.bat").getPath()),
                actualArguments.stream()
        ).collect(Collectors.toList());

        ExternalProcess externalProcess = processService.executeExternalProcess(
                mockTaskContext,
                new ExternalProcessBuilder()
                        .workingDirectory(new File("."))
                        .command(command)
        );

        assertThat(externalProcess.getHandler().isComplete(), is(true));

        List<String> logs = asStringLogs(buildLoggerManager.getLogger(PLAN_RESULT_KEY).getBuildLog());
        assertThat(logs.size(), greaterThanOrEqualTo(expectedOutput.size()));
        assertThat(logs.subList(logs.size() - expectedOutput.size(), logs.size()), equalTo(expectedOutput));
    }

    private List<String> asStringLogs(List<LogEntry> logs)
    {
        return logs.stream()
                .map(LogEntry::getUnstyledLog)
                .collect(Collectors.toList());
    }

    @Test
    public void testTargetWithoutPipe() throws Exception
    {
        assumeThat(SystemUtils.IS_OS_WINDOWS, is(true));

        when(mockEnvironmentVariableAccessor.getPaths(mockTaskContext)).thenReturn(Collections.emptyList());
        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(TaskProcessCommandDecoratorModuleDescriptor.class)).thenReturn(Collections.emptyList());

        final List<String> actualArguments = Arrays.asList(
                "C:\\bamboo\\xml-data\\build-dir\\TRUNK-WIN64-SANDBOX\\source",
                "C:\\Program Files (x86)\\Microsoft Visual Studio 10.0",
                "x86",
                "product/legasrv/legasrv-vs2010.sln",
                "/build",
                "Release Unicode"
        );
        final List<String> expectedOutput = Arrays.asList(
                "cd \"C:\\bamboo\\xml-data\\build-dir\\TRUNK-WIN64-SANDBOX\\source\"",
                "call \"C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\VC\\vcvarsall.bat\" x86",
                "call \"C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\IDE\\devenv.com\"  \"product/legasrv/legasrv-vs2010.sln\" /build \"Release Unicode\""
        );

        final List<String> command = Stream.concat(
                Stream.of(BambooTestUtils.getFileFromResourceDirectory(this, "devenvrunner-test.bat").getPath()),
                actualArguments.stream()
        ).collect(Collectors.toList());

        ExternalProcess externalProcess = processService.executeExternalProcess(
                mockTaskContext,
                new ExternalProcessBuilder()
                        .workingDirectory(new File("."))
                        .command(command)
        );

        assertThat(externalProcess.getHandler().isComplete(), is(true));

        List<String> logs = asStringLogs(buildLoggerManager.getLogger(PLAN_RESULT_KEY).getBuildLog());
        assertThat(logs.size(), greaterThanOrEqualTo(expectedOutput.size()));
        assertThat(logs.subList(logs.size() - expectedOutput.size(), logs.size()), equalTo(expectedOutput));
    }

}