package com.atlassian.bamboo.plugin.dotnet.tests.mstest;

import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;

public class MSTestParserExporterTest {

    private final String TEST_RESULT_DIRECTORY = "**/TestResults/*.trx";
    private final Boolean PICK_UP_TEST_RESULTS_CREATED_OUTSIDE_THIS_BUILD = Boolean.TRUE;
    private final String PICK_UP_TEST_RESULTS_CREATED_OUTSIDE_THIS_BUILD_STRING = "true";
    private MSTestParserExporter exporter;

    @Before
    public void setUp() {
        exporter = new MSTestParserExporter();
    }

    @Test
    public void testToTaskConfiguration() {
        final TestParserTask task = new TestParserTask(TestParserTaskProperties.TestType.MSTEST)
                .resultDirectories(TEST_RESULT_DIRECTORY)
                .pickUpTestResultsCreatedOutsideOfThisBuild(PICK_UP_TEST_RESULTS_CREATED_OUTSIDE_THIS_BUILD);

        final Map<String, String> taskConfiguration = exporter.toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(taskConfiguration.get(MSTestCollectorTaskType.TEST_DIRECTORY), equalTo(TEST_RESULT_DIRECTORY));
        assertThat(taskConfiguration.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE), equalTo(PICK_UP_TEST_RESULTS_CREATED_OUTSIDE_THIS_BUILD_STRING));
    }

    @Test
    public void testToSpecsEntity() {
        ImmutableMap.Builder<String, String> mapBuilder = new ImmutableMap.Builder<>();
        mapBuilder.put(MSTestCollectorTaskType.TEST_DIRECTORY, TEST_RESULT_DIRECTORY);
        mapBuilder.put(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, PICK_UP_TEST_RESULTS_CREATED_OUTSIDE_THIS_BUILD_STRING);

        final TaskDefinitionImpl taskDefinition = new TaskDefinitionImpl(0,
                "",
                "task description",
                mapBuilder.build());


        final TestParserTask builder = (TestParserTask) exporter.toSpecsEntity(taskDefinition);
        final TestParserTaskProperties task = EntityPropertiesBuilders.build(builder);

        assertThat(task.getResultDirectories(), hasSize(1));
        assertThat(task.getResultDirectories().get(0), equalTo(TEST_RESULT_DIRECTORY));
        assertThat(task.getPickUpTestResultsCreatedOutsideOfThisBuild(), equalTo(PICK_UP_TEST_RESULTS_CREATED_OUTSIDE_THIS_BUILD));

    }
}
