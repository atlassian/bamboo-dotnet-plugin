package com.atlassian.bamboo.plugin.dotnet.ncover;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.spring.AutowiringSupport;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildContextHelper;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.junit.Before;
import org.junit.Test;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @author Ross Rowe
 */
public class NCoverBuildProcessorTest {

	private static final PlanResultKey TEST_PLAN_RESULT_KEY = PlanKeys.getPlanResultKey("FOO-BAR-1");

	private NCoverBuildProcessor buildProcessor;

	private CurrentBuildResult buildResult;

	private Map configuration;

	private Map customBuildData;

	private Map previousCustomData;
	private NCoverBuildProcessorServer buildProcessorServer;

	public NCoverBuildProcessorTest() {
		configuration = new HashMap();
		customBuildData = new HashMap();
		previousCustomData = new HashMap();
	}

	@SuppressWarnings("unchecked")
	@Before
	public void setUp()
	{
		this.buildProcessor = new NCoverBuildProcessor();
		this.buildProcessorServer = new NCoverBuildProcessorServer();

		// enable ncover by default
		configuration.put(NCoverBuildProcessor.NCOVER_EXISTS, "true");

		// Create Mocks
		buildResult = createMock(CurrentBuildResult.class);
		BuildContext buildContext = createMock(BuildContext.class);
		BuildDefinition buildPlanDefinition = createMock(BuildDefinition.class);
		ResultsSummaryManager resultsSummaryManager = createMock(ResultsSummaryManager.class);
        BuildResultsSummary previousSummary = createMock(BuildResultsSummary.class);

		expect(buildContext.getBuildDefinition()).andReturn(
				buildPlanDefinition).anyTimes();
		expect(buildContext.getPlanResultKey()).andReturn(TEST_PLAN_RESULT_KEY).anyTimes();
		expect(buildContext.getBuildResult()).andReturn(buildResult).anyTimes();
		expect(buildContext.getCurrentResult()).andReturn(buildResult).anyTimes();
		replay(buildContext);

		expect(buildResult.getCustomBuildData()).andReturn(customBuildData)
				.anyTimes();
		expect(buildPlanDefinition.getCustomConfiguration()).andReturn(
				configuration).anyTimes();
		customBuildData.put(BuildContextHelper.BUILD_WORKING_DIRECTORY, System.getProperty("user.dir"));
		expect(
				resultsSummaryManager
						.getLastSuccessfulResultSummary(TEST_PLAN_RESULT_KEY.getPlanKey()))
				.andReturn(previousSummary).anyTimes();
		expect(previousSummary.getCustomBuildData()).andReturn(
				previousCustomData).anyTimes();

		replay(buildResult);
		replay(buildPlanDefinition);
		replay(resultsSummaryManager);
		replay(previousSummary);

		TransactionTemplate mockTransactionTemplate = new TransactionTemplate() {
			@Override
			public <T> T execute(TransactionCallback<T> transactionCallback) {
				return transactionCallback.doInTransaction();
			}
		};
		AutowiringSupport.autowire(buildProcessorServer, mockTransactionTemplate, resultsSummaryManager);

		buildProcessor.init(buildContext);
		buildProcessorServer.init(buildContext);
		configuration.put(NCoverBuildProcessor.NCOVER_XML_PATH_KEY,
				"src/test/resources/com/atlassian/bamboo/plugin/dotnet/ncover/Coverage.xml");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void unableToLocateCoverageFile() throws Exception {
		configuration.put(NCoverBuildProcessor.NCOVER_XML_PATH_KEY,
				"src/test/resources/blah.xml");
		buildProcessor.call();
		buildProcessorServer.call();
		Map map = buildResult.getCustomBuildData();
		assertNull(map.get(NCoverBuildProcessor.NCOVER_LINE_RATE));
	}

	@Test
	public void noPreviousCoverage() throws Exception {
		reset(buildProcessorServer.getResultsSummaryManager());
		expect(
				buildProcessorServer.getResultsSummaryManager()
						.getLastSuccessfulResultSummary(TEST_PLAN_RESULT_KEY.getPlanKey()))
				.andReturn(null).anyTimes();
		replay(buildProcessorServer.getResultsSummaryManager());

		buildProcessor.call();
		buildProcessorServer.call();
		Map map = buildResult.getCustomBuildData();
		// assert that custom data map includes NCOVER* results
		assertNotNull(map.get(NCoverBuildProcessor.NCOVER_LINE_RATE));
		assertNull(map.get(NCoverBuildProcessor.NCOVER_COVERAGE_DELTA));

	}

	@SuppressWarnings("unchecked")
	@Test
	public void differenceInCoverage() throws Exception {
		previousCustomData.put(NCoverBuildProcessor.NCOVER_LINE_RATE, "10");

		buildProcessor.call();
		buildProcessorServer.call();
		Map map = buildResult.getCustomBuildData();
		// assert that custom data map includes NCOVER* results
		// assert that custom data map includes non-zero deltas
		assertNotNull(map.get(NCoverBuildProcessor.NCOVER_LINE_RATE));
		assertNotNull(map.get(NCoverBuildProcessor.NCOVER_COVERAGE_DELTA));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void notRunNCoverIfDisabled() throws Exception {
		configuration.remove(NCoverBuildProcessor.NCOVER_EXISTS);
		previousCustomData.put(NCoverBuildProcessor.NCOVER_LINE_RATE, "10");

		buildProcessor.call();
		buildProcessorServer.call();
		Map map = buildResult.getCustomBuildData();
		// assert that custom data map does not include NCOVER* results
		// assert that custom data map does not include deltas
		assertNull(map.get(NCoverBuildProcessor.NCOVER_LINE_RATE));
		assertNull(map.get(NCoverBuildProcessor.NCOVER_COVERAGE_DELTA));
	}
}
