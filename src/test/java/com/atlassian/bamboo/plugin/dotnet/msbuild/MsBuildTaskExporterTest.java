package com.atlassian.bamboo.plugin.dotnet.msbuild;

import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.MsBuildTask;
import com.atlassian.bamboo.specs.model.task.MsBuildTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

public class MsBuildTaskExporterTest {
    private MsBuildTaskExporter msBuildTaskExporter;

    private final String EXECUTABLE = "EXECUTABLE";
    private final String PROJECT_FILE = "PROJECT_FILE";
    private final String OPTIONS = "-options";
    private final String ENVIRONMENT_VARIABLES = "OPTS=\"-a1 -b2";
    private final String WORKING_SUBDIRECTORY = "/tmp/";

    @Before
    public void setUp()
    {
        msBuildTaskExporter = new MsBuildTaskExporter();
    }

    @Test
    public void testToTaskConfiguration()
    {
        final MsBuildTask task = new MsBuildTask()
                .executable(EXECUTABLE)
                .projectFile(PROJECT_FILE)
                .options(OPTIONS)
                .environmentVariables(ENVIRONMENT_VARIABLES)
                .workingSubdirectory(WORKING_SUBDIRECTORY);

        final Map<String, String> taskConfiguration = msBuildTaskExporter.toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(taskConfiguration.get(MsBuildConfig.LABEL), equalTo(EXECUTABLE));
        assertThat(taskConfiguration.get(MsBuildConfig.SOLUTION), equalTo(PROJECT_FILE));
        assertThat(taskConfiguration.get(MsBuildConfig.OPTIONS), equalTo(OPTIONS));
        assertThat(taskConfiguration.get(MsBuildConfig.ENVIRONMENT), equalTo(ENVIRONMENT_VARIABLES));
        assertThat(taskConfiguration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), equalTo(WORKING_SUBDIRECTORY));
    }

    @Test
    public void testToSpecsEntity()
    {
        ImmutableMap.Builder<String, String> mapBuilder = new ImmutableMap.Builder<>();
        mapBuilder.put(MsBuildConfig.LABEL, EXECUTABLE);
        mapBuilder.put(MsBuildConfig.SOLUTION, PROJECT_FILE);
        mapBuilder.put(MsBuildConfig.OPTIONS, OPTIONS);
        mapBuilder.put(MsBuildConfig.ENVIRONMENT, ENVIRONMENT_VARIABLES);
        mapBuilder.put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, WORKING_SUBDIRECTORY);

        final TaskDefinitionImpl taskDefinition = new TaskDefinitionImpl(0,
                "",
                "task description",
                mapBuilder.build());

        final MsBuildTask builder = msBuildTaskExporter.toSpecsEntity(taskDefinition);
        final MsBuildTaskProperties task = EntityPropertiesBuilders.build(builder);

        assertThat(task.getExecutable(), equalTo(EXECUTABLE));
        assertThat(task.getProjectFile(), equalTo(PROJECT_FILE));
        assertThat(task.getOptions(), equalTo(OPTIONS));
        assertThat(task.getEnvironmentVariables(), equalTo(ENVIRONMENT_VARIABLES));
        assertThat(task.getWorkingSubdirectory(), equalTo(WORKING_SUBDIRECTORY));
    }
}
