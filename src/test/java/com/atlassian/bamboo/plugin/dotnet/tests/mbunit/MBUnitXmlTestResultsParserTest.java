package com.atlassian.bamboo.plugin.dotnet.tests.mbunit;

import com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParser;
import com.atlassian.bamboo.utils.BambooTestUtils;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;

import static com.atlassian.bamboo.plugin.dotnet.tests.TestResultsParserMatcherBuilder.testResultsParser;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;

/**
 * @author Ross Rowe
 *
 */
@RunWith(JUnitParamsRunner.class)
public class MBUnitXmlTestResultsParserTest
{
    private static final String PROP_XML_TRANSFORMER_FACTORY = "javax.xml.transform.TransformerFactory";
    private static final String UPDATED_FORMAT_FILE = "com/atlassian/bamboo/plugin/dotnet/tests/mbunit/mbunit_new.xml";

    @Rule public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(PROP_XML_TRANSFORMER_FACTORY);

    private TestResultsParser parser;

    @Before
    public void setUp() {
        parser = new MBUnitXmlTestResultsParser();
    }

    @Parameters({
            "mbunit.xml, 4, 1, 1",
            "test-report-20090118-164832.xml, 5, 1, 0",
    })
    @Test
    public void testParser(@NotNull String inputFile, int successfulTestCount, int failedTestCount, int inconclusiveTestCount)
    {
        parser.parse(BambooTestUtils.getInputStream(inputFile, this));

        assertThat(parser,
                   testResultsParser()
                           .successfulTests(successfulTestCount)
                           .failedTests(failedTestCount)
                           .inconclusiveTests(inconclusiveTestCount)
                           .build());
    }

    @Test
    public void parseNewFormat()
    {
        System.setProperty(PROP_XML_TRANSFORMER_FACTORY,
                           "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
        parser.parse(BambooTestUtils.getInputStream(UPDATED_FORMAT_FILE));
        assertFalse(parser.getSuccessfulTests().isEmpty());
    }
}
