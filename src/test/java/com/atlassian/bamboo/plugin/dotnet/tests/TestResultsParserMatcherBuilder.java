package com.atlassian.bamboo.plugin.dotnet.tests;

import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.testutils.matchers.PropertyMatcherBuilder;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

public class TestResultsParserMatcherBuilder extends PropertyMatcherBuilder<TestResultsParser, TestResultsParserMatcherBuilder>
{
    public TestResultsParserMatcherBuilder failedTests(Matcher<Iterable<TestResults>> matcher)
    {
        return put("failedTests", matcher);
    }

    public TestResultsParserMatcherBuilder failedTests(int size)
    {
        return failedTests(Matchers.<TestResults>iterableWithSize(size));
    }

    public TestResultsParserMatcherBuilder inconclusiveTests(Matcher<Iterable<TestResults>> matcher)
    {
        return put("inconclusiveTests", matcher);
    }

    public TestResultsParserMatcherBuilder inconclusiveTests(int size)
    {
        return inconclusiveTests(Matchers.<TestResults>iterableWithSize(size));
    }

    public TestResultsParserMatcherBuilder successfulTests(Matcher<Iterable<TestResults>> matcher)
    {
        return put("successfulTests", matcher);
    }

    public TestResultsParserMatcherBuilder successfulTests(int size)
    {
        return successfulTests(Matchers.<TestResults>iterableWithSize(size));
    }


    public static TestResultsParserMatcherBuilder testResultsParser()
    {
        return new TestResultsParserMatcherBuilder();
    }
}
