<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/TR/html4/strict.dtd">	
	<xsl:output method="html"/>	
	
	<xsl:template match="/">
		<xsl:variable name="coverage.root" select="cruisecontrol//coverage" />
		<xsl:variable name="visited" select="count($coverage.root/module/method/seqpnt[not(@visitcount=0)])" />
		<xsl:variable name="total" select="count($coverage.root/module/method/seqpnt)" />
		<div id="report">
			<script>
				function toggleDiv( imgId, divId )
				{
					eDiv = document.getElementById( divId );
					eImg = document.getElementById( imgId );
					
					if ( eDiv.style.display == "none" )
					{
						eDiv.style.display = "block";
						eImg.src = "images/arrow_minus_small.gif";
					}
					else
					{
						eDiv.style.display = "none";
						eImg.src = "images/arrow_plus_small.gif";
					}
				}
			</script>
			<h1>NCover Results</h1>
			<div id="summary">
				<h3>Summary</h3>
				<table>
					<tbody>
						<tr>
							<td>Assemblies checked:</td>
							<td><xsl:value-of select="count($coverage.root/module)"/></td>
						</tr>
						<tr>
							<td>Lines of Code:</td>
							<td><xsl:value-of select="$total"/></td>
						</tr>
						<tr>
							<td>Lines Executed:</td>
							<td><xsl:value-of select="$visited"/></td>
						</tr>
						<tr>
							<td>Lines Not Executed</td>
							<td><xsl:value-of select="$total - $visited"/></td>
						</tr>
						<tr>
							<td>Code Coverage:</td>
							<td><xsl:value-of select="concat (format-number($visited div $total * 100,'0.##'), '%')"/></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="details">
				<h3>Assembly Coverage Details:</h3>
				<xsl:apply-templates select="$coverage.root" />
			</div>
		</div>
	</xsl:template>
	
	<xsl:template match="coverage/module">
		<xsl:variable name="coverage.module.id" select="generate-id()" />
		<xsl:variable name="coverage.module.name" select="./@assembly"/>
		<xsl:variable name="total" select="count(./method/seqpnt)" />
		<xsl:variable name="visited" select="count(./method/seqpnt[not(@visitcount=0)])" />
		
		<table cellpadding="2" cellspacing="0" border="0" width="98%">
			<tr>
				<td class="yellow-sectionheader" colspan="3" valign="top">
					<xsl:call-template name="PassFailIcon">
						<xsl:with-param name="visitPercent" select="$visited div $total * 100" />
					</xsl:call-template>
					
					<input type="image" src="images/arrow_minus_small.gif">
						<xsl:attribute name="id">img<xsl:value-of select="$coverage.module.id"/></xsl:attribute>
						<xsl:attribute name="onclick">javascript:toggleDiv('img<xsl:value-of select="$coverage.module.id"/>', 'divDetails<xsl:value-of select="$coverage.module.id"/>');</xsl:attribute>
					</input>&#160;<xsl:value-of select="$coverage.module.name"/>
				</td>
			</tr>
		</table>
		<div>
			<xsl:attribute name="id">divDetails<xsl:value-of select="$coverage.module.id"/></xsl:attribute>
			<blockquote>
				<table>
					<tr>
						<th>Class</th>
						<th>Coverage</th>
						<th>Percent</th>
					</tr>
					<xsl:apply-templates select="method[not(./@class = preceding-sibling::method/@class)]" mode="class">
						<xsl:sort select="@class" order="ascending" data-type="text"/>
						<xsl:sort select="@name" order="ascending" data-type="text"/>
					</xsl:apply-templates>
				</table>
			</blockquote>
		</div>		
	</xsl:template>
	
	<xsl:template match="method" mode="class">
	
		<xsl:variable name="class" select="@class" />
		<xsl:variable name="name" select="substring-after(@class,concat(../@assembly,'.'))" />
		<xsl:variable name="total" select="count(../method[@class=$class]/seqpnt)" />
		<xsl:variable name="visited" select="count(../method[@class=$class]/seqpnt[not(@visitcount=0)])" /> 
		<xsl:variable name="visitPercent" select="$visited div $total * 100" />
	
		<tr>
			<td valign="top" width="30%">
				<input type="image" src="images/arrow_plus_small.gif">
					<xsl:attribute name="id">imgCoverClass_<xsl:value-of select="@class"/></xsl:attribute>
					<xsl:attribute name="onClick">javascript:toggleDiv('imgCoverClass_<xsl:value-of select="@class"/>', 'divMethod_<xsl:value-of select="@class"/>');</xsl:attribute>
				</input>
				<a>
					<xsl:attribute name="name"><xsl:value-of select="@class"/></xsl:attribute>
					<xsl:value-of select="$name"/>
				</a>
			</td>
			<td width="70%">
				<table border="0" cellspacing="1" width="100%">
					<tr>
						<xsl:if test="$visited > 0">
							<td bgcolor="green">
								<xsl:attribute name="width"><xsl:value-of select="$visitPercent"/>%</xsl:attribute>
								&#160;
							</td>
						</xsl:if>
						<xsl:if test="$total > $visited">
							<td bgcolor="red">
								<xsl:attribute name="width"><xsl:value-of select="100 - $visitPercent"/>%</xsl:attribute>
								&#160;
							</td>
						</xsl:if>
					</tr>
				</table>
				<div style="display:none">
					<xsl:attribute name="id">divMethod_<xsl:value-of select="@class"/></xsl:attribute>
					<table border="0" cell-padding="6" cell-spacing="0" width="100%">
						<xsl:apply-templates select="../method[@class=$class]" mode="method">
							<xsl:sort select="@name" order="ascending" data-type="text"/>
						</xsl:apply-templates>
					</table>
				</div>
			</td>
			<td valign="top">
				(<xsl:value-of select="concat (format-number($visitPercent,'0.##'), '%')"/>)
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match="method" mode="method">
		<xsl:variable name="fullname" select="concat(@class,'.',@name,generate-id())"/>
		<xsl:variable name="total" select="count(seqpnt)" />
		<xsl:variable name="visited" select="count(seqpnt[not(@visitcount=0)])" /> 
		<xsl:variable name="visitPercent" select="$visited div $total * 100" />
		
		<tr>
			<xsl:if test="position() mod 2 = 0">
				<xsl:attribute name="class">section-oddrow</xsl:attribute>
			</xsl:if>
			<td valign="top" width="10%">
				<xsl:call-template name="PassFailIcon">
					<xsl:with-param name="visitPercent" select="$visitPercent" />
				</xsl:call-template>
				<input type="image" src="images/arrow_plus_small.gif">
					<xsl:attribute name="id">imgCoverLines_<xsl:value-of select="$fullname"/></xsl:attribute>
					<xsl:attribute name="onClick">javascript:toggleDiv('imgCoverLines_<xsl:value-of select="$fullname"/>', 'divLines_<xsl:value-of select="$fullname"/>');</xsl:attribute>
				</input>
			</td>
			<td valign="top" width="80%">
				<xsl:value-of select="@name" />
			</td>
			<td valign="top" width="10%">
				<xsl:value-of select="concat (format-number($visitPercent,'0.##'), '%')"/>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div style="display:none">
					<xsl:attribute name="id">divLines_<xsl:value-of select="$fullname"/></xsl:attribute>
					<table border="0" cell-padding="6" cell-spacing="0" width="100%" bgcolor="white">
					<tr>
						<th align="left">Line</th>
						<th align="left">Visits</th>
						<th align="left">Document</th>
					</tr>
						<xsl:apply-templates select="seqpnt">
							<xsl:sort select="@line" order="ascending" data-type="number"/>
						</xsl:apply-templates>
					</table>
				</div>
			</td>
			<td></td>
		</tr>
	</xsl:template>
	
	<xsl:template match="seqpnt">
		<tr>
			<xsl:if test="position() mod 2 = 0">
				<xsl:attribute name="class">section-oddrow</xsl:attribute>
			</xsl:if>
			<td><xsl:value-of select="@line" /></td>
			<td><xsl:value-of select="@visitcount" /></td>
			<td><a target="_new"><xsl:attribute name="href"><xsl:value-of select="@document" /></xsl:attribute>Source</a></td>
		</tr>
	</xsl:template>
	
	<xsl:template name="PassFailIcon">
		<xsl:param name="visitPercent" />
		<xsl:choose>
		<xsl:when test="$visitPercent &lt; 50">
			<img src="images/fxcop-critical-error.gif">
				<xsl:attribute name="title">Coverage: <xsl:value-of select="concat (format-number($visitPercent,'0.##'), '%')"/></xsl:attribute>
			</img>
		</xsl:when>
		<xsl:when test="$visitPercent &lt; 70">
			<img src="images/fxcop-error.gif">
				<xsl:attribute name="title">Coverage: <xsl:value-of select="concat (format-number($visitPercent,'0.##'), '%')"/></xsl:attribute>
			</img>
		</xsl:when>
		<xsl:otherwise>
			<img src="images/check.jpg" width="16" height="16">
				<xsl:attribute name="title">Coverage: <xsl:value-of select="concat (format-number($visitPercent,'0.##'), '%')"/></xsl:attribute>
			</img>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>