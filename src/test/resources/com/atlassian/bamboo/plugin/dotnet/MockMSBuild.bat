@echo off
set BAM_COUNT=0
:Loop
IF "%~1"=="" GOTO Continue
set /A BAM_COUNT=%BAM_COUNT%+1
echo %~1
SHIFT
GOTO Loop
:Continue
exit 0