@echo off
set BAM_COUNT=0
set BAM_ARGS=
:Loop
IF "%~1"=="" GOTO Continue
set /A BAM_COUNT=%BAM_COUNT%+1

echo "%~1" | findstr "|"
if %errorlevel% == 0 (
set ARG="%~1"
goto ARG_SET
)

echo "%~1" | findstr /r /c:". ."
if %errorlevel% == 0 (
set ARG="%~1"
goto ARG_SET
)

set ARG=%~1

:ARG_SET

if %BAM_COUNT% == 1 set BAM_WORK_DIR=%~1
if %BAM_COUNT% == 2 set BAM_VS_HOME=%~1
if %BAM_COUNT% == 3 set BAM_VS_ARCH=%~1
if %BAM_COUNT% == 4 set BAM_ARGS=%BAM_ARGS% "%~1"
if %BAM_COUNT% GTR 4 set BAM_ARGS=%BAM_ARGS% %ARG%

SHIFT
GOTO Loop
:Continue

echo cd "%BAM_WORK_DIR%"
echo call "%BAM_VS_HOME%\VC\vcvarsall.bat" %BAM_VS_ARCH%
echo call "%BAM_VS_HOME%\Common7\IDE\devenv.com" %BAM_ARGS%
exit ERRORLEVEL